<div>
    <h1>Reset Password</h1>
</div>
<hr>
<div class="row">
    <div class="span12 page_default">
         <div class="span5">
    		<?php
    		    echo Form::open('/auth/reset_password', array('class' => 'well', 'id' => 'reset_pass'));
    		?>
    		<div class="form_field">
    		    <?php 
    		        echo Form::label('reset[email]', 'Enter Email Address');
    		        echo Form::input('reset[email]', '', array('class' => 'span3'));
    		    ?>
    		</div>
    		<?php
    		    echo Form::submit(NULL, 'Reset', array('class' => 'btn btn-primary'));
    		    echo Form::close();		
    		?>
        </div>
    </div>
</div>