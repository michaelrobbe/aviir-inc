<script>
    $(function() {
        $("input[name|='login[username]']").focus();
    });
</script>

<h1>Login</h1>
<?php
    echo Form::open('/auth/process_login', array('class' => 'well'));
?>
<div class="form_field">
    <?php 
        echo Form::label('login[username]', 'Username');
        echo Form::input('login[username]', '', array('class' => 'span3'));
    ?>
</div>
<div class="form_field">
    <?php 
        echo Form::label('login[password]', 'Password');
        echo Form::password('login[password]', '', array('class' => 'span3'));
    ?>
</div>
<?php
    echo Form::submit(NULL, 'Login', array('class' => 'btn btn-primary'));
    
    echo Form::close();
?>