<div class='aviir_pagination'>
    <ul>
    <?php if ($previous_page !== FALSE): ?>
        <li><a class='btn btn-info' href="<?php echo HTML::chars($page->url($previous_page)) ?>" rel="prev"><i class='icon-chevron-left icon-white'></i><i class='icon-chevron-left icon-white '></i> NEWER</a></li>
    <?php endif ?>
    
    <?php if ($next_page !== FALSE): ?>
        <li><a class='btn btn-info' href="<?php echo HTML::chars($page->url($next_page)) ?>" rel="next">OLDER <i class='icon-chevron-right icon-white'></i><i class='icon-chevron-right icon-white'></i> </a></li>
    <?php endif ?>
    </ul>
</div><!-- .pagination -->