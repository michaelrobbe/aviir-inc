<?php defined('SYSPATH') or die('No direct access allowed.');

return array(
    'media_bucket' => '',
    'video_bucket' => '',
    'image_sizes' => array(
        'tiny' => 50,
        'small' => 250,
        'medium' => 500,
        'large' => 800,
        'xlarge' => 1170
    )
);