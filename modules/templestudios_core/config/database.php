<?php defined('SYSPATH') or die('No direct access allowed.');

return array
(
	'default' => array
	(
		'type'       => 'MySQL',
		'connection' => array(
			'hostname'   => 'localhost',
			'database'   => 'temple_studios',
// 			'username'   => 'temple_studios',
// 			'password'   => 'e297m5bXUpXUG5mG',
			'username'   => 'root',
			'password'   => 'throw4',
			'persistent' => FALSE,
		),
		'table_prefix' => '',
		'charset'      => 'utf8',
		'caching'      => FALSE,
		'profiling'    => TRUE,
	),
);
