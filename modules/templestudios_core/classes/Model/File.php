<?php defined('SYSPATH') or die('No direct access allowed.');

class Model_File extends ORM {
    protected $_belongs_to = array(
	    'assets' => array(
	        'model'   => 'Asset',
	        'through' => 'assets_files',
	    )
    );
	
	public function action_delete()
	{
	    if ($this->storage == 'local')
        {
            if (file_exists($this->url))
            {
                unlink($this->url);
            }
        }
        else
        {
            $s3 = new AmazonS3;
            $bucket = str_replace('s3:', '', $this->storage);
            $remote_filename = str_replace('http://'.$bucket.'s3.amazonaws.com/', '', $this->url);
            $result = $s3->delete_object($bucket, $remote_filename);
        }
		
		parent::before();
	}
}