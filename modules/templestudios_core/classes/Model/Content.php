<?php defined('SYSPATH') or die('No direct access allowed.');

class Model_Content extends ORM {
	
    public $memcache = false;
    
	protected $_has_many = array(
	    'contentfields' => array(
	        'model' => 'Contentfield'
	    )
	);
    
    public function __construct($id = null)
    {
        parent::__construct($id);
        $this->memcache = Cache::instance();
    }
    
    public function clear_cache()
    {
        $content_cache_key = $this->id.'_'.$this->node_name.'_';
        
        $content_fields = $this->contentfields->find_all();
        foreach ($content_fields as $content_field)
        {
            $this->memcache->delete($content_cache_key.$content_field->field_name);
        }
    }
    
    public function get_field_value($field_name)
    {
        $field_value = $this->memcache->get($this->id.'_'.$this->node_name.'_'.$field_name);
        if ( ! $field_value)
        {
            $field_value = $this->contentfields->where('field_name', '=', $field_name)->find()->value;
            $this->cache_item($field_name, $field_value);
        }
        return $field_value;
    }
    
    public function get_image_value($field_name, $size = null)
    {
        $field_value = $this->get_field_value($field_name);
        $asset = ORM::factory('Asset', $field_value);
        if ($size != null)
        {
            $image_url = $asset->files->where('type', '=', 'image_'.$size)->find()->url;
        }
        else
        {
            $image_url = $asset->files->where('type', '=', 'image_medium')->find()->url;
        }
        
        return $image_url;
    }
    
    public function get_raw_value($field_name)
    {
        $field_value = $this->get_field_value($field_name);
        $asset = ORM::factory('Asset', $field_value);
        $raw_url = $asset->files->where('type', '=', 'raw')->find()->url;
        
        return $raw_url;
    }
    
    private function cache_item($field_name, $field_value)
    {
        $cache_key = $this->id.'_'.$this->node_name.'_'.$field_name;
        $this->memcache->set($cache_key, $field_value);
    }
	
	public function clear_cache_item($field_name)
	{
		 $field_value = $this->memcache->delete($this->id.'_'.$this->node_name.'_'.$field_name);
	}
}
