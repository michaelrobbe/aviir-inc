<?php defined('SYSPATH') or die('No direct access allowed.');

class Model_Setting extends ORM {
	public function get_setting_by_name($setting_name)
	{
		return $this->where('name', '=', $setting_name)->find()->value;
	}
}