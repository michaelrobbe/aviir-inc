<?php defined('SYSPATH') or die('No direct access allowed.');

class Model_User extends Model_Auth_User {
	protected $_has_many = array(
        'oauth_users' => array(
            'model'   => 'Oauthuser',
            'foreign_key' => 'user_id',
        ),
        'roles'       => array('model' => 'Role', 'through' => 'roles_users'),
        'user_tokens' => array('model' => 'User_Tokens'),
    );
		
	protected $_has_one = array('profile' => array());
	
	public function save(Validation $validation = NULL)
	{
		parent::save();
		
		if ($this->profile->id == NULL)
		{
			$profile = ORM::factory('Profile');
			$profile->user_id = $this->id;
			$profile->save();
			
			$address = ORM::factory('Address');
			$address->profile_id = $profile->id;
			$address->save();		
		}		
	}
}