<?php defined('SYSPATH') or die('No direct access allowed.');

class Model_Profile extends ORM {  
	protected $_belongs_to = array('user' => array());
	
	protected $_has_one = array(
	    'address' => array(
	        'model'   => 'Address',
	        'foreign_key' => 'profile_id',
	    ),
    );
	
	protected $_has_many = array(
	    'assets' => array(
	        'model'   => 'Asset',
	        'through' => 'assets_profiles',
	    ),
    );
}