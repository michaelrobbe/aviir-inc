<?php defined('SYSPATH') or die('No direct access allowed.');

class Model_Events_Share extends ORM {
   protected $_belongs_to = array(
        'share' => array(
            'model'   => 'share',
        )
    );
}