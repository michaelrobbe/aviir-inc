<?php defined('SYSPATH') or die('No direct access allowed.');

class Model_Address extends ORM {
   protected $_belongs_to = array(
	    'profile' => array(
	        'model'   => 'Profile',
	        'foreign_key' => 'profile_id',
	    )
    );
}