<?php defined('SYSPATH') or die('No direct access allowed.');

class Model_Share extends ORM {
   protected $_has_many = array(
	    'events_shares' => array(
	        'model'   => 'Events_Share',
	    )
    );
    protected $_belongs_to = array(
        'user' => array(
            'model'   => 'User',
        ),
    );
    
    public function generate_share($user, $type, $type_id)
    {
        $this->user_id = $user->id;
        $this->session_id = session_id();
        $this->unique_id = $this->generate_unique_id();
        $this->type = $type;
        $this->type_id = $type_id;
        $this->save();
        
        if ($this->id > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    private function generate_unique_id()
    {
        return strtoupper(uniqid());
    }
}