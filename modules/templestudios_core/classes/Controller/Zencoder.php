<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Zencoder extends Controller_Website {
    
    public function action_test()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, 'http://tstuds.com/zencoder/notification/41');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, array(
            'input' => '{"output":{"total_bitrate_in_kbps":807,"frame_rate":24.0,"md5_checksum":null,"channels":"2","audio_codec":"aac","video_codec":"h264","state":"finished","video_bitrate_in_kbps":770,"width":568,"format":"mpeg4","url":"http://savn-tv-video.s3.amazonaws.com/assets/41/7b210afe_320.mp4","height":320,"audio_sample_rate":22050,"label":"small 320","audio_bitrate_in_kbps":37,"duration_in_ms":60232,"file_size_in_bytes":6078864,"id":36961800},"input":{"total_bitrate_in_kbps":676,"frame_rate":24.0,"md5_checksum":null,"channels":"2","audio_codec":"aac","state":"finished","video_codec":"h264","video_bitrate_in_kbps":612,"width":640,"format":"mpeg4","height":360,"audio_sample_rate":22050,"audio_bitrate_in_kbps":64,"duration_in_ms":60095,"file_size_in_bytes":5510872,"id":21629203},"job":{"created_at":"2012-07-05T14:41:54Z","test":false,"pass_through":null,"updated_at":"2012-07-05T14:42:43Z","submitted_at":"2012-07-05T14:41:54Z","state":"finished","id":21632212}}'
        )); 
        $response = curl_exec($ch);
        
        echo $response;
        die();
    }
    
    public function action_notification()
    {
        $id = $this->request->param('id');
        $asset = ORM::factory('Asset', $id);
        
        require_once Kohana::find_file('vendor', 'amazon/sdk.class');
        
        $this->s3 = new AmazonS3;
        
        $zencoder_config = Kohana::$config->load('zencoder');
        $zencoder_api_key = $zencoder_config->get('api_key');
        
        $media_bucket = Kohana::$config->load('amazon.media_bucket');
        $video_bucket = Kohana::$config->load('amazon.video_bucket');
        $image_sizes = Kohana::$config->load('amazon.image_sizes');
        
        require_once Kohana::find_file('vendor', 'zencoder/Services/Zencoder');
        
        $zencoder = new Services_Zencoder($zencoder_api_key);
        
        $notification = $zencoder->notifications->parseIncoming();
        
        foreach ($notification->job->outputs as $output)
        {
            if ($output->state == 'finished')
            {
                $file = $asset->files->where('url', '=', $output->url)->find();
                $file->type = 'video_'.str_replace(' ', '_', $output->label);
                $file->url = $output->url;
                $file->storage = 's3:'.$video_bucket;
                $file->save();
                
                $asset->add('files', $file);
                
                if (isset($output->thumbnails[0]))
                {
                    foreach ($output->thumbnails[0]->images as $thumbnail)
                    {
                        $thumbnail_url_path = pathinfo($thumbnail->url);
                        $temp_folder = 'media/uploads/temp/';
                        $short_filename = $thumbnail_url_path['filename'];
                        $filename_extension = $thumbnail_url_path['extension'];
                        $temp_image = $temp_folder.$short_filename.'.'.$filename_extension;
                        
                        file_put_contents($temp_image, file_get_contents($thumbnail->url));
                        
                        foreach ($image_sizes as $size_name => $width)
                        {
                            $save_base_filenamne = $short_filename.'_'.$size_name.'.'.$filename_extension;
                            $save_image_location = $temp_folder.$save_base_filenamne;
                            $remote_filename = 'assets/'.$asset->id.'/'.$save_base_filenamne;
                            
                            $image = Image::factory($temp_image);
                            $image->resize($width, null, Image::WIDTH);
                            $image->save($save_image_location);
                            
                            $push_result = $this->push_file_to_s3($save_image_location, $remote_filename, $video_bucket);
                            if ($push_result)
                            {
                                $file = $asset->files->where('url', '=', 'http://'.$video_bucket.'.s3.amazonaws.com/'.$remote_filename)->find();
                                $file->type = 'image_'.$size_name;
                                $file->url = 'http://'.$video_bucket.'.s3.amazonaws.com/'.$remote_filename;
                                $file->storage = 's3:'.$video_bucket;
                                $file->save();
                                
                                $asset->add('files', $file);
                                
                                unlink($save_image_location);
                            }
                        }
                        unlink($temp_image);
                    }
                    
                    $upload_file = $asset->files->where('type', '=', 'upload')->find();
                    if ($upload_file->id > 0)
                    {
                        $asset->remove('files', $upload_file);
                        unlink(substr($upload_file->url, 1));
                        $upload_file->delete();
                    }
                }
            }
        }
        die();
    }

    private function push_file_to_s3($local_filename, $remote_filename, $bucket)
    {
        $options = array();
        $options['fileUpload'] = $local_filename;
        $options['acl'] = AmazonS3::ACL_PUBLIC;
        $result = $this->s3->create_object($bucket, $remote_filename, $options);
        
        if ($result->status == 200)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}   