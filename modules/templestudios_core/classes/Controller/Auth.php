<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Auth extends Controller_Website {
    
    public function action_login()
    {
        $request = Request::initial();
        
        $session = Session::instance();
        
        $auth = Auth::instance();
        if ($session->get('referrer_url', false) === false)
        {
            $referrer_url = $request->referrer();
            if ($referrer_url == NULL)
            {
                $referrer_url = '/';
            }
            $session->set('referrer_url', $referrer_url);
        }
        
        if (isset($this->user) AND $auth->logged_in())
        {
            $this->redirect($session->get('referrer_url'));
        }
        
        $site_url = Arr::get($_SERVER, 'SERVER_NAME', Kohana::$config->load('website')->get('url'));
        if ($this->protocol != 'https' AND strstr($site_url, 'admin') === false)
        {
            $this->redirect('https://'.$site_url.'/login');
        }
        
        $view = View::factory('auth/login');
        
        $this->template->body = $view;
    }
    
    public function action_login_facebook()
    {
        require_once Kohana::find_file('vendor', 'facebook/facebook');
        
        $session = Session::instance();
        
        $facebook_config = (array) Kohana::$config->load('facebook');
        $facebook = new Facebook($facebook_config);
        
        $facebook_login_state = Arr::get($_GET, 'state', false);
        
        if ($facebook_login_state)
        {
            $facebook_user = $facebook->getUser();
            if (isset($facebook_user) AND $facebook_user > 0)
            {
                $facebook_profile = (object) $facebook->api('/me');
                
                $oauthuser = ORM::factory('Oauthuser')->where('oauth_type', '=', 'facebook')->where('oauth_user_id', '=', $facebook_user)->find();
                
                if ($oauthuser->id == 0)
                {
                    // Oauth User Not Found - Register or Link
                    $facebook_profile = (object) $facebook->api('/me');
                    
                    $user = ORM::factory('User')->where('email', '=', $facebook_profile->email)->find();
                    if ($user->loaded() AND $user->id > 0)
                    {
                        // User with Facebook Email found - link to user
                        $oauthuser = ORM::factory('Oauthuser');
                        $oauthuser->user_id = $user->id;
                        $oauthuser->oauth_type = 'facebook';
                        $oauthuser->oauth_user_id = $facebook_user;
                        $oauthuser->token = $facebook->getAccessToken();
                        $oauthuser->save();
                    }
                    else
                    {
                        // User with Facebook Email not found - Register user
                        $facebook_profile = (object) $facebook->api('/me');
                        
                        $user = ORM::factory('User');
                        $user->username = $facebook_profile->username;
                        $user->email = $facebook_profile->email;
                        $user->first_name = $facebook_profile->first_name;
                        $user->last_name = $facebook_profile->last_name;
                        $user->password = 'test1234';
                        $user->save();
                        
                        $oauthuser = ORM::factory('Oauthuser');
                        $oauthuser->user_id = $user->id;
                        $oauthuser->oauth_type = 'facebook';
                        $oauthuser->oauth_user_id = $facebook_user;
                        $oauthuser->token = $facebook->getAccessToken();
                        $oauthuser->save();
                    }
                }
                
                // Oauth User Found - Login
                $this->auth->force_login($oauthuser->user);
                $redirect_url = $session->get('referrer_url', '/');
                $session->delete('referrer_url');
                $message = "You have been logged in. Welcome ".$oauthuser->user->username.'.';
                Notice::add(Notice::SUCCESS, $message);
                $this->redirect($redirect_url);
            }
            die();
        }
        else
        {
            $login_url = $facebook->getLoginUrl(array(
                'scope' => $facebook_config['req_perms']
            ));
            $this->redirect($login_url);
        }
    }
    
    public function action_login_twitter()
    {
        require_once Kohana::find_file('vendor', 'twitter/twitteroauth');
        
        $session = Session::instance();
        
        define('CONSUMER_KEY', Kohana::$config->load('twitter.consumer_key'));
        define('CONSUMER_SECRET', Kohana::$config->load('twitter.consumer_secret'));
        define('OAUTH_CALLBACK', 'http://'.$_SERVER["SERVER_NAME"].url::site('/auth/login_twitter'));
        
        $access_token = $session->get('access_token', false);
        $oauth_verifier = Arr::get($_REQUEST, 'oauth_verifier');
        
        if ($access_token AND $oauth_verifier)
        {
            $connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $access_token['oauth_token'], $access_token['oauth_token_secret']);
            // $content = $connection->get('account/verify_credentials');
            // echo Debug::vars($content);
            
            $access_token = $connection->getAccessToken($oauth_verifier);
            
            if (isset($access_token['user_id']) AND $access_token['user_id'] != 0)
            {
                $oauthuser = ORM::factory('Oauthuser')->where('oauth_type', '=', 'twitter')->where('oauth_user_id', '=', $access_token['user_id'])->find();
                
                if ($oauthuser->id == 0)
                {
                    // Oauth User Not Found - Register or Link
                    $user = ORM::factory('User')->where('username', '=', $access_token['screen_name'])->find();
                    if ($user->loaded() AND $user->id > 0)
                    {
                        // User with Facebook Email found - link to user
                        $oauthuser = ORM::factory('Oauthuser');
                        $oauthuser->user_id = $user->id;
                        $oauthuser->oauth_type = 'twitter';
                        $oauthuser->oauth_user_id = $access_token['user_id'];
                        $oauthuser->token = $access_token['oauth_token'];
                        $oauthuser->save();
                    }
                    else
                    {
                        // User with Facebook Email not found - Register user
                        $user = ORM::factory('User');
                        $user->username = $access_token['screen_name'];
                        $user->email = $access_token['screen_name'].'@savn.tv';
                        $user->password = 'test1234';
                        $user->save();
                        
                        $oauthuser = ORM::factory('Oauthuser');
                        $oauthuser->user_id = $user->id;
                        $oauthuser->oauth_type = 'twitter';
                        $oauthuser->oauth_user_id = $access_token['user_id'];
                        $oauthuser->token = $access_token['oauth_token'];
                        $oauthuser->save();
                    }
                }
                
                // Oauth User Found - Login
                $this->auth->force_login($oauthuser->user);
                $redirect_url = $session->get('referrer_url', '/');
                $session->delete('referrer_url');
                $message = "You have been logged in. Welcome ".$oauthuser->user->username.'.';
                Notice::add(Notice::SUCCESS, $message);
                $this->redirect($redirect_url);
                die();
            }
        }
        else
        {
            $connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET);
            
            $request_token = $connection->getRequestToken(OAUTH_CALLBACK);
            $token = $request_token['oauth_token'];
            $session->set('access_token', $request_token);
            $session->set('oauth_token', $token);
            $session->set('oauth_token_secret', $request_token['oauth_token_secret']);
            
            $url = $connection->getAuthorizeURL($token);
            $this->redirect($url);
        }
        die();
    }
    
    public function action_login_google()
    {
        require_once Kohana::find_file('vendor', 'google/apiClient');
        require_once Kohana::find_file('vendor', 'google/contrib/apiOauth2Service');
        
        $session = Session::instance();
        
        $code = Arr::get($_GET, 'code', false);
        
        $google_config = (array) Kohana::$config->load('google');
        
        $client = new apiClient($google_config);
        $oauth2 = new apiOauth2Service($client);
        
        
        if ($code)
        {
            $client->authenticate();
            $session->set('token', $client->getAccessToken());
            $this->redirect('/auth/login_google');
        }
        
        if ($session->get('token', false)) {
            $client->setAccessToken($session->get('token'));
        }
        
        if ($client->getAccessToken())
        {
            $session->set('token', $client->getAccessToken());
            
            $google_user = $oauth2->userinfo->get();
            
            $oauthuser = ORM::factory('Oauthuser')->where('oauth_type', '=', 'google')->where('oauth_user_id', '=', $google_user->id)->find();
            
            if ($oauthuser->id == 0)
            {
                // Oauth User Not Found - Register or Link
                $user = ORM::factory('User')->where('email', '=', $google_user->email)->find();
                if ($user->loaded() AND $user->id > 0)
                {
                    // User with Facebook Email found - link to user
                    $oauthuser = ORM::factory('Oauthuser');
                    $oauthuser->user_id = $user->id;
                    $oauthuser->oauth_type = 'google';
                    $oauthuser->oauth_user_id = $google_user->id;
                    $oauthuser->token = $session->get('token');
                    $oauthuser->save();
                }
                else
                {
                    // User with Facebook Email not found - Register user
                    $user = ORM::factory('User');
                    $user_email = explode('@', $google_user->email);
                    $user->username = $user_email[0];
                    $user->email = $google_user->email;
                    $user_name = explode(' ', $google_user->name);
                    $user->first_name = $user_name[0];
                    $user->last_name = $user_name[1];
                    $user->password = 'test1234';
                    $user->save();
                    
                    $oauthuser = ORM::factory('Oauthuser');
                    $oauthuser->user_id = $user->id;
                    $oauthuser->oauth_type = 'google';
                    $oauthuser->oauth_user_id = $google_user->id;
                    $oauthuser->token = $session->get('token');
                    $oauthuser->save();
                }
            }
            
            // Oauth User Found - Login
            $this->auth->force_login($oauthuser->user);
            $redirect_url = $session->get('referrer_url', '/');
            $session->delete('referrer_url');
            $message = "You have been logged in. Welcome ".$oauthuser->user->username.'.';
            Notice::add(Notice::SUCCESS, $message);
            $this->redirect($redirect_url);
        }
        else
        {
            $auth_url = $client->createAuthUrl();
            $this->redirect($auth_url);
        }
        die();
    }
    
    public function action_register()
    {
        $view = View::factory('auth/login');
        
        $this->template->body = $view;
    }
    
    public function action_process_login()
    {
        $this->auth = Auth::instance();
        
        $post = Arr::get($_POST, 'login', null);
        
        $remember = false;
        if (isset($post['remember']))
        {
            $remember = true;
        }
        
        $login_result = $this->auth->login($post['username'], trim($post['password']), $remember);
        
        $user = ORM::factory('User')->where('username', '=', $post['username'])->or_where('email', '=', $post['username'])->find();
        
        echo Debug::vars($user);
        if ($login_result == true)
        {
            $user = $this->auth->get_user();
            $message = "You have been logged in. Welcome ".$user->username.'.';
            Notice::add(Notice::SUCCESS, $message);
            $session = Session::instance();
            $redirect_url = $session->get('referrer_url', '/');
            $session->delete('referrer_url');
            $this->redirect($redirect_url);
        }
        elseif ( ! $user->has('roles', ORM::factory('Role', 1)))
        {
            Notice::add(Notice::ERROR, 'Please check your email ('.$user->email.') to activate your account.');
            $this->redirect('login');
        }
        else
        {
            Notice::add(Notice::ERROR, 'The username or password you entered is incorrect.');
            $this->redirect('login');
        }
    }
    
    public function action_logout()
    {
        Notice::add(Notice::INFO, 'You have been logged out.');
        Auth::instance()->logout();
        $this->redirect('login');
    }
    
    public function action_process_register()
    {
        require_once Kohana::find_file('vendor','mailchimp/MCAPI.class');
        require_once Kohana::find_file('vendor','mailchimp/config.inc');
                
        $user = ORM::factory('User');
        $post = $_POST['register'];
        
        $user->username = $post['username'];
        $user->email = $post['email'];  
        $user->password = $post['password1'];
        
        $profanities = array(); 
        $profanities_orm = ORM::factory('Profanity')->find_all();
        foreach ($profanities_orm as $profanity)
        {
            $profanities[] = $profanity->word;
        }
        
        $censored_username = Text::censor($user->username, $profanities);
        
        if (strstr($censored_username, '##'))
        {
            Notice::add(Notice::ERROR, 'Username must not contain profanity.');
            $this->redirect('login');
        }
        else
        {
            $user->save();
            
            $site_name = Kohana::$config->load('website')->get('site_name');
            $site_url = Kohana::$config->load('website')->get('url');
            
            $to = array($user->email => $user->username);
            $from = array('noreply@'.$site_url => $site_name); 
            $subject = 'Welcome to '.$site_name.'. Activate your account.';
            $message = 'Welcome to '.$site_name.'. Activate your account by clicking on the following link.<br/><br/>';
            
            $activation_code = AuthHelper::activation_code($user->email);
            $activation_url = 'http://'.$site_url.'/auth/activate_account?activation_code='.$activation_code;
            $message.= '<a href="'.$activation_url.'">'.$activation_url.'</a>.';
            $message.= '<br/><br/>';
            
            $message.= 'If this is not you, click on the following link to delete your account.<br/><br/>';
            $activation_code = AuthHelper::activation_code($user->email);
            $activation_url = 'http://'.$site_url.'/auth/delete_account?activation_code='.$activation_code;
            $message.= '<a href="'.$activation_url.'">'.$activation_url.'</a>.';
            $message.= '<br/><br/>';
            
            $message.= $site_name.' Team';
            
            $mailer = Mailer::instance();
            $mailer->to($to);
            $mailer->from($from);
            $mailer->subject($subject);
            $mailer->html($message);
            $result = $mailer->send();
            
            if (isset($post['subscribe']))
            {       
                $api = new MCAPI($apikey);
                $subscribed = $api->listMemberInfo($listId, $user->email);
                if ( ! $api->errorCode)
                {
                    if ($subscribed['success'] != 1)
                    {
                        $opt_in = $api->listSubscribe($listId, $user->email,  $merge_vars=NULL, $email_type='html', $double_optin=false, $update_existing=false, $replace_interests=true, $send_welcome=true);  
                    }
                }
            }
            
            $message = 'Your account has been created. Please check your email ('.$user->email.') to activate your account.';
            Notice::add(Notice::SUCCESS, $message);
            $this->redirect('/login');
        }
    }
    
    public function action_profile()
    {
        $view = View::factory('auth/profile');
        if (isset($this->user))
        {
            $view->user = $this->user;
        }
        else
        {
            $message = 'Please log in.';
            Notice::add(Notice::ERROR, $message);
            $this->redirect('/login');
        }
        
        if (isset($_POST))
        {
            if (isset($_POST['profile']['password1']) AND isset($_POST['profile']['password2']) AND $_POST['profile']['password1'] != "" AND $_POST['profile']['password2'] != "")
            {
                if ($_POST['profile']['password1'] == $_POST['profile']['password2'])
                {
                    $this->user->password = $_POST['profile']['password1'];
                    $message = 'Successfully Changed Password';
                    Notice::add(Notice::SUCCESS, $message);
                }
                else
                {
                    $message = 'Passwords did not match';
                    Notice::add(Notice::ERROR, $message);
                }           
            }
            if (isset($_POST['profile']['username']) AND $_POST['profile']['username'] != $this->user->username)
            {
                $this->user->username = $_POST['profile']['username'];
                $message = 'Successfully Changed Username';
                Notice::add(Notice::SUCCESS, $message);
            }
            if (isset($_POST['profile']['email']) AND $_POST['profile']['email'] != $this->user->email)
            {
                $this->user->email = $_POST['profile']['email'];
                $message = 'Successfully Changed Email';
                Notice::add(Notice::SUCCESS, $message);
            }
            
            $this->user->save();    
        }
        
        $this->template->body = $view;
    }
    
    public function action_check_email()
    {
        $email = $_GET['register']['email'];
        $user = ORM::factory('User')->where('email', '=', $email)->find();
        if ($user->id == 0)
        {
            echo 'true';
        }
        else
        {
            echo 'false';
        }
        die();
    }
    
    public function action_check_username()
    {
        $username = $_GET['register']['username'];
        $user = ORM::factory('User')->where('username', '=', $username)->find();
        if ($user->id == 0)
        {
            echo 'true';
        }
        else
        {
            echo 'false';
        }
        die();
    }
    
    public function action_forgot_password()
    {
        $view = View::factory('auth/forgot');
        $this->template->body = $view;      
    }
    
    public function action_reset_password()
    {
        $post = $_POST['reset']['email'];
        $user = ORM::factory('User')->where('email', '=', $post)->find();
    
        if ($user->loaded() AND $user->id != 0)
        {
            $new_pass = $this->generatePassword($length = 8, $strength = 2);
            $user->password = $new_pass;
            $user->save();
            
            $to = array($user->email => $user->username);
            $from = array('noreply@'.Kohana::$config->load('website')->get('url') => Kohana::$config->load('website')->get('site_name')); 
            $subject = 'Reset Your Password';
            $message = 'Your new password is: '.$new_pass.' <br/> You can change this by <a href="http://';
            $message.= Kohana::$config->load('website')->get('url');
            $message.= '/login">logging in</a> , navigating to your account, and changing your password.';
            $mailer = Mailer::instance();
            $mailer->to($to);
            $mailer->from($from);
            $mailer->subject($subject);
            $mailer->html($message);
            $result = $mailer->send();
            
            $message = 'Your new password has been sent to '.$user->email.'. Please check your email to retrieve your password.';
            Notice::add(Notice::SUCCESS, $message);
            $this->redirect('/login');
        }
        else
        {
            $message = 'Email Address Not Found.';
            Notice::add(Notice::ERROR, $message);
            $this->redirect('/forgot_password');
        }
    }

    public function generatePassword($length=9, $strength=0)
    {
        $vowels = 'aeuy';
        $consonants = 'bdghjmnpqrstvz';
        if ($strength & 1) {
            $consonants .= 'BDGHJLMNPQRSTVWXZ';
        }
        if ($strength & 2) {
            $vowels .= "AEUY";
        }
        if ($strength & 4) {
            $consonants .= '23456789';
        }
        if ($strength & 8) {
            $consonants .= '@#$%';
        }
     
        $password = '';
        $alt = time() % 2;
        for ($i = 0; $i < $length; $i++) {
            if ($alt == 1) {
                $password .= $consonants[(rand() % strlen($consonants))];
                $alt = 0;
            } else {
                $password .= $vowels[(rand() % strlen($vowels))];
                $alt = 1;
            }
        }
        return $password;
    }
    
    public function action_delete_account()
    {
        $request = Request::initial();
        
        $activation_code = Arr::get($_GET, 'activation_code', false);
        
        if ($activation_code)
        {
            $email = AuthHelper::decypher_activation_code($activation_code);
            
            $user = ORM::factory('User')->where('email', '=', $email)->find();
            
            if ($user->id > 0)
            {
                if ( ! $user->has('roles', ORM::factory('Role', 1)))
                {
                    $user->delete();
                    
                    $message = 'You have deleted your account.';
                    Notice::add(Notice::SUCCESS, $message);
                    $this->redirect('/');
                }
                else
                {
                    $message = 'This account ('.$user->username.') has already been activated.';
                    Notice::add(Notice::ERROR, $message);
                    $this->redirect('/');
                }
            }
            else
            {
                $message = 'No account exists with that email address.';
                Notice::add(Notice::ERROR, $message);
                $this->redirect('/');
            }
        }
    }
    
    public function action_activate_account()
    {
        $request = Request::initial();
        $auth = Auth::instance();
        
        $activation_code = Arr::get($_GET, 'activation_code', false);
        
        if ($activation_code)
        {
            $email = AuthHelper::decypher_activation_code($activation_code);
            
            $user = ORM::factory('User')->where('email', '=', $email)->find();
            
            if ($user->id > 0)
            {
                if ( ! $user->has('roles', ORM::factory('Role', 1)))
                {
                    $user->add('roles', ORM::factory('Role', 1));
                    
                    $auth->force_login($user);
                    
                    $message = "You have been logged in. Welcome ".$user->username.'.';
                    Notice::add(Notice::SUCCESS, $message);
                    $this->redirect('/profile');
                }
                else
                {
                    $message = 'This account ('.$user->username.') has already been activated.';
                    Notice::add(Notice::ERROR, $message);
                    $this->redirect('/');
                }
            }
            else
            {
                $message = 'No account exists with that email address.';
                Notice::add(Notice::ERROR, $message);
                $this->redirect('/');
            }
        }
    }
} // End Auth
