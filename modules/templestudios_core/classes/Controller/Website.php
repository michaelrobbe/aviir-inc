<?php defined('SYSPATH') or die('No direct script access.');

abstract class Controller_Website extends Controller_Template {
 
    public $page_title;
 
    public function before()
    {
        $this->auth = Auth::instance();
        $logged_in = $this->auth->logged_in();
        $this->logged_in = $logged_in;
        $template_base = 'default';
        $this->template = 'templates/'.$template_base.'/layout';
        $this->template_base = $template_base;
        
        $settings = ORM::factory('Setting');
        $this->site_name = '';
        
        parent::before();
        
        $request = Request::initial();
        $requsted_controller = $request->controller();
        $requsted_action = $request->action();
        
        $config = Kohana::$config->attach(new Config_Database);
        
        // Make $page_title available to all views
        $this->site_name = Kohana::$config->load('website')->get('site_name');
        $this->url = Kohana::$config->load('website')->get('site_name');
        
        $maintenance = (bool) Kohana::$config->load('website')->get('maintenance');
        
        $page_main_title = ucfirst($requsted_controller);
        if ($requsted_action != 'index')
        {
            $page_main_title .= ' - '.ucfirst($requsted_action);
        }
        $page_main_title = str_replace('Index', 'Home', $page_main_title);
        
        if (! isset($this->page_title) OR $this->page_title == '')
        {
            $this->page_title = $this->site_name.' - '.$page_main_title;
        }
        else
        {
            $this->page_title = $this->site_name.' - '.$this->page_title;
        }
        
        $navigation = Kohana::$config->load('navigation.main');
        $header_vars = '';
        
        $protocol = 'http';
        $https = Arr::get($_SERVER, 'HTTPS', 'off');
        if ($https == 'on')
        {
            $protocol = 'https';
        }
        
        if ($requsted_controller != 'auth' AND $protocol == 'https')
        {
            $site_url = Kohana::$config->load('website')->get('url');
            $redirect_url = 'http://'.$site_url.$_SERVER['REQUEST_URI'];
            $this->redirect($redirect_url);
        }
        
        $this->protocol = $protocol;
        $this->template->protocol = $protocol;
        $this->template->title = $this->page_title;
        $this->template->logged_in = $logged_in;
        $this->template->header = View::factory('templates/'.$this->template_base.'/header');
        $this->template->header->protocol = $protocol;
        $this->template->header->site_name = $this->site_name;
        $this->template->header->logged_in = $logged_in;
        $this->template->header->navigation = $navigation;
        $this->template->header_vars = $header_vars;
        $this->template->body = '';
        $this->template->pre_body = '';
        $this->template->footer = View::factory('templates/'.$this->template_base.'/footer');
        $this->template->footer->logged_in = $logged_in;
        $this->template->footer->navigation = $navigation;
        $this->template->footer->site_name = $this->site_name;
        
        if ($logged_in == true)
        {
            $this->user = $this->auth->get_user();
        }
        else
        {
            $this->user = ORM::factory('User');    
        }
        
        $this->template->header->user = $this->user;
        $this->template->footer->user = $this->user;
        $this->template->header->current_page = $requsted_controller;
        
        if ($maintenance)
        {
            $this->page_title = $this->site_name;
            $this->template->title = $this->page_title;
            $this->template->body = View::factory('maintenance');
            
            $request = Request::initial();
            $requsted_controller = $request->controller();
            $requsted_action = $request->action();
            
            $this->template->body->requsted_controller = $requsted_controller;
            $this->template->body->requsted_action = $requsted_action;
            $this->template->body->requsted_uri = $request->uri();
            
            $this->template->header->requsted_controller = $requsted_controller;
            $this->template->header->requsted_action = $requsted_action;
            $this->template->header->requsted_uri = $request->uri();
            
            $this->template->footer->requsted_controller = $requsted_controller;
            $this->template->footer->requsted_action = $requsted_action;
            $this->template->footer->requsted_uri = $request->uri();
            
            echo $this->template->render();
            die();
        }
    }

    public function after()
    {
        $navigation = Kohana::$config->load('navigation.main');
        $this->template->body->navigation = $navigation;
        
        $request = Request::initial();
        $requsted_controller = $request->controller();
        $requsted_action = $request->action();
        
        $this->template->body->requsted_controller = $requsted_controller;
        $this->template->body->requsted_action = $requsted_action;
        $this->template->body->requsted_uri = $request->uri();
        
        $this->template->header->requsted_controller = $requsted_controller;
        $this->template->header->requsted_action = $requsted_action;
        $this->template->header->requsted_uri = $request->uri();
        
        $this->template->footer->requsted_controller = $requsted_controller;
        $this->template->footer->requsted_action = $requsted_action;
        $this->template->footer->requsted_uri = $request->uri();
        
        parent::after();
    }
}