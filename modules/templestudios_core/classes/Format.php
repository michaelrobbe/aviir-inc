<?php
    class Format {
        public static function create_sort_link($title, $field, $query_array, $controller)
        {
            $sort_title = $title;
            $sort_field_name = $field;
            $temp_query_array = $query_array;
            $temp_query_array['order_by'] = $sort_field_name;
            
            if ( ! in_array($sort_field_name, $query_array))
            {
                $temp_query_array['sorted'] = 'asc';
            }
            else
            {
                if ($query_array['sorted'] == 'desc')
                {
                    $sort_title.= '&darr;';
                    $temp_query_array['sorted'] = 'asc';
                }
                else
                {
                    $sort_title.= '&uarr;';
                    $temp_query_array['sorted'] = 'desc';
                }
            }
            return HTML::anchor('/'.$controller.'?'.http_build_query($temp_query_array), $sort_title);
        }
        
        public static function url_title($title)
        {
        	$title = trim($title);
        	$title = strtolower($title);
        	$title = str_replace(array("'", '%', '?', ':'), '', $title);
        	$title = str_replace(' ', '_', $title);
            return $title;
        }
        
        public static function un_url_title($title)
        {
            return str_replace('_', ' ', $title);
        }
    }