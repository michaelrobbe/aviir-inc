<?php defined('SYSPATH') or die('No direct script access.');

class HTML extends Kohana_HTML {
    public static function style($file, array $attributes = NULL, $protocol = NULL, $index = FALSE)
    {
        if (strpos($file, '://') === FALSE)
        {
            // Add the base URL
            $file = URL::base($protocol, $index).$file;
        }
        else
        {
            $protocol = 'http';
            $https = Arr::get($_SERVER, 'HTTPS', 'off');
            if ($https == 'on')
            {
                $protocol = 'https';
            }
            $file = str_replace('http', $protocol, $file);
        }
        
        // Set the stylesheet link
        $attributes['href'] = $file;

        // Set the stylesheet rel
        $attributes['rel'] = 'stylesheet';

        // Set the stylesheet type
        $attributes['type'] = 'text/css';

        return '<link'.HTML::attributes($attributes).' />';
    }
    
    public static function image($file, array $attributes = NULL, $protocol = NULL, $index = FALSE)
    {
        if (strpos($file, '://') === FALSE)
        {
            // Add the base URL
            $file = URL::base($protocol, $index).$file;
        }
        else
        {
            $protocol = 'http';
            $https = Arr::get($_SERVER, 'HTTPS', 'off');
            if ($https == 'on')
            {
                $protocol = 'https';
            }
            $file = str_replace('http', $protocol, $file);
        }

        // Add the image link
        $attributes['src'] = $file;

        return '<img'.HTML::attributes($attributes).' />';
    }
}