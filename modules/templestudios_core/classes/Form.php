<?php

class Form extends Kohana_Form {
    public static function file_image($name, $value = '')
    {
        $view = View::factory('content/upload');
        $view->name = $name;
        $view->value = $value;
        $view->type = 'image';
        return $view;
    }
    
    public static function file_raw($name, $value = '')
    {
        $view = View::factory('content/upload');
        $view->name = $name;
        $view->value = $value;
        $view->type = 'raw';
        return $view;
    }
    
    public static function toggle($name, $value = '')
    {
        $view = View::factory('content/toggle');
        $view->name = $name;
        $view->value = $value;
        return $view;
    }
}