<?php defined('SYSPATH') or die('No direct access allowed.');

return array(
    'company_about' => array(
        'label' => 'About',
        'type' => 'multiple',
        'fields' => array(
        	'title' => array(
                'label' => 'Title',
                'type' => 'input',
                'rules' => array(
                
                ),  
            ),
            'description' => array(
                'label' => 'Description',
                'type' => 'textarea',
                'rules' => array(
                
                ),
                'class' => 'ckeditor',
                
            ),
        )
    ),
    'company_executives' => array(
        'label' => 'Executives',
        'type' => 'multiple',
        'fields' => array(
        	'name' => array(
                'label' => 'Name',
                'type' => 'input',
                'rules' => array(
                
                ),  
            ),
            'title' => array(
                'label' => 'Title',
                'type' => 'input',
                'rules' => array(
                
                ),  
            ),
            'description' => array(
                'label' => 'Description',
                'type' => 'textarea',
                'rules' => array(
                
                ),
                'class' => 'ckeditor',
                
            ),
            'image'  => array(
                'label' => 'Upload Image',
                'type' => 'file_image',
                'class' => 'fileupload',
                'rules' => array(    
                ),
            ),
        )
    ),
    'company_investors' => array(
        'label' => 'Investors',
        'type' => 'multiple',
        'fields' => array(
        	'name' => array(
                'label' => 'Name',
                'type' => 'input',
                'rules' => array(
                
                ),  
            ),
            'description' => array(
                'label' => 'Description',
                'type' => 'textarea',
                'rules' => array(
                
                ),
                'class' => 'ckeditor',
                
            ),
            'website_link' => array(
                'label' => 'Link To Website',
                'type' => 'input',
                'rules' => array(
                
                ),  
            ),
            'Linked_in' => array(
                'label' => 'Linked In Link',
                'type' => 'input',
                'rules' => array(
                
                ),  
            ),
            'image'  => array(
                'label' => 'Upload Image',
                'type' => 'file_image',
                'class' => 'fileupload',
                'rules' => array(    
                ),
            ),
        )
    ),
    'blog_events' => array(
        'label' => 'Events',
        'type' => 'multiple',
        'fields' => array(
        	'title' => array(
                'label' => 'Post Title',
                'type' => 'input',
                'class' => 'span8',
                'rules' => array(
                
                ),  
            ),
            'urltitle' => array(
                'label' => 'URL',
                'type' => 'input',
                'class' => 'span8',
                'rules' => array(
                	'not_empty' => 'true'
                ),
                'table_display' => false
            ),
            'post_content' => array(
                'label' => 'Post Content',
                'type' => 'textarea',
                'rules' => array(
                
                ),
                'class' => 'ckeditor',
                
            ),
            'date' => array(
                'label' => 'Date',
                'type' => 'input',
                'class' => 'datepicker',
                'rules' => array(
                
                ),  
            ),
            'time' => array(
                'label' => 'Time',
                'type' => 'input',
                'rules' => array(
                
                ),  
            ),
            'address' => array(
                'label' => 'Address',
                'type' => 'input',
                'rules' => array(
                
                ),  
            ),
            'image'  => array(
                'label' => 'Post Image',
                'type' => 'file_image',
                'class' => 'fileupload',
                'rules' => array(    
                ),
            ),
        )
    ),
    'blog_news' => array(
        'label' => 'News',
        'type' => 'multiple',
        'fields' => array(
        	'title' => array(
                'label' => 'Post Title',
                'type' => 'input',
                'class' => 'span8',
                'rules' => array(
                
                ),  
            ),
            'urltitle' => array(
                'label' => 'URL',
                'type' => 'input',
                'class' => 'span8',
                'rules' => array(
                	'not_empty' => 'true'
                ),
                'table_display' => false
            ),
            'post_content' => array(
                'label' => 'Post Content',
                'type' => 'textarea',
                'rules' => array(
                
                ),
                'class' => 'ckeditor',
                
            ),
            'date' => array(
                'label' => 'Posted On',
                'type' => 'input',
                'class' => 'datepicker',
                'rules' => array(
                
                ),  
            ),
            'author' => array(
                'label' => 'Author',
                'type' => 'input',
                'rules' => array(
                
                ),  
            ),
            'image'  => array(
                'label' => 'Post Image',
                'type' => 'file_image',
                'class' => 'fileupload',
                'rules' => array(    
                ),
            ),
        )
    ),
    'blog_press' => array(
        'label' => 'Press',
        'type' => 'multiple',
        'fields' => array(
        	'title' => array(
                'label' => 'Post Title',
                'type' => 'input',
                'class' => 'span8',
                'rules' => array(
                
                ),  
            ),
            'urltitle' => array(
                'label' => 'URL',
                'type' => 'input',
                'class' => 'span8',
                'rules' => array(
                	'not_empty' => 'true'
                ), 
                'table_display' => false
            ),
            'post_content' => array(
                'label' => 'Post Content',
                'type' => 'textarea',
                'rules' => array(
                
                ),
                'class' => 'ckeditor',
                
            ),
            'date' => array(
                'label' => 'Posted On',
                'type' => 'input',
                'class' => 'datepicker',
                'rules' => array(
                
                ),  
            ),
            'author' => array(
                'label' => 'Author',
                'type' => 'input',
                'rules' => array(
                
                ),  
            ),
            'image'  => array(
                'label' => 'Post Image',
                'type' => 'file_image',
                'class' => 'fileupload',
                'rules' => array(    
                ),
            ),
        )
    ),
    
    'patients_steps' => array(
        'label' => 'Steps',
        'type' => 'multiple',
        'fields' => array(
        	'step_number' => array(
                'label' => 'Step #',
                'type' => 'input',
                'rules' => array(
                    
                ),
                
            ),
            'title' => array(
                'label' => 'Title',
                'type' => 'input',
                'rules' => array(
                    
                ),
                
            ),
            'heading' => array(
                'label' => 'Heading',
                'type' => 'input',
                'rules' => array(
                    
                ),
          
            ),
            'description' => array(
                'label' => 'Description',
                'type' => 'textarea',
                'class' => 'ckeditor',
                'rules' => array(
                    
                ),
          
            ),
            'image' => array(
                'label' => 'Large Image',
                'type' => 'file_image',
                'class' => 'fileupload',
                'rules' => array(
                    
                ),
                'table_display' => false
            ),
             'image2' => array(
                'label' => 'Small Image',
                'type' => 'file_image',
                'class' => 'fileupload',
                'rules' => array(
                    
                ),
                'table_display' => false
            ),
            'small_image_link' => array(
                'label' => 'Link for small image',
                'type' => 'input',
                'rules' => array(    
                ),
                'table_display' => false
            ),
            'image3' => array(
                'label' => 'Box Image (used on landing page)',
                'type' => 'file_image',
                'class' => 'fileupload',
                'rules' => array(
                    
                ),
                'table_display' => false
            ),
            'video' => array(
                'label' => 'Video Link',
                'type' => 'input',            
                'rules' => array(
                    
                ),
                'table_display' => false
            ),
        )
    ),
    
	'professionals_steps' => array(
        'label' => 'Steps',
        'type' => 'multiple',
        'fields' => array(
        	'step_number' => array(
                'label' => 'Step #',
                'type' => 'input',
                'rules' => array(
                    
                ),
                
            ),
            'title' => array(
                'label' => 'Title',
                'type' => 'input',
                'rules' => array(
                    
                ),
                
            ),
            'heading' => array(
                'label' => 'Heading',
                'type' => 'input',
                'rules' => array(
                    
                ),
          
            ),
            'description' => array(
                'label' => 'Description',
                'type' => 'textarea',
                'class' => 'ckeditor',
                'rules' => array(
                    
                ),
          
            ),
            'image' => array(
                'label' => 'Large Image',
                'type' => 'file_image',
                'class' => 'fileupload',
                'rules' => array(
                    
                ),
                'table_display' => false
            ),
             'image2' => array(
                'label' => 'Small Image',
                'type' => 'file_image',
                'class' => 'fileupload',
                'rules' => array(
                    
                ),
                'table_display' => false
            ),
            'small_image_link' => array(
                'label' => 'Link for small image',
                'type' => 'input',
                'rules' => array(    
                ),
                'table_display' => false
            ),
             'image3' => array(
                'label' => 'Box Image (used on landing page)',
                'type' => 'file_image',
                'class' => 'fileupload',
                'rules' => array(
                    
                ),
                'table_display' => false
            ),
            'video' => array(
                'label' => 'Video Link',
                'type' => 'input',            
                'rules' => array(
                    
                ),
                'table_display' => false
            ),
        )
    ),
    'pulse' => array(
        'label' => 'The Pulse',
        'type' => 'multiple',
        'fields' => array(
            'title' => array(
                'label' => 'Title',
                'type' => 'input',
                'rules' => array(
                    
                ),
                'class' => 'span6'
            ),
            'description' => array(
                'label' => 'Description',
                'type' => 'input',
                'rules' => array(
                    
                ),
            ),
            'link' => array(
                'label' => 'Link',
                'type' => 'input',
                'rules' => array(
                    
                ),
            ),
            'image' => array(
                'label' => 'Image',
                'type' => 'file_image',
                'class' => 'fileupload',
                'rules' => array(
                ),
            ),         
        )
    ), 
	
    'homepage_boxes' => array(
        'label' => 'Homepage Boxes',
        'type' => 'multiple',
        'fields' => array(
            'title' => array(
                'label' => 'Title',
                'type' => 'input',
                'rules' => array(
                    
                ),
            ),
            'blurb' => array(
                'label' => 'Blurb',
                'type' => 'input',
                'rules' => array(
                    
                ),
            ),
            'link' => array(
                'label' => 'Link',
                'type' => 'input',
                'rules' => array(
                    
                ),
            ),
            'description' => array(
                'label' => 'Description',
                'type' => 'textarea',
                'class' => 'ckeditor',
                'rules' => array(
                    
                ),
            ),
            'image' => array(
                'label' => 'Image',
                'type' => 'file_image',
                'class' => 'fileupload',
                'rules' => array(
                ),
            ),         
        )
    ),
    'slider' => array(
        'label' => 'Slider',
        'type' => 'multiple',
        'fields' => array(
        	'type' => array(
                'label' => 'Type',
                'type' => 'select',
                'options' => array(
					'image' => 'Image', 'video' => 'Video'
				),
                'rules' => array(
                ),
            ),
            'title' => array(
                'label' => 'Title',
                'type' => 'input',
                'rules' => array(
                ),
            ),
            'link' => array(
                'label' => 'Link',
                'type' => 'input',
                'rules' => array(
                ),
            ),
            'description' => array(
                'label' => 'Description',
                'type' => 'textarea',
                'class' => 'ckeditor',
                'rules' => array(
                   
                ),
            ),
            'image' => array(
                'label' => 'Image',
                'type' => 'file_image',
                'class' => 'fileupload',
                'rules' => array(
                ),
            ), 
            'Video' => array(
                'label' => 'Video (vimeo video id)',
                'type' => 'input',
                'rules' => array(
                ),
            ),        
        )
    ),
    'locations' => array(
        'label' => 'PSC Locations',
        'type' => 'multiple',
        'fields' => array(
            'name' => array(
                'label' => 'Name',
                'type' => 'input',
                'rules' => array(
                ),
            ),
            'location' => array(
                'label' => 'Location',
                'type' => 'input',
                'table_display' => false,
                'rules' => array(
                ),
            ),
            'address' => array(
                'label' => 'Address',
                'type' => 'input',
                'rules' => array(
                   
                ),
            ),
            'city' => array(
                'label' => 'City',
                'type' => 'input',
                'rules' => array(
                   
                ),
            ),
            'state' => array(
                'label' => 'State',
                'type' => 'input',
                'rules' => array(
                   
                ),
            ), 
            'zip' => array(
                'label' => 'Zip',
                'type' => 'input',
                'rules' => array(
                   
                ),
            ),
            'phone' => array(
                'label' => 'Phone',
                'type' => 'input',
                'rules' => array(
                   
                ),
            ),
            'fax' => array(
                'label' => 'Fax',
                'type' => 'input',
                'table_display' => false,
                'rules' => array(
                   
                ),
            ),
            'hours_week' => array(
                'label' => 'Office Hours Work Week',
                'type' => 'input',
                'table_display' => false,
                'rules' => array(
                   
                ),
            ),
            'hours_weekend' => array(
                'label' => 'Office Hours Weekend',
                'type' => 'input',
                'table_display' => false,
                'rules' => array(
                   
                ),
            ),
        )
    ),
    'tips' => array(
        'label' => 'Guard A Heart Tips',
        'type' => 'multiple',
        'fields' => array(
            'tip' => array(
                'label' => 'Tip',
                'type' => 'input',
                'rules' => array(
                    
                ),
                'class' => 'span6'
            ),
        )
    ),
    'aviir_corporate' => array(
        'label' => 'Aviir Corporate',
        'type' => 'single',
        'fields' => array(
            'address' => array(
                'label' => 'Address',
                'type' => 'textarea',
                'rules' => array(
                    
                ),
                'class' => 'span6 ckeditor'
            ),
            'phone' => array(
                'label' => 'Phone',
                'type' => 'input',
                'rules' => array(
                    
                ),
            ),
            'fax' => array(
                'label' => 'Fax',
                'type' => 'input',
                'rules' => array(
                    
                ),
            ),
            'media_inquiries' => array(
                'label' => 'Media Inquiries',
                'type' => 'input',
                'rules' => array(
                    
                ),
            ),
            'investor_relations' => array(
                'label' => 'Investor Relations:',
                'type' => 'input',
                'rules' => array(
                    
                ),
            ),
        )
    ),
    'aviir_email' => array(
        'label' => 'Aviir Email',
        'type' => 'single',
        'fields' => array(
            'clientservice' => array(
                'label' => 'Client Services',
                'type' => 'input',
                'rules' => array(
                    
                ),
            ),
            'billing' => array(
                'label' => 'Billing Department',
                'type' => 'input',
                'rules' => array(
                    
                ),
            ),
            'marketing' => array(
                'label' => 'Marketing',
                'type' => 'input',
                'rules' => array(
                    
                ),
            ),
            'sales' => array(
                'label' => 'Sales',
                'type' => 'input',
                'rules' => array(
                    
                ),
            ),
            
        )
    ),
    'aviir_client_services' => array(
        'label' => 'Client Services',
        'type' => 'single',
        'fields' => array(
            'phone' => array(
                'label' => 'Phone',
                'type' => 'input',
                'rules' => array(
                    
                ),
            ),
            'fax' => array(
                'label' => 'Fax',
                'type' => 'input',
                'rules' => array(
                    
                ),
            ),
            'email' => array(
                'label' => 'Email',
                'type' => 'input',
                'rules' => array(
                    
                ),
            ),
            'hours' => array(
                'label' => 'Hours of Operation',
                'type' => 'textarea',
                'class' => 'ckeditor',
                'rules' => array(
                    
                ),
            ),
        )
    ),
	'professionals_pages' => array(
        'label' => 'Pages',
        'type' => 'multiple',
        'fields' => array(
        	'menu_order' => array(
                'label' => 'Menu Order',
                'type' => 'input',
                'rules' => array(
                    
                ),
                
            ),
            'title' => array(
                'label' => 'Title',
                'type' => 'input',
                'rules' => array(
                    
                ),
                
            ),
            'heading' => array(
                'label' => 'Heading',
                'type' => 'input',
                'rules' => array(
                    
                ),
          
            ),
            'description' => array(
                'label' => 'Description',
                'type' => 'textarea',
                'class' => 'ckeditor',
                'rules' => array(
                    
                ),
          
            ),
            'image' => array(
                'label' => 'Upload Image',
                'type' => 'file_image',
                'class' => 'fileupload',
                'rules' => array(
                    
                ),
                'table_display' => false
            ),
            'video' => array(
                'label' => 'Video Link',
                'type' => 'input',            
                'rules' => array(
                    
                ),
                'table_display' => false
            ),
            'menu_show' => array(
                'label' => 'Show in Menu',
                'type' => 'toggle',
                'rules' => array(
                    
                ),
                'table_display' => false
            )
        ),
    ),
    
	'ADL_pages' => array(
        'label' => 'Pages',
        'type' => 'multiple',
        'fields' => array(
        	'menu_order' => array(
                'label' => 'Menu Order',
                'type' => 'input',
                'rules' => array(
                    
                ),
                
            ),
            'title' => array(
                'label' => 'Title',
                'type' => 'input',
                'rules' => array(
                    
                ),
                
            ),
            'heading' => array(
                'label' => 'Heading',
                'type' => 'input',
                'rules' => array(
                    
                ),
          
            ),
            'description' => array(
                'label' => 'Description',
                'type' => 'textarea',
                'class' => 'ckeditor',
                'rules' => array(
                    
                ),
          
            ),
            'image' => array(
                'label' => 'Upload Image',
                'type' => 'file_image',
                'class' => 'fileupload',
                'rules' => array(
                    
                ),
                'table_display' => false
            ),
            'video' => array(
                'label' => 'Video Link',
                'type' => 'input',            
                'rules' => array(
                    
                ),
                'table_display' => false
            ),
            'menu_show' => array(
                'label' => 'Show in Menu',
                'type' => 'toggle',
                'rules' => array(
                    
                ),
                'table_display' => false
            )
        )
    ),
    
	'patients_pages' => array(
        'label' => 'Pages',
        'type' => 'multiple',
        'fields' => array(
        	'menu_order' => array(
                'label' => 'Menu Order',
                'type' => 'input',
                'rules' => array(
                    
                ),
                
            ),
            'title' => array(
                'label' => 'Title',
                'type' => 'input',
                'rules' => array(
                    
                ),
                
            ),
            'heading' => array(
                'label' => 'Heading',
                'type' => 'input',
                'rules' => array(
                    
                ),
          
            ),
            'description' => array(
                'label' => 'Description',
                'type' => 'textarea',
                'class' => 'ckeditor',
                'rules' => array(
                    
                ),
          
            ),
            'image' => array(
                'label' => 'Upload Image',
                'type' => 'file_image',
                'class' => 'fileupload',
                'rules' => array(
                    
                ),
                'table_display' => false
            ),
            'video' => array(
                'label' => 'Video Link',
                'type' => 'input',            
                'rules' => array(
                    
                ),
                'table_display' => false
            ),
            'menu_show' => array(
                'label' => 'Show in Menu',
                'type' => 'toggle',
                'rules' => array(
                    
                ),
                'table_display' => false
            )
        )
    ),
    
	'company_pages' => array(
        'label' => 'Pages',
        'type' => 'multiple',
        'fields' => array(
        	'menu_order' => array(
                'label' => 'Menu Order',
                'type' => 'input',
                'rules' => array(
                   
                ),
                
            ),
            'title' => array(
                'label' => 'Title',
                'type' => 'input',
                'rules' => array(
                    
                ),
                
            ),
            'heading' => array(
                'label' => 'Heading',
                'type' => 'input',
                'rules' => array(
                    
                ),
          
            ),
            'description' => array(
                'label' => 'Description',
                'type' => 'textarea',
                'class' => 'ckeditor',
                'rules' => array(
                    
                ),
          
            ),
            'image' => array(
                'label' => 'Upload Image',
                'type' => 'file_image',
                'class' => 'fileupload',
                'rules' => array(
                    
                ),
                'table_display' => false
            ),
            'video' => array(
                'label' => 'Video Link',
                'type' => 'input',            
                'rules' => array(
                    
                ),
                'table_display' => false
            ),
            'menu_show' => array(
                'label' => 'Show in Menu',
                'type' => 'toggle',
                'rules' => array(
                    
                ),
                'table_display' => false
            )
        )
    ),
    'ADL_tests' => array(
        'label' => 'Tests',
        'type' => 'multiple',
        'fields' => array(
            'testcode' => array(
                'label' => 'Test Code',
                'type' => 'input',
                'rules' => array(   
                ),  
            ), 
            'testname' => array(
                'label' => 'Test Name',
                'type' => 'input',
                'rules' => array(   
                ),  
            ), 
            'cptcode' => array(
                'label' => 'CPT Code',
                'type' => 'input',
                'rules' => array(   
                ),  
            ), 
            'units' => array(
                'label' => 'Units',
                'type' => 'input',
                'rules' => array(   
                ),  
            ), 
            'category' => array(
                'label' => 'Category',
                'type' => 'select',
                'options' => array(
					'blood_bank' => 'Blood Bank',
					'chemistry'  => 'Chemistry',
					'coagulation' => 'Coagulation',
					'hematology' => 'Hematology',
					'molecular' => 'Molecular',
					'special_chemistry' => 'Special Chemistry',
					'urinalysis' => 'Urinalysis',
				),
                'rules' => array(   
                ),  
            ), 
            'specimen_required' => array(
                'label' => 'Specimen Required',
                'type' => 'select',
                'options' => array(
					'serum_separator_tube_sst' => 'Serum Separator Tube (SST®)',
					'edta_lavendertoptube_'  => 'EDTA (Lavender‐Top) Tube',
					'full_edta_lavendertop_tube' => 'Full EDTA (Lavender-Top) Tube',
					'random_urine' => 'Random Urine (UR)',
					'24_hour_urine' => '24 Hour Urine (UR)',
					'sodium_heparin_light_bluetop_tube' => 'Sodium Heparin (Light Blue-Top) Tube',
					'sodium_citrate_light_bluetop_tube' => 'Sodium Citrate (Light Blue-Top) Tube',
					'redtop_tube_no_gel' => 'Red-Top Tube (No Gel)'
				),
                'rules' => array(   
                ),  
            ),  
        )
    ),
    
	'company_boardmembers' => array(
        'label' => 'Board Members',
        'type' => 'multiple',
        'fields' => array(
        	'name' => array(
                'label' => 'Name',
                'type' => 'input',
                'rules' => array(
                
                ),  
            ),
            'description' => array(
                'label' => 'Description',
                'type' => 'textarea',
                'rules' => array(
                
                ),
                'class' => 'ckeditor',
                
            ),
            'image'  => array(
                'label' => 'Upload Image',
                'type' => 'file_image',
                'class' => 'fileupload',
                'rules' => array(    
                ),
            ),
        )
    ),
    
    'privacy' => array(
        'label' => 'Privacy Policy',
        'type' => 'single',
        'fields' => array(
            'privacy_policy' => array(
                'label' => 'Privacy Policy',
                'type' => 'textarea',
                'rules' => array(
                
                ), 
                'class' => 'ckeditor'
            ),
        )
    ),
    'terms' => array(
        'label' => 'Terms and Conditions',
        'type' => 'single',
        'fields' => array(
            'terms_of_use' => array(
                'label' => 'Terms and Conditions',
                'type' => 'textarea',
                'rules' => array(
                
                ), 
                'class' => 'ckeditor'
            ),
        )
    ),
    'ADL_boxes' => array(
        'label' => 'ADL Boxes',
        'type' => 'multiple',
        'fields' => array(
            'title' => array(
                'label' => 'Title',
                'type' => 'input',
                'rules' => array(
                    
                ),
            ),
            'blurb' => array(
                'label' => 'Blurb',
                'type' => 'input',
                'rules' => array(
                    
                ),
            ),
            'link' => array(
                'label' => 'Link',
                'type' => 'input',
                'rules' => array(
                    
                ),
            ),
            'description' => array(
                'label' => 'Description',
                'type' => 'textarea',
                'class' => 'ckeditor',
                'rules' => array(
                    
                ),
            ),
            'image' => array(
                'label' => 'Image',
                'type' => 'file_image',
                'class' => 'fileupload',
                'rules' => array(
                ),
            ),         
        )
    ),
    'professionals_blood_draw' => array(
        'label' => 'Blood Draw',
        'type' => 'single',
        'fields' => array(
            'title' => array(
                'label' => 'Title',
                'type' => 'input',
                'rules' => array(
                    
                ),
                'class' => 'span8'
                
            ),
            'heading' => array(
                'label' => 'Heading',
                'type' => 'input',
                'rules' => array(
                    
                ),
                'class' => 'span8'
            ),
            'description' => array(
                'label' => 'Description',
                'type' => 'textarea',
                'rules' => array(
                
                ), 
                'class' => 'ckeditor'
            ),
            'closing' => array(
                'label' => 'Closing text',
                'type' => 'textarea',
                'rules' => array(
                
                ), 
                'class' => 'ckeditor'
            ),
            'email_subject' => array(
                'label' => 'Confirmation Email Subject',
                'type' => 'input',
                'rules' => array(
                    
                ),
                'class' => 'span8'
                
            ),
            'email_body' => array(
                'label' => 'Confirmation Email Body',
                'type' => 'textarea',
                'rules' => array(
                
                ), 
                'class' => 'ckeditor'
            ),
        )
    ),
    'ADL_blood_draw' => array(
        'label' => 'Blood Draw',
        'type' => 'single',
        'fields' => array(
            'title' => array(
                'label' => 'Title',
                'type' => 'input',
                'rules' => array(
                    
                ),
                'class' => 'span8'
                
            ),
            'heading' => array(
                'label' => 'Heading',
                'type' => 'input',
                'rules' => array(
                    
                ),
                'class' => 'span8'
            ),
            'description' => array(
                'label' => 'Description',
                'type' => 'textarea',
                'rules' => array(
                
                ), 
                'class' => 'ckeditor'
            ),
            'closing' => array(
                'label' => 'Closing text',
                'type' => 'textarea',
                'rules' => array(
                
                ), 
                'class' => 'ckeditor'
            ),
            'email_subject' => array(
                'label' => 'Confirmation Email Subject',
                'type' => 'input',
                'rules' => array(
                    
                ),
                'class' => 'span8'
                
            ),
            'email_body' => array(
                'label' => 'Confirmation Email Body',
                'type' => 'textarea',
                'rules' => array(
                
                ), 
                'class' => 'ckeditor'
            ),
        )
    ),
    'patients_blood_draw' => array(
        'label' => 'Blood Draw',
        'type' => 'single',
        'fields' => array(
            'title' => array(
                'label' => 'Title',
                'type' => 'input',
                'rules' => array(
                    
                ),
                'class' => 'span8'
                
            ),
            'heading' => array(
                'label' => 'Heading',
                'type' => 'input',
                'rules' => array(
                    
                ),
                'class' => 'span8'
            ),
            'description' => array(
                'label' => 'Description',
                'type' => 'textarea',
                'rules' => array(
                
                ), 
                'class' => 'ckeditor'
            ),
            'closing' => array(
                'label' => 'Closing text',
                'type' => 'textarea',
                'rules' => array(
                
                ), 
                'class' => 'ckeditor'
            ),
            'email_subject' => array(
                'label' => 'Confirmation Email Subject',
                'type' => 'input',
                'rules' => array(
                    
                ),
                'class' => 'span8'
                
            ),
            'email_body' => array(
                'label' => 'Confirmation Email Body',
                'type' => 'textarea',
                'rules' => array(
                
                ), 
                'class' => 'ckeditor'
            ),
        )
    ),
);
