<?php 
	if (count($notifications) > 0)
	{
?>
<div>
<?php foreach($notifications as $type => $notification): ?>
	<?php if ( ! empty($notification)): ?>

	<?php foreach ($notification as $notice): ?>
	<div class="alert alert-<?php echo $type ?>">
		<strong><?php echo __(UTF8::ucfirst($type)) ?></strong> - 
		
		<?php 
			if ($notice['message'] !== NULL)
			{
				echo $notice['message'];
			}
		?>

		<?php if ( ! empty($notice['items'])): ?>
		<ul>
			<?php foreach($notice['items'] as $item): ?>
			<li><?php echo __($item) ?></li>
			<?php endforeach; ?>
		</ul>
		<?php endif; ?>
	</div>
	<?php endforeach; ?>

	<?php endif; ?>
<?php endforeach; ?>
</div>
<?php 
	}
?>