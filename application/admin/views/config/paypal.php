<h1>Paypal Config</h1>

<?php
    echo Form::open('/config/paypal', array('class' => 'well'));
    
    foreach ($configs as $key => $config)
    {
            echo '<div>';
            echo Form::label('paypal['.$key.']', ucwords(str_replace('_', ' ', $key)));
            echo Form::input('paypal['.$key.']', $config, array('class' => 'span3'));
            echo '</div>';
    }
    
    echo '<br/>';
    echo Form::submit(NULL, 'Save', array('class' => 'btn btn-primary'));
    echo '&nbsp;&nbsp;or&nbsp;&nbsp;';
    echo HTML::anchor('/config/paypal', 'cancel');
    
    echo Form::close();
?>