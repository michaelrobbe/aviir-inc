<h1>Google Config</h1>

<?php
    echo Form::open('/config/google', array('class' => 'well'));
    
    foreach ($configs as $key => $config)
    {
            echo '<div>';
            echo Form::label('google['.$key.']', ucwords(str_replace('_', ' ', $key)));
            echo Form::input('google['.$key.']', $config, array('class' => 'span3'));
            echo '</div>';
    }
    
    echo '<br/>';
    echo Form::submit(NULL, 'Save', array('class' => 'btn btn-primary'));
    echo '&nbsp;&nbsp;or&nbsp;&nbsp;';
    echo HTML::anchor('/config/google', 'cancel');
    
    echo Form::close();
?>