<h1>Facebook Config</h1>

<?php
    echo Form::open('/config/facebook', array('class' => 'well'));
    
    foreach ($configs as $key => $config)
    {
            echo '<div>';
            echo Form::label('facebook['.$key.']', ucwords(str_replace('_', ' ', $key)));
            echo Form::input('facebook['.$key.']', $config, array('class' => 'span3'));
            echo '</div>';
    }
    
    echo '<br/>';
    echo Form::submit(NULL, 'Save', array('class' => 'btn btn-primary'));
    echo '&nbsp;&nbsp;or&nbsp;&nbsp;';
    echo HTML::anchor('/config/facebook', 'cancel');
    
    echo Form::close();
?>