<h1>Twitter Config</h1>
<br/>
<?php
	echo Form::open('/config/twitter', array('class' => 'well'));
	
	foreach ($configs as $key => $config)
	{
		echo '<div>';
		echo Form::label('twitter['.$key.']', ucwords(str_replace('_', ' ', $key)));
		echo Form::input('twitter['.$key.']', $config, array('class' => 'span3'));
		echo '</div>';
	}
    
	echo '<br/>';
	echo Form::submit(NULL, 'Save', array('class' => 'btn btn-primary'));
	echo '&nbsp;&nbsp;or&nbsp;&nbsp;';
    echo HTML::anchor('/config/twitter', 'cancel');
	
	echo Form::close();
?>