<script type="text/javascript">
$(document).ready(function() 
    { 
        $("#video_table").tablesorter(); 
    } 
); 
</script>
<h1><?php echo $content_title ?></h1>
<div class="well">
	<div class="form medium_form">
		<?php
			echo Form::open('/asset/video/', array('method' => 'get'));
		?>
		<div class="form_field">
			<?php 
				echo Form::label('form[title]', 'Title');
				echo Form::input('form[title]', $video['title']);
			?>
		</div>
		<div class="buttons">
			<?php echo Form::button(NULL, 'Search', array('type' => 'submit', 'class' => 'btn btn-primary')); ?>
			<?php echo HTML::anchor('/asset/upload', 'Add Video', array('type' => 'submit', 'class' => 'btn btn-success')); ?>
		</div>
		<?php
            echo Form::close();
            echo '<br/>';
            echo $view_all_button;
        ?>
	</div>
	<div style="text-align:center;">
        <?php echo $pagination; ?>
    </div>
	<table class="table table-striped" id="video_table">
		<thead>
			<tr>
				<th><?php echo Format::create_sort_link('ID', 'id', $query_array, 'asset/video') ?></th>
				<th>Preview</th>
				<th><?php echo Format::create_sort_link('Title', 'title', $query_array, 'asset/video') ?></th>
                <th><?php echo Format::create_sort_link('Description', 'description', $query_array, 'asset/video') ?></th>
                <th><a href="#">Views</a></th>
                <th><a href="#">Shares</a></th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<?php
				foreach ($videos as $video)
				{
					echo '<tr class="table_row '.Text::alternate('odd', 'even').'">';
					echo '<td>'.$video->id.'</td>';
                    echo '<td>'.HTML::anchor('/asset/edit/'.$video->id, HTML::image($video->files->where('type', '=', 'image_tiny')->find()->url)).'</td>';
					echo '<td>'.Text::limit_chars($video->title, 50).'</td>';
					echo '<td>'.Text::limit_chars($video->description, 75).'</td>';
					echo '<td>'.$video->get_views().'</td>';
					echo '<td>'.$video->get_shares().'</td>';
					echo '<td style="text-align:right;" class="btn-group">';
					echo HTML::anchor('/asset/edit/'.$video->id, 'Edit', array('alt' => 'Edit', 'class' => 'btn btn-small'));
					echo HTML::anchor('/asset/delete/'.$video->id, 'Delete', array('alt' => 'Delete', 'class' => 'delete btn btn-small btn-danger'));
					echo '</td>';
					echo '</tr>';
				}
			?>
		</tbody>
	</table>
</div>

<div class="modal hide dialog" id="delete_dialog">
  <div class="modal-header">
    <a class="close" data-dismiss="modal">×</a>
    <h3>Confirmation Required</h3>
  </div>
  <div class="modal-body">
    <p>Are you sure you want to delete this?</p>
  </div>
  <div class="modal-footer">
    <a href="#" class="btn modal_hide">No</a>
    <a href="#" class="btn btn-primary modal_delete_yes_button">Yes</a>
  </div>
</div>