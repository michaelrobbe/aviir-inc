<?php
    echo HTML::style('media/admin/css/jquery.tagit.css');
    echo HTML::style('media/admin/css/multi-select.css');
?>

<h1><?php echo $content_title ?></h1>
<div class="well">
    <div class="form medium_form">
        <?php
            echo Form::open('/asset/save');
            echo Form::hidden('asset[id]', $asset->id);
        ?>
        <div class="form_field">
            <?php echo HTML::anchor($asset->files->where('type', '=', 'image_large')->find()->url, HTML::image($asset->files->where('type', '=', 'image_medium')->find()->url), array('target' => '_BLANK')) ?>
        </div>
        <div class="form_field">
            <?php 
                echo Form::label('asset[title]', 'Title');
                echo Form::input('asset[title]', $asset->title, array('class' => 'span6'));
            ?>
        </div>
        <div class="form_field">
            <?php 
                echo Form::label('asset[description]', 'Description');
                echo Form::textarea('asset[description]', $asset->description, array('class' => 'span8'));
            ?>
        </div>
        <div class="form_field">
            <?php
                $tags_string = '';
                foreach ($asset->tags->find_all() as $tag)
                {
                    $tags_string.= $tag->name.',';
                }
                echo Form::label('tags', 'Tags');
                echo Form::input('tags', $tags_string, array('class' => 'span6 tag_field'));
            ?>
        </div>
        <div class="form_field">
        <?php
            echo Form::label('categories[]', 'Categories');
            echo Form::select('categories[]', $categories, $selected_categories, array('class' => 'category_select', 'multiple' => 'multiple'));
        ?>
        </div>
        <div class="buttons">
            <?php echo Form::button(NULL, 'Save', array('type' => 'submit', 'class' => 'btn btn-primary')); ?>
            or
            <?php echo HTML::anchor('/asset', 'Cancel', array('class' => '')) ?>
        </div>
        <?php echo Form::close(); ?>
    </div>
</div>

<?php
    if ($asset->type == 'video')
    {
?>
<div class="well">
    <h3>Image Replacement</h3>
    <?php
        echo Form::open('/asset/replace_image', array('enctype' => 'multipart/form-data'));
        echo Form::hidden('asset_id', $asset->id);
        
        echo '<br/>';
        echo Form::file('image_replacement', array('class' => 'span6 file_field'));
        echo '<br/><br/>';
        echo Form::button(NULL, 'Save', array('type' => 'submit', 'class' => 'btn btn-primary'));
        
        echo Form::close();
    ?>
</div>
<?php
    }
?>

<div class="well">
    <h3>Files</h3>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>ID</th>
                <th>Type</th>
                <th>URL</th>
            </tr>
        </thead>
        <tbody>
            <?php
                foreach ($asset->files->find_all() as $file)
                {
                    $file_type = str_replace('_', ' ', $file->type);
                    $file_type = ucwords($file_type);
                    echo '<tr>';
                    echo '<td>'.$file->id.'</td>';
                    echo '<td>'.$file_type.'</td>';
                    echo '<td>'.HTML::anchor($file->url, $file->url, array('target' => '_BLANK')).'</td>';  
                    echo '</tr>';
                }
            ?>
        </tbody>
    </table>
</div>