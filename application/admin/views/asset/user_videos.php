<h1><?php echo $content_title ?></h1>
<div class="well">
    <div class="form medium_form">
        <?php
            echo Form::open('/asset/image/', array('method' => 'get'));
        ?>
        <div class="form_field">
            <?php 
                echo Form::label('form[title]', 'Title');
                echo Form::input('form[title]', $image['title']);
            ?>
        </div>
        <div class="buttons">
            <?php echo Form::button(NULL, 'Search', array('type' => 'submit', 'class' => 'btn btn-primary')); ?>
        </div>
        <?php echo Form::close(); ?>
        <br/>
        <?php echo $view_all; ?>
    </div>
    <br/>
    <div>
        <?php if(!(isset($_GET["view_all"]))) echo $pagination; ?>
    </div>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>ID</th>
                <th>Preview</th>
                <th><?php echo $thead_title; ?></th>
                <th>Description</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php
                foreach ($images as $image)
                {
                    echo '<tr class="table_row '.Text::alternate('odd', 'even').'">';
                    echo '<td>'.$image->id.'</td>';
                    echo '<td>'.HTML::anchor('/asset/edit/'.$image->id,HTML::image($image->files->where('type', '=', 'image_tiny')->find()->url)).'</td>';
                    echo '<td>'.Text::limit_chars($image->title, 50).'</td>';
                    echo '<td>'.Text::limit_chars($image->description, 75).'</td>';
                    echo '<td style="text-align:right;" class="btn-group">';
                    echo HTML::anchor('/asset/edit/'.$image->id, 'Edit', array('alt' => 'Edit', 'class' => 'btn btn-small'));
                    echo HTML::anchor('/asset/delete/'.$image->id, 'Delete', array('alt' => 'Delete', 'class' => 'delete btn btn-small btn-danger'));
                    echo '</td>';
                    echo '</tr>';
                }
            ?>
        </tbody>
    </table>
</div>

<div class="modal hide dialog" id="delete_dialog">
  <div class="modal-header">
    <a class="close" data-dismiss="modal">×</a>
    <h3>Confirmation Required</h3>
  </div>
  <div class="modal-body">
    <p>Are you sure you want to delete this?</p>
  </div>
  <div class="modal-footer">
    <a href="#" class="btn modal_hide">No</a>
    <a href="#" class="btn btn-primary modal_delete_yes_button">Yes</a>
  </div>
</div>