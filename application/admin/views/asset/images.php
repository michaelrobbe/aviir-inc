<h1><?php echo $content_title ?></h1>
<div class="well">
	<div class="form medium_form">
		<?php
			echo Form::open('/asset/image/', array('method' => 'get'));
		?>
		<div class="form_field">
			<?php 
				echo Form::label('form[title]', 'Title');
				echo Form::input('form[title]', $image['title']);
			?>
		</div>
		<div class="buttons">
			<?php echo Form::button(NULL, 'Search', array('type' => 'submit', 'class' => 'btn btn-primary')); ?>
			<?php echo HTML::anchor('/asset/upload', 'Add Image', array('type' => 'submit', 'class' => 'btn btn-success')); ?>
		</div>
		<?php
            echo Form::close();
            echo '<br/>';
            echo $view_all_button;
        ?>
    </div>
    <div style="text-align:center;">
        <?php echo $pagination; ?>
    </div>
	<table class="table table-striped">
		<thead>
			<tr>
				<th><?php echo Format::create_sort_link('ID', 'id', $query_array, 'asset/image') ?></th>
                <th>Preview</th>
                <th><?php echo Format::create_sort_link('Title', 'title', $query_array, 'asset/image') ?></th>
                <th><?php echo Format::create_sort_link('Description', 'description', $query_array, 'asset/image') ?></th>
                <th></th>
			</tr>
		</thead>
		<tbody>
			<?php
				foreach ($images as $image)
				{
					echo '<tr class="table_row '.Text::alternate('odd', 'even').'">';
					echo '<td>'.$image->id.'</td>';
                    echo '<td>'.HTML::anchor('/asset/edit/'.$image->id,HTML::image($image->files->where('type', '=', 'image_tiny')->find()->url)).'</td>';
					echo '<td>'.Text::limit_chars($image->title, 50).'</td>';
					echo '<td>'.Text::limit_chars($image->description, 75).'</td>';
					echo '<td style="text-align:right;" class="btn-group">';
					echo HTML::anchor('/asset/edit/'.$image->id, 'Edit', array('alt' => 'Edit', 'class' => 'btn btn-small'));
					echo HTML::anchor('/asset/delete/'.$image->id, 'Delete', array('alt' => 'Delete', 'class' => 'delete btn btn-small btn-danger'));
					echo '</td>';
					echo '</tr>';
				}
			?>
		</tbody>
	</table>
</div>

<div class="modal hide dialog" id="delete_dialog">
  <div class="modal-header">
    <a class="close" data-dismiss="modal">×</a>
    <h3>Confirmation Required</h3>
  </div>
  <div class="modal-body">
    <p>Are you sure you want to delete this?</p>
  </div>
  <div class="modal-footer">
    <a href="#" class="btn modal_hide">No</a>
    <a href="#" class="btn btn-primary modal_delete_yes_button">Yes</a>
  </div>
</div>