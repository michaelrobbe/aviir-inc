<div class="well">
	<h3>Create A New Category</h3>
	<div class="form medium_form">
		<?php echo Form::open('/category/add', array('class' => 'category_form')) ?>
		<div class="form_field">
			<?php
				$no_parent = array(0 => 'None');
				$select_options = Arr::merge($no_parent, $parent_ids);
				echo Form::hidden('form[id]', $category->id);
				echo Form::label('form[parent]', 'Parent Category');
				echo Form::select('form[parent]', $select_options, $category->parent_id);
			?>
		</div>
		<div class="form_field">
			<?php
				echo Form::label('form[name]', 'Category Name');
				echo Form::input('form[name]', $category->name);
			?>
		</div>
		<div class="form_field">
			<?php
				echo Form::label('form[description]', 'Description');
				echo Form::textarea('form[description]', $category->description);
			?>
		</div>
		<div class="buttons">
			<?php echo Form::button(NULL, 'Save', array('type' => 'submit', 'class' => 'btn btn-primary')); ?>
		</div>
		<?php echo Form::close() ?>
	</div>
</div>