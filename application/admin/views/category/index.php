<h1><?php echo $content_title ?></h1>
<div class="well">
	<div class="row">
		<div class="span3">	
			<h3>Category Structure</h3><br/>
			<?php
				$parent_ids = array();
				foreach ($categories as $parent_category)
				{
					$parent_ids[$parent_category->id] = $parent_category->name;
					echo '<a class="trigger level1 tip" cat="'.$parent_category->id.'" href="#" style="padding-left:18px;">'.$parent_category->name.'</a>';
					
					$subcategories = ORM::factory('Category')->where('parent_id', '=', $parent_category->id)->order_by('name', 'ASC')->find_all();
					if (count($subcategories) > 0)
					{
						echo '<ul class="level2">';
						foreach ($subcategories as $subcategory)
						{
							echo '<li class="tip" cat="'.$subcategory->id.'" style="list-style:none;"><a href="#">'.$subcategory->name.'</a></li>';
						}
						echo '</ul>';
					}		
					echo '<br/>';
				}
			?>
		</div>
	</div>
</div>
<div class="well">
	<h3>Create A New Category</h3>
	<div class="form medium_form">
		<?php echo Form::open('/category/add', array('class' => 'category_form')) ?>
		<div class="form_field">
			<?php
				$no_parent = array(0 => 'None (make a parent)');
				$select_options = Arr::merge($no_parent, $parent_ids);
				echo Form::label('form[parent]', 'Parent Category');
				echo Form::select('form[parent]', $select_options);
			?>
		</div>
		<div class="form_field">
			<?php
				echo Form::label('form[name]', 'Category Name');
				echo Form::input('form[name]');
			?>
		</div>
		<div class="form_field">
			<?php
				echo Form::label('form[description]', 'Description');
				echo Form::textarea('form[description]');
			?>
		</div>
		<div class="buttons">
			<?php echo Form::button(NULL, 'Add', array('type' => 'submit', 'class' => 'btn btn-primary')); ?>
		</div>
		<?php echo Form::close() ?>
	</div>
</div>

<script>
$(document).ready(function(){
	$("a.trigger").click(function () {
		return false;
		$(this).next().animate({
		  height: 'toggle', opacity: 'toggle'
		}, "slow");
		$(this).toggleClass("opened");
	});
	
	$('.tip').each(function(){
		$(this).tooltip({
		trigger:'hover',
		placement: 'right',
		delay:{
			show: 800,
			hide: 1350
		},
		title: '<a href="/category/edit/'+$(this).attr('cat')+'">Edit</a> or <a href="/category/delete/'+$(this).attr('cat')+'">Delete</a>'
		});
	});
	
});
	
	

</script>