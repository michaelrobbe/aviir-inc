<div class="navbar navbar-inverse navbar-fixed-top">
	<div class="navbar-inner">
		<div class="container-fluid">
			<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</a>
			<?php echo HTML::anchor('/', $site_name, array('class' => 'brand')) ?>
			<div class="nav-collapse">
				<ul class="nav">
					<?php
                        if ($user)
                        {
    						foreach ($main_navigation as $navigation)
    						{
    						    if ($user->has('roles', ORM::factory('Role')->where('name', '=', $navigation['permission'])->find()))
                                {
        							echo '<li';
        							if ($navigation['controller'] == $requsted_controller)
        							{
        								echo ' class="active"';
        							}
        							echo '>';
        							echo HTML::anchor($navigation['url'], $navigation['title']);
        							echo '</li>';
                                }
                            }
						}
					?>
				</ul>
				<?php
					if ($user)
					{
						echo '<p class="navbar-text pull-right">Logged in as ';
						echo HTML::anchor('/user/index?form%5Busername%5D='.$user->username.'&form%5Bemail%5D=&form%5Bfirst_name%5D=&form%5Blast_name%5D=', $user->username);
						echo ' | ';
						echo HTML::anchor('/logout', 'Log Out');
						echo '</p>';
					}
					else
					{
						echo '<p class="navbar-text pull-right"><a href="/login">Please Login</a></p>';
					}
				?>
			</div><!--/.nav-collapse -->
		</div>
	</div>
</div>