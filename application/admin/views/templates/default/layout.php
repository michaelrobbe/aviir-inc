<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
	<head>
		<?php echo $html_header ?>
	</head>

	<body>
		<!--[if lt IE 7]><p class=chromeframe>Your browser is <em>ancient!</em> <a href="http://browsehappy.com/">Upgrade to a different browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to experience this site.</p><![endif]-->
		
		<?php echo $header ?>
		
		<div id="body" class="container-fluid">
			<div class="row-fluid">
				<?php
					if (is_array($sidebar_navigation) AND count($sidebar_navigation) > 0)
					{
				?>
				<div class="span3">
					<div class="well sidebar-nav">
						<ul class="nav nav-list">
							<li class="nav-header"><?php echo $requsted_controller ?></li>
							<?php
								foreach ($sidebar_navigation as $method => $title)
								{
									echo '<li';
									if ($method == $requsted_action)
									{
										echo ' class="active"';
									}
									echo '>';
									echo HTML::anchor('/'.$requsted_controller.'/'.$method, $title);
									echo '</li>';
								}
							?>
						</ul>
					</div><!--/.well -->
				</div><!--/span-->
				<div class="span9">
				<?php
					}
					elseif($content_navigation) 
					{
						
				?>
						<div class="span3">
							<div class="well sidebar-nav">
								<ul class="nav nav-list">
									<li class="nav-header"><?php echo $requsted_controller ?></li>
									<?php
                                        $nav_parent = false;
                                        $ul_opened = false;
										foreach ($content_navigation as $node => $node_content)
										{
										    $subnav_node = explode('_', $node);
                                            if (count($subnav_node) > 1 AND $nav_parent != ucfirst($subnav_node[0]))
                                            {
                                                if ($ul_opened)
                                                {
                                                    echo '</ul>';
                                                    $ul_opened = false;
                                                }
                                                $ul_opened = true;
                                                $nav_parent = ucfirst($subnav_node[0]);
                                                
                                                echo '<li class="nav-header">';
                                                echo $nav_parent;
                                                echo '</li>';
                                                
                                                echo '<ul class="nav nav-list">';
                                            }
                                            elseif (count($subnav_node) == 1)
                                            {
                                                if ($ul_opened)
                                                {
                                                    echo '</ul>';
                                                    $ul_opened = false;
                                                }
                                            }
                                            
											echo '<li ';
											if ($node_name == $node)
                                            {
                                                echo 'class="active"';
                                            }
											echo '>';
											echo HTML::anchor('/content/view/'.$node, $node_content['label']);
											echo '</li>';
										}
									?>
								</ul>
							</div><!--/.well -->
						</div><!--/span-->
						<div class="span9">
				<?php
					}
					else
					{
				?>
				<div class="span12">
				<?php
					}
					echo Notice::render();
					echo $body;
				?>
				</div><!--/span-->
			</div><!--/row-->
			<hr>
			<footer>
				<?php echo $footer ?>
			</footer>
    	</div><!--/.fluid-container-->
		
		<script src="/media/admin/js/plugins.js"></script>
		<script src="/media/common/js/libs/jquery.validate.min.js"></script>
		<script src="/media/admin/js/script.js"></script>
		<script src="/media/admin/js/libs/tablesorter.js"></script>
		<?php
            echo HTML::script('media/common/jquery_file_uploader/js/jquery.iframe-transport.js');
            echo HTML::script('media/common/jquery_file_uploader/js/jquery.fileupload.js');
            echo HTML::script('media/common/jquery_file_uploader/js/jquery.fileupload-fp.js');
            echo HTML::script('media/common/jquery_file_uploader/js/jquery.fileupload-ui.js');
		?>
	</body>
</html>
