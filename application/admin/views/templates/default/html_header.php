<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<title><?php echo $title ?></title>
<meta name="description" content="">
<meta name="author" content="">

<meta name="viewport" content="width=device-width">
<?php
    echo HTML::style('media/common/css/bootstrap.css');
    echo HTML::style('http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.21/themes/smoothness/jquery-ui.css');
    echo HTML::style('media/admin/css/style.css');
?>

<script src="/media/common/js/libs/modernizr-2.5.3.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.9/jquery-ui.min.js"></script>
<script>window.jQuery || document.write('<script src="/media/common/js/libs/jquery-1.7.2.min.js"><\/script>')</script>


<!-- Le fav and touch icons -->
<link rel="shortcut icon" href="/media/common/ico/favicon.ico">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="/media/common/ico/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="/media/common/ico/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="/media/common/ico/apple-touch-icon-57-precomposed.png">