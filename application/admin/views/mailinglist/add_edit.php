<h3><?php echo $content_title ?></h3>
<div class="content_body well">
    <div class="form medium_form">
        <?php
            echo Form::open('/'.$model_name.'/save/'.$pledge->id, array('method' => 'post'));
            echo Form::hidden('form[action]', $action);
        ?>
        <div class="form_field">
            <?php 
                echo Form::label('form[pledge][email]', 'Email');
                echo Form::input('form[pledge][email]', $pledge->email, array('class' => 'span8'));
            ?>
        </div>
        <div class="form_field">
            <?php 
                echo Form::label('form[pledge][first_name]', 'First Name');
                echo Form::input('form[pledge][first_name]', $pledge->first_name, array('class' => 'span6'));
            ?>
        </div>
        <div class="form_field">
            <?php 
                echo Form::label('form[pledge][last_name]', 'Last Name');
                echo Form::input('form[pledge][last_name]', $pledge->last_name, array('class' => 'span6'));
            ?>
        </div>
        <div class="buttons">
            <?php
                echo Form::button(NULL, 'Save', array('type' => 'submit', 'class' => 'btn btn-primary'));
                echo '&nbsp;&nbsp;or&nbsp;&nbsp;';
                echo HTML::anchor('/'.$model_name, 'cancel');
            ?>
        </div>
        <?php echo Form::close(); ?>
    </div>
</div>