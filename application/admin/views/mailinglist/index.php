<h1><?php echo $content_title ?></h1>
<div class="well">
    <div class="buttons">
        <?php echo HTML::anchor('/'.$model_name.'/add', 'Add New Entry', array('type' => 'submit', 'class' => 'btn btn-success')); ?>
    </div>
    <div class="form medium_form">
        <?php
            echo Form::open('/'.$model_name.'/index/', array('method' => 'get'));
        ?>
        <div class="form_field">
            <?php 
                echo Form::label('form[email]', 'Email');
                echo Form::input('form[email]', $pledge['email']);
            ?>
        </div>
        <div class="form_field">
            <?php 
                echo Form::label('form[first_name]', 'First Name');
                echo Form::input('form[first_name]', $pledge['first_name']);
            ?>
        </div>
        <div class="form_field">
            <?php 
                echo Form::label('form[last_name]', 'Last Name');
                echo Form::input('form[last_name]', $pledge['last_name']);
            ?>
        </div>
        <div class="buttons">
            <?php echo Form::button(NULL, '<i class="icon-search icon-white"></i>'.'Search', array('type' => 'submit', 'class' => 'btn btn-primary')); ?>
        </div>
        <?php
            echo Form::close();
            echo '<br/>';
            echo $view_all_button;
            echo HTML::anchor('/'.$model_name.'/export?'.http_build_query($query_array), 'Export', array('class' => 'btn btn-primary'));
        ?>
    </div>
    </br>
    <div style="text-align:center;">
        <?php echo $pagination; ?>
    </div>
    <table class="table table-striped">
        <thead>
            <tr>
                <th><?php echo Format::create_sort_link('ID', 'id', $query_array, 'pledge') ?></th>
                <th><?php echo Format::create_sort_link('Email', 'email', $query_array, 'pledge') ?></th>
                <th><?php echo Format::create_sort_link('First Name', 'first_name', $query_array, 'pledge') ?></th>
                <th><?php echo Format::create_sort_link('Last Name', 'last_name', $query_array, 'pledge') ?></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php
                foreach ($pledges as $pledge)
                {
                    echo '<tr class="table_row '.Text::alternate('odd', 'even').'">';
                    echo '<td>'.$pledge->id.'</td>';
                    echo '<td>'.$pledge->email.'</td>';
                    echo '<td>'.$pledge->first_name.'</td>';
                    echo '<td>'.$pledge->last_name.'</td>';
                    echo '<td style="text-align:right;" class="btn-group">';
                    echo HTML::anchor('/mailinglist/edit/'.$pledge->id, 'Edit', array('alt' => 'Edit', 'class' => 'btn btn-small'));
                    echo HTML::anchor('/mailinglist/delete/'.$pledge->id, 'Delete', array('alt' => 'Delete', 'class' => 'delete btn btn-small btn-danger'));
                    echo '</td>';
                    echo '</tr>';
                }
            ?>
        </tbody>
    </table>
</div>

<div class="modal hide dialog" id="delete_dialog">
  <div class="modal-header">
    <a class="close" data-dismiss="modal">×</a>
    <h3>Confirmation Required</h3>
  </div>
  <div class="modal-body">
    <p>Are you sure you want to delete this?</p>
  </div>
  <div class="modal-footer">
    <a href="#" class="btn modal_hide">No</a>
    <a href="#" class="btn btn-primary modal_delete_yes_button">Yes</a>
  </div>
</div>