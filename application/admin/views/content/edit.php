<?php
    echo HTML::style('media/common/css/bootstrap-image-gallery.min.css');
    echo HTML::style('media/common/jquery_file_uploader/css/jquery.fileupload-ui.css');
    echo HTML::style('media/common/css/iphone-checkbox.css');
    
    echo HTML::script('/media/admin/js/libs/ckeditor/ckeditor.js');
    echo Form::open('/content/save', array('class' => 'well', 'id' => 'content_form', 'enctype' =>'multipart/form-data'));
    echo Form::hidden('id', $content->id);
    echo Form::hidden('node_name', $node_name);
    
    foreach ($node['fields'] as $key => $value)
    {
        $form_field = $content->contentfields->where('field_name', '=', $key)->find();
        $class = Arr::get($value, 'class', '');
        $options = Arr::get($value, 'options', ''); 
        if ($value['type'] == 'file_image')
        {
            if ($form_field->value)
            {
                $asset = ORM::factory('Asset', $form_field->value);
                $image_url = $asset->files->where('type', '=', 'image_small')->find()->url;
                echo '<div id="'.$key.'_image_container">';
                echo HTML::image($image_url, array('class' => 'img-polaroid', 'id' => $key.'_image'));
                echo '&nbsp;&nbsp;';
                echo HTML::anchor('#', 'Remove', array('class' => 'btn btn-danger remove_image', 'image_key' => $key));
                echo '</div>';
            }
        }
        elseif ($value['type'] == 'file_raw')
        {
            if ($form_field->value)
            {
                echo '<div>';
                $asset = ORM::factory('Asset', $form_field->value);
                if ($asset->type == 'image')
                {
                    $image_url = $asset->files->where('type', '=', 'image_small')->find()->url;
                    echo Html::image($image_url, array('class' => 'img-polaroid'));
                }
                else
                {
                    $image_url = $asset->files->where('type', '=', 'image_tiny')->find()->url;
                    echo HTML::image($image_url, array('class' => 'img-polaroid'));
                    echo '&nbsp;&nbsp;';
                    $upload_url = $asset->files->where('type', '=', 'raw')->find()->url;
                    $upload_url_pathinfo = pathinfo($upload_url);
                    echo HTML::anchor($upload_url, $upload_url_pathinfo['basename'], array('target' => '_BLANK'));
                }
                echo '</div>';
            }
        }

		echo Form::label($key, $value['label']);
		
		if ($value['type'] == 'select')
		{
        	echo Form::$value['type']($key, $options, $form_field->value, array('class' => $class));		
		}
		else
		{
            if ($class AND strstr($class, 'date') !== false)
            {
                $form_field->value = date('m/d/Y', (int) $form_field->value);
            }
			echo Form::$value['type']($key, $form_field->value, array('class' => $class));
		}    
    }
    echo '<div id="status_report"></div>';
    
    echo '<div>';
    echo HTML::image('media/common/img/loader.gif', array('id' => 'loader', 'style'=>'display:none;'));
    echo Form::submit('save', 'save', array('class' => 'btn btn-success'));
    echo '&nbsp;';
    echo HTML::anchor('/content/view/'.$node_name, 'cancel');
    echo '</div>';
    echo Form::close();
    
    echo HTML::script('media/common/jquery_file_uploader/js/content_main.js');
    echo HTML::script('media/common/jquery_file_uploader/js/vendor/jquery.ui.widget.js');
    echo HTML::script('media/common/jquery_file_uploader/js/jquery.iframe-transport.js');
    //echo HTML::script('media/common/js/tmpl.min.js');
    echo HTML::script('media/common/js/load-image.min.js');
    echo HTML::script('media/common/js/canvas-to-blob.min.js');
    echo HTML::script('media/common/jquery_file_uploader/js/locale.js');
    
    echo HTML::script('/media/common/js/iphone-style-checkboxes.js');
?>

<style>
    .bar {
        height: 18px;
        background: green;
    }
</style>