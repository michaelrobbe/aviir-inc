<?php
    echo Form::input($name.'_display', '', array('id' => $name.'_display', 'readonly' => 'readonly', 'class' => 'span5'));
?>
<div class="btn btn-primary fileinput-button" id="<?php echo $name ?>_container">
    <i class="icon-plus icon-white"></i> File
	<?php
	   echo Form::file($name.'_fileupload', array('id' => $name.'_fileupload', 'targetinput' => $name, 'class' => 'fileupload', 'data-url' => '/asset/handle_upload'));
	   echo Form::hidden($name, $value);
	?>
</div>
<div id="<?php echo $name ?>_progress">
    <div class="bar" style="width: 0%;"></div>
</div>
<script>
    var <?php echo $name ?>_data = false;
    $(function () {
        $('#<?php echo $name ?>_fileupload').fileupload({
            replaceFileInput: false,
            autoUpload: false,
            dataType: 'json',
            add: function(e, data) {
                if ($('#<?php echo $name ?>_display').val() == '') {
                    files_to_upload++;
                }
                <?php echo $name ?>_data = data;
                $('#<?php echo $name ?>_display').val(data.files[0].name);
            },
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('#<?php echo $name ?>_progress .bar').css('width', progress + '%');
            },
            success: function(result, textStatus, jqXHR) {
                $.each(result, function (index, file) {
                    $('<p/>').text(file.name+' upload complete.').appendTo($('#status_report'));
                    $('<p/>').text(file.name+' processing.').appendTo($('#status_report'));
                });
                $.ajax({
                    url: '/asset/upload_complete',
                    data: { url: result[0]['url'], type: '<?php echo $type ?>'},
                    dataType:'json',
                    success:function(data, textStatus, jqXHR){
                        var asset_id = data.asset_id;
                        $('input[name="<?php echo $name ?>"]').val(asset_id);
                        
                        $.each(result, function (index, file) {
                            $('<p/>').text(file.name+' processing complete.').appendTo($('#status_report'));
                        });
                        
                        $('#<?php echo $name ?>_display').remove();
                        $('#<?php echo $name ?>_fileupload').remove();
                        
                        files_to_upload--;
                        $('#content_form').submit();
                    }
                });
            }
        });
    });
</script>