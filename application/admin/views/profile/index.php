<h1><?php echo $content_title ?></h1>
<div class="well">
	<div class="form medium_form">
		<?php
			echo Form::open('/profile/index/', array('method' => 'get'));
		?>
		<div class="form_field">
			<?php 
				echo Form::label('form[user_id]', 'User ID');
				echo Form::input('form[user_id]', $profile['user_id']);
			?>
		</div>
		<div class="buttons">
			<?php echo Form::button(NULL, 'Search', array('type' => 'submit', 'class' => 'btn btn-primary')); ?>
		</div>
		<?php
            echo Form::close();
            echo '<br/>';
            echo $view_all_button;
        ?>
    </div>
    </br>
    <div style="text-align:center;">
        <?php echo $pagination; ?>
    </div>
	<table class="table table-striped">
		<thead>
			<tr>
				<th><?php echo Format::create_sort_link('ID', 'id', $query_array, 'profile') ?></th>
				<th></th>
				<th>Username</th>
				<th><?php echo Format::create_sort_link('User ID', 'user_id', $query_array, 'profile') ?></th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<?php
				foreach ($profiles as $profile)
				{
				    $profile_image = $profile->assets->where('type', '=', 'image')->find();
                    $profile_image = $profile_image->files->where('type', '=', 'image_tiny')->find()->url;
                    
					echo '<tr class="table_row '.Text::alternate('odd', 'even').'">';
					echo '<td>'.$profile->id.'</td>';
                    echo '<td>';
                    if ($profile_image != '')
                    {
                        echo HTML::image($profile_image);
                    }
                    echo '</td>';
					echo '<td>'.Html::anchor('/user/edit/'.$profile->user->id, $profile->user->username).'</td>';
					echo '<td>'.$profile->user->id.'</td>';
					echo '<td style="text-align:right;" class="btn-group">';
					echo HTML::anchor('/profile/edit/'.$profile->id, 'Edit', array('alt' => 'Edit', 'class' => 'btn btn-small'));
					if ($profile->address)
					{
						echo HTML::anchor('/profile/edit_address/'.$profile->id, 'Edit Address', array('alt' => 'Edit Address', 'class' => 'btn btn-small'));
					}
					else
					{
						echo HTML::anchor('/profile/create_address/'.$profile->id, 'Add Address', array('alt' => 'Add Address', 'class' => 'btn btn-small'));	
					}
                    echo HTML::anchor('/profile/delete/'.$profile->id, 'Delete', array('alt' => 'Delete', 'class' => 'delete btn btn-small btn-danger'));
					
					echo '</td>';
					echo '</tr>';
				}
			?>
		</tbody>
	</table>
</div>

<div class="modal hide dialog" id="delete_dialog">
  <div class="modal-header">
    <a class="close" data-dismiss="modal">×</a>
    <h3>Confirmation Required</h3>
  </div>
  <div class="modal-body">
    <p>Are you sure you want to delete this?</p>
  </div>
  <div class="modal-footer">
    <a href="#" class="btn modal_hide">No</a>
    <a href="#" class="btn btn-primary modal_delete_yes_button">Yes</a>
  </div>
</div>