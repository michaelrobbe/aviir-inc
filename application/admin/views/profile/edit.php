<link rel="stylesheet" type="text/css" href="/media/common/css/jquery-ui-1.8.21.custom.css" />

<h3><?php echo $content_title ?></h3>
<div class="content_body well">
	<div class="form medium_form">
		<?php			
			echo Form::open('/'.$model_name.'/save/'.$profile->id, array('method' => 'post', 'id' => 'profile_form'));
			if (isset($user))
			{
				echo Form::hidden('form[user_id]', $user->id);
			}
            
            $profile_image = $profile->assets->where('type', '=', 'image')->find();
            $profile_image = $profile_image->files->where('type', '=', 'image_small')->find()->url;
            
            if ($profile_image != '')
            {
                echo HTML::image($profile_image);
            }
		?>
		
		<div class="form_field">
			<?php 
				echo Form::label('form[mission_statement]', 'Mission Statement');
				echo Form::textarea('form[mission_statement]', $profile->mission_statement, array('class' => 'span8'));
			?>
		</div><br/>
		<div class="form_field control-group">
			<label class="control-label">Show Journey</label>
			<div class="controls">
			<?php
				
				echo Form::radio('form[journey]', 1, ($profile->show_journey == 1 ? true:false));
				echo Form::label('form[journey]', 'Yes', array('class'=> 'radio inline decent_inline'));
				echo Form::radio('form[journey]', 0, ($profile->show_journey == 0 ? true:false));
				echo Form::label('form[journey]', 'No', array('class'=> 'radio inline decent_inline'));
			?>
			</div>
		</div>
		<div class="form_field control-group">
			<label class="control-label">Show Mission</label>
			<div class="controls">
			<?php
				
				echo Form::radio('form[mission]', 1, ($profile->show_mission == 1 ? true:false));
				echo Form::label('form[mission]', 'Yes', array('class'=> 'radio inline decent_inline'));
				echo Form::radio('form[mission]', 0, ($profile->show_mission == 0 ? true:false));
				echo Form::label('form[mission]', 'No', array('class'=> 'radio inline decent_inline'));
			?>
			</div>
		</div>
		<div class="form_field control-group">
			<label class="control-label">Show Impact</label>
			<div class="controls">
			<?php
				
				echo Form::radio('form[graphs]', 1, ($profile->show_graphs == 1 ? true:false));
				echo Form::label('form[graphs]', 'Yes', array('class'=> 'radio inline decent_inline'));
				echo Form::radio('form[graphs]', 0, ($profile->show_graphs == 0 ? true:false));
				echo Form::label('form[graphs]', 'No', array('class'=> 'radio inline decent_inline'));
			?>
			</div>
		</div>
		<div class="form_field control-group">
			<label class="control-label">Show Media</label>
			<div class="controls">
			<?php
				
				echo Form::radio('form[media]', 1, ($profile->show_media == 1 ? true:false));
				echo Form::label('form[media]', 'Yes', array('class'=> 'radio inline decent_inline'));
				echo Form::radio('form[media]', 0, ($profile->show_media == 0 ? true:false));
				echo Form::label('form[media]', 'No', array('class'=> 'radio inline decent_inline'));
			?>
			</div>
		</div>
		<div class="form_field control-group">
			<label class="control-label">Show Social</label>
			<div class="controls">
			<?php
				
				echo Form::radio('form[social]', 1, ($profile->show_social == 1 ? true:false));
				echo Form::label('form[social]', 'Yes', array('class'=> 'radio inline decent_inline'));
				echo Form::radio('form[social]', 0, ($profile->show_social == 0 ? true:false));
				echo Form::label('form[social]', 'No', array('class'=> 'radio inline decent_inline'));
			?>
			</div>
		</div>
		<div class="form_field control-group">
			<label class="control-label">Show Friends</label>
			<div class="controls">
			<?php
				
				echo Form::radio('form[friends]', 1, ($profile->show_friends == 1 ? true:false));
				echo Form::label('form[friends]', 'Yes', array('class'=> 'radio inline decent_inline'));
				echo Form::radio('form[friends]', 0, ($profile->show_friends == 0 ? true:false));
				echo Form::label('form[friends]', 'No', array('class'=> 'radio inline decent_inline'));
			?>
			</div>
		</div>
				
		<div class="buttons">
			<?php
				echo Form::button(NULL, 'Save', array('type' => 'submit', 'class' => 'btn btn-primary'));
				echo '&nbsp;&nbsp;or&nbsp;&nbsp;';
                echo HTML::anchor('/'.$model_name, 'cancel');
			?>
		</div>
		<?php echo Form::close(); ?>
	</div>
</div>
