<link rel="stylesheet" type="text/css" href="/media/common/css/jquery-ui-1.8.21.custom.css" />

<div class="content_header">
	<?php echo $content_title ?>
</div>
<div class="content_body">
	<div class="form medium_form">
		<?php
			echo Form::open('/'.$model_name.'/save_address/'.$address->id, array('method' => 'post', 'id' => 'address_form'));
			echo Form::hidden('form[profile_id]', $profile->id);
		?>
		<div class="form_field">
			<?php 
				echo Form::label('form[address_1]', 'Address');
				echo Form::input('form[address_1]', $profile->address->address_1);
			?>
		</div>
		<div class="form_field">
			<?php 
				echo Form::label('form[address_2]', 'Address (cont.)');
				echo Form::input('form[address_2]', $profile->address->address_2);
			?>
		</div>
		<div class="form_field">
			<?php 
				echo Form::label('form[city]', 'City');
				echo Form::input('form[city]', $profile->address->city);
			?>
		</div>
		<div class="form_field">
			<?php 
				echo Form::label('form[state]', 'State');
				echo Form::select('form[state]', $states , $profile->address->state);
			?>
		</div>
		<div class="form_field">
			<?php 
				echo Form::label('form[zip]', 'Zip');
				echo Form::input('form[zip]', $profile->address->zip);
			?>
		</div>
		<div class="form_field">
			<?php 
				echo Form::label('form[country]', 'Country');
				echo Form::select('form[country]', $countries, $profile->address->country);
			?>
		</div><br/>		
		<div class="buttons">
			<?php
				echo Form::button(NULL, 'Save', array('type' => 'submit', 'class' => 'btn btn-success'));
				echo '&nbsp;';
				echo HTML::anchor('/'.$model_name, 'Cancel', array('type' => 'submit', 'class' => 'btn btn-danger'));
				echo '&nbsp;';
				echo HTML::anchor('/'.$model_name.'/delete_address/'.$profile->address->id, 'Delete', array('type' => 'submit', 'class' => 'btn btn-danger'));
			?>
		</div>
		<?php echo Form::close(); ?>
	</div>
</div>
