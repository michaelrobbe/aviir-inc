<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Profile extends Controller_Website {

	public function before()
	{
		$this->page_title = '';
		$this->model_name = 'profile';
		parent::before();
	}
	
	public function action_index()
	{
		$model_name = $this->model_name;
		$view = View::factory($model_name.'/index');
		$view->model_name = $this->model_name;
		$view->content_title = 'Profiles';
        
        $request = Request::initial();
        $query_array = $request->query();
        $view->query_array = $query_array;
		
		$search_form = array(
			'user_id' => ''
		);
		$form = Arr::get($_GET, 'form', $search_form);
		if ( ! isset($form))
		{
			$form = array();
		}
		foreach ($search_form as $key => $value)
		{
			$form[$key] = Arr::get($form, $key, $value);
		}
		$view->profile = $form;
				
		$profiles = $this->database_search($model_name, $form);
		$result_count = $profiles->count_all();
        
        $profiles = $this->database_search($model_name, $form);
        
        $page = Arr::get($_GET, 'page', 1);
        
        if (Arr::get($_GET, 'view_all', false))
        {
            $page_limit = $result_count;
            
            $view_all_button = HTML::anchor('/profile', 'Paginate List', array('id' => 'abutton', 'class' => 'btn btn-success'));
            $view->view_all_button = $view_all_button;
        }
        else
        {
            $page_limit = 20;
            $offset = ($page-1)*$page_limit;
            $profiles->limit($page_limit)->offset($offset);
            
            $view_all_button = HTML::anchor('/profile?view_all=true', 'View All', array('id' => 'abutton', 'class' => 'btn btn-success'));
            $view->view_all_button = $view_all_button;
        }
        
        $pagination = Pagination::factory(array(
            'items_per_page' => $page_limit,
            'total_items' => $result_count,
        ));
        $view->pagination = $pagination;
        
        $order_by_value = Arr::get($_GET, 'order_by', 'id');
        $sorted = Arr::get($_GET, 'sorted', 'asc');
        
		$profiles = $profiles->order_by($order_by_value, $sorted)->find_all();
		$view->profiles = $profiles;
		
		$this->template->body = $view;
	}

	public function action_add()
	{
		$id = $this->request->param('id');
		$user = ORM::factory('User', $id);
		$profile = ORM::factory(ucfirst($this->model_name));
		$model_name = $this->model_name;
		
		$view = View::factory($model_name.'/edit');
		$view->profile = $profile;
		$view->user = $user;
		$view->model_name = $this->model_name;
		$view->content_title = 'Edit Profile For: '.$user->username;
		$this->template->body = $view;
	}
	
	public function action_edit()
	{
		$id = $this->request->param('id');
		$profile = ORM::factory(ucfirst($this->model_name), $id);
		$model_name = $this->model_name;
		
		$view = View::factory($model_name.'/edit');
		$view->profile = $profile;
		$view->model_name = $this->model_name;
		$view->content_title = 'Edit Profile For: '.$profile->user->username;
		$this->template->body = $view;
	}
	
	public function action_delete()
	{
		$id = $this->request->param('id');
		$profile = ORM::factory(ucfirst($this->model_name), $id);
		
		$assets = $profile->assets->find_all();
		if (count($assets) > 0)
		{
			foreach ($assets as $asset)
	        {
	            $profile->remove('Assets', $asset);			
				$asset->remove_all_relations();
				$asset->delete();		
	        }
		}
		
		if($profile->address->id != NULL)
		{
			$profile->address->delete();
		}
			
        $profile->delete();
		
		Notice::add(Notice::SUCCESS, 'Profile Deleted.');				
		$this->redirect('/profile/index');
	}
	
	public function action_save()
	{
		$form = Arr::get($_POST, 'form', NULL);
		$id = $this->request->param('id');
		$profile = ORM::factory(ucfirst($this->model_name), $id);
		
		if (isset($form['user_id']))
		{
			$user = ORM::factory('User', $form['user_id']);
			$profile->user_id = $user->id;
		}	
				
		$profile->mission_statement = $form['mission_statement'];
		$profile->show_journey = $form['journey'];
		$profile->show_mission = $form['mission'];
		$profile->show_graphs = $form['graphs'];
		$profile->show_media = $form['media'];
		$profile->show_social = $form['social'];
		$profile->show_friends = $form['friends'];
		
		$profile->save();
		
		Notice::add(Notice::SUCCESS, 'Profile Saved.');			
		$this->redirect('/profile/index');
	}
	
	public function action_create_address()
	{
		$id = $this->request->param('id');
		$profile = ORM::factory(ucfirst($this->model_name), $id);
		
		$model_name = $this->model_name;
		
		$view = View::factory($model_name.'/edit_address');
		$view->profile = $profile;
		$view->address = $profile->address;
		$view->model_name = $this->model_name;
		$view->states = Infolist::states();
        $view->countries = Infolist::countries();
		$view->content_title = 'Create Address For: '.$profile->user->username;
		$this->template->body = $view;
	}
	
	public function action_edit_address()
	{
		$id = $this->request->param('id');
		$profile = ORM::factory(ucfirst($this->model_name), $id);
		
		$model_name = $this->model_name;
		$view = View::factory($model_name.'/edit_address');
		$view->profile = $profile;
		$view->model_name = $this->model_name;
		$view->address = $profile->address;
		$view->states = Infolist::states();
        $view->countries = Infolist::countries();
		$view->content_title = 'Edit Address For: '.$profile->user->username;
		$this->template->body = $view;
	}
	
	public function action_save_address()
	{
		$form = Arr::get($_POST, 'form', NULL);
		$id = $this->request->param('id');
		$address = ORM::factory('Address', $id);
		
		foreach ($form as $key => $value)
		{
			$address->$key = $value;
		}
		
		$address->save();
		
		Notice::add(Notice::SUCCESS, 'Address Saved.');				
		$this->redirect('/user/index');
	}
	
	public function action_delete_address()
	{
		$id = $this->request->param('id');
		$address = ORM::factory('Address', $id);
		$address->delete();
		Notice::add(Notice::SUCCESS, 'Address Deleted.');			
		$this->redirect('/user/index');
	}
	
	private function database_search($model, $params)
	{
		$model_orm = ORM::factory(ucfirst($model));
		foreach ($params as $key => $value)
		{
			if ($value != '')
			{
				$model_orm->where($key, 'like', '%'.$value.'%');
			}
		}
		return $model_orm;
	}
	
} // End Index
