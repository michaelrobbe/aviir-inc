<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Content extends Controller_Website {

    public function before()
    {
        $this->content = (array) Kohana::$config->load('content');
       	ksort($this->content);
       
        $this->page_title = '';
        $this->default_node = '';
        $this->selected_node = '';
        
        parent::before();
    }
    
    public function after()
    {
        $this->template->node_name = $this->selected_node;
        parent::after();
    }
    
    public function action_index()
    {
        foreach ($this->content as $content_key => $content_item)
        {
            $node_name = $content_key;
            $this->default_node = $node_name;
            $this->selected_node = $node_name;
            break;
        }
        
        $this->action_view();
    }
    
    public function action_view()
    {
        $node_name = $this->request->param('id', $this->default_node);
        $this->selected_node = $node_name;
        
        $node_type = $this->content[$node_name]['type'];
        
        if($node_type ==  'single')
        {
            $view = View::factory('content/edit');
            $content = ORM::factory('Content')->where('node_name','=',$node_name)->find();
            $view->content = $content;
        }
        elseif($node_type == 'multiple') 
        {
            $view = View::factory('content/index');
            $contents = ORM::factory('Content')->where('node_name','=',$node_name)->find_all();
            $view->contents = $contents;
        }
        
        $view->node_name = $node_name;
        $view->node = $this->content[$node_name];
        $this->template->body = $view;
    }
    
    public function action_add()
    {
        $node_name = $this->request->param('id');
        $this->selected_node = $node_name;
        
        $view = View::factory('content/edit');
        $content = ORM::factory('Content');
        $view->content = $content;
        
        $view->node_name = $node_name;
        $view->node = $this->content[$node_name];
        $this->template->body = $view;
    }
    
    public function action_edit()
    {
        $content_id = $this->request->param('id');
        
        $view = View::factory('content/edit');
        $content = ORM::factory('Content', $content_id);
        $node_name = $content->node_name;
        $view->content = $content;
        $this->selected_node = $node_name;
        
        $view->node_name = $node_name;
        $view->node = $this->content[$node_name];
        $this->template->body = $view;
    }
    
    public function action_delete()
    {
        $content_id = $this->request->param('id');
        
        $content = ORM::factory('Content', $content_id);
        $node_name = $content->node_name;
        
        $form_fields = $content->contentfields->find_all();
        foreach ($form_fields as $form_field)
        {
            $form_field->delete();
        }
        $content->delete();
        
        Notice::add(Notice::SUCCESS, $this->content[$node_name]['label'].' deleted.');
        $this->redirect('/content/view/'.$node_name);
    }
    
    public function action_save()
    {
        $post = $this->request->post();
        
        $id = Arr::get($post, 'id');
        unset($post['id']);
        $node_name = Arr::get($post, 'node_name');
        unset($post['node_name']);
        unset($post['submit']);
        
        $validation = Validation::factory($post);
        
        foreach ($this->content[$node_name]['fields'] as $field_key => $value)
        {
            if ($value['type'] == 'toggle' AND Arr::get($post, $field_key, false) === false)
            {
                $post[$field_key] = 0;
            }
            foreach ($value['rules'] as $rule_key => $rule_value)
            {
                if ($rule_value === true)
                {
                    $validation->rule($field_key, $rule_key);
                }
                else
                {
                    $validation->rule($field_key, $rule_key, array(':value', $rule_value));
                }
            }
        }
        
        if ($validation->check())
        {
            if ($id != 0)
            {
                $content = ORM::factory('Content', $id);
            }
            else
            {
                $content = ORM::factory('Content');
                $content->node_name = $node_name;
                $content->save();
            }
            
            $content->clear_cache();
            
            foreach ($post as $key => $value)
            {
                $field_test = Arr::get($this->content[$node_name]['fields'], $key);
                if ($field_test)
                {
                    $form_field = $content->contentfields->where('field_name', '=', $key)->find();
                    $form_field->content_id = $content->id;
                    $form_field->field_name = $key;
                    
                    $field_class = Arr::get($this->content[$node_name]['fields'][$key], 'class', false);
                    if ($field_class AND strstr($field_class, 'date') !== false)
                    {
                        $value = strtotime($value);
                    }
                    $form_field->value = $value;
                    $form_field->save();
                    $content->clear_cache_item($key);
                }
            }
            
            Notice::add(Notice::SUCCESS, $this->content[$node_name]['label'].' saved.');
            $this->redirect('/content/view/'.$node_name);
        }
        else
        {
            $errors = $validation->errors();
            
            $error_message = $this->content[$node_name]['label'].' not saved.';
            $error_message.= '<ul>';
            foreach ($errors as $field_name => $error)
            {
                $error_message.= '<li>';
                $error_message.= $this->content[$node_name]['fields'][$field_name]['label'];
                switch ($error[0])
                {
                    case 'not_empty':
                        $error_message.= ' cannot be blank.';
                        break;
                    case 'min_length':
                        $min_length = $this->content[$node_name]['fields'][$field_name]['rules'][$error[0]];
                        $error_message.= ' must be at least '.$min_length.' characters.';
                        break;
                    case 'max_length':
                        $max_length = $this->content[$node_name]['fields'][$field_name]['rules'][$error[0]];
                        $error_message.= ' must be under '.$max_length.' characters.';
                        break;
                    case 'exact_length':
                        $exact_length = $this->content[$node_name]['fields'][$field_name]['rules'][$error[0]];
                        $error_message.= ' must be exactly '.$exact_length.' characters.';
                        break;
                    case 'email':
                        $error_message.= ' must be a valid email address.';
                        break;
                    case 'url':
                        $error_message.= ' must be a valid url.';
                        break;
                    case 'phone':
                        $error_message.= ' must be a valid phone number.';
                        break;
                    case 'date':
                        $error_message.= ' must be a valid date.';
                        break;
                    case 'alpha':
                        $error_message.= ' must be letters only.';
                        break;
                    case 'alpha_dash':
                        $error_message.= ' must be only letters or dashes.';
                        break;
                    case 'alpha_numeric':
                        $error_message.= ' must be only letters or numbers.';
                        break;
                    case 'digit':
                        $error_message.= ' must be only numbers.';
                        break;
                    case 'decimal':
                        $error_message.= ' must be decimal.';
                        break;
                    case 'numeric':
                        $error_message.= ' must be numeric.';
                        break;
                }
                $error_message.= '</li>';
            }
            $error_message.= '</ul>';
            
            Notice::add(Notice::ERROR, $error_message);
            $this->redirect('/content/edit/'.$id);
        }
    }

	public function action_import_csv()
	{
		$adlcontents = ORM::factory('Content')->where('node_name', '=', 'ADL_tests')->find_all();
		foreach($adlcontents as $content)
		{
			$form_fields = $content->contentfields->find_all();
	        foreach ($form_fields as $form_field)
	        {
	            $form_field->delete();
	        }
	        $content->delete();
		}	
		
		$handle = fopen('media/admin/testlist.csv', 'r');
		ini_set('auto_detect_line_endings',true);
		
		while ( !feof($handle))
		{
			$datas = fgetcsv($handle,1024);
			$content = ORM::factory('Content');	
			$content->node_name = 'ADL_tests';
			$content->save();
			$count = 1;
			
			foreach ($datas as $data)
			{
				switch ($count)
				{
					case 1:
						$form_field = $content->contentfields->where('field_name', '=', 'testcode')->find();
		                $form_field->content_id = $content->id;
		                $form_field->field_name = 'testcode';
		                $form_field->value = $data;
		                $form_field->save();
						break;
					case 2:
						$form_field = $content->contentfields->where('field_name', '=', 'testname')->find();
		                $form_field->content_id = $content->id;
		                $form_field->field_name = 'testname';
		                $form_field->value = $data;
		                $form_field->save();
						break;
					case 3:
						$form_field = $content->contentfields->where('field_name', '=', 'cptcode')->find();
		                $form_field->content_id = $content->id;
		                $form_field->field_name = 'cptcode';
		                $form_field->value = $data;
		                $form_field->save();
						break;
					case 4:
						$form_field = $content->contentfields->where('field_name', '=', 'units')->find();
		                $form_field->content_id = $content->id;
		                $form_field->field_name = 'units';
		                $form_field->value = $data;
		                $form_field->save();
						break;
					case 5:
						$form_field = $content->contentfields->where('field_name', '=', 'category')->find();
		                $form_field->content_id = $content->id;
		                $form_field->field_name = 'category';
		                $form_field->value = strtolower(preg_replace('/(?<!,) /','_',preg_replace("/[^ \w]+/", "",$data)));
		                $form_field->save();
						break;
					case 6:
						$form_field = $content->contentfields->where('field_name', '=', 'specimen_required')->find();
		                $form_field->content_id = $content->id;
		                $form_field->field_name = 'specimen_required';
		                $form_field->value = strtolower(preg_replace('/(?<!,) /','_',preg_replace("/[^ \w]+/", "",$data)));
		                $form_field->save();
						break;	
				}
				$count ++;	
			}
		}
		fclose($handle);
	}
}