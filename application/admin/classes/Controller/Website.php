<?php defined('SYSPATH') or die('No direct script access.');

abstract class Controller_Website extends Controller_Template {
 
	public $auth_required = array('admin');
    public $page_title;
 
    public function before()
    {
    	$this->auth = Auth::instance();
		
		$template_base = 'default';
		$this->template = 'templates/'.$template_base.'/layout';
		$this->template_base = $template_base;
		
		$settings = new Model_Setting;
		$this->site_name = '';
		
		parent::before();
        
        $config = Kohana::$config->attach(new Config_Database);
        
        $protocol = 'http';
        $https = Arr::get($_SERVER, 'HTTPS', 'off');
        if ($https == 'on')
        {
            $protocol = 'https';
        }
        
        $this->protocol = $protocol;
		
		$logged_in = false;
		if ( ! $this->auth->logged_in())
		{
			$logged_in = false;
		}
		else
		{
			foreach ($this->auth_required as $auth_required_role)
			{
				if ($this->auth->logged_in($auth_required_role))
				{
					$logged_in = true;
				}
			}
            
            if ( ! $logged_in)
            {
                Notice::add(Notice::ERROR, 'You do not have permission to access admin.');
                $customer_url = Kohana::$config->load('website')->get('url');
                $this->redirect('http://'.$customer_url);
            }
		}
		
		$request = Request::initial();
		$requsted_controller = $request->controller();
		$requsted_action = $request->action();
		
		if ($logged_in != true AND $requsted_controller != 'Auth')
		{
			Notice::add(Notice::INFO, 'Please login.');
            $this->redirect('auth/login');
		}
        
		$this->user = $this->auth->get_user();
		
		$main_navigation = Kohana::$config->load('navigation.main');
		$sidebar_navigation = Kohana::$config->load('navigation.sidebar.'.$requsted_controller);
		$content_navigation = false;
        
        if ($requsted_controller != 'auth')
        {
            foreach ($main_navigation as $main_navigation_item)
            {
                if ($main_navigation_item['controller'] == $requsted_controller)
                {
                    if ( ! $this->user->has('roles', ORM::factory('role')->where('name', '=', $main_navigation_item['permission'])->find()))
                    {
                        Notice::add(Notice::ERROR, 'You do not have the appropriate permissions to view this page.');
                        $this->redirect('/');
                    }
                }
            }
        }
		
		if ($requsted_controller == 'Content') 
		{
			$content_navigation = (array) Kohana::$config->load('content');
			ksort($content_navigation);
		}
        
        // Make $page_title available to all views
        View::bind_global('page_title', $this->page_title);
		
		$this->site_name = Kohana::$config->load('website')->get('site_name');
		$this->url = Kohana::$config->load('website')->get('site_name');
		
		$this->page_title = $this->site_name.' - '.ucfirst($requsted_controller).' - '.ucfirst($requsted_action);
        
		$this->template->title = $this->page_title;
		$this->template->requsted_controller = $requsted_controller;
		$this->template->requsted_action = $requsted_action;
		$this->template->sidebar_navigation = $sidebar_navigation;
		$this->template->content_navigation = $content_navigation;
        $this->template->header = View::factory('templates/'.$this->template_base.'/header');
		$this->template->header->user = $this->user;
		$this->template->header->site_name = $this->site_name;
		$this->template->header->requsted_controller = $requsted_controller;
		$this->template->header->main_navigation = $main_navigation;
		$this->template->html_header = View::factory('templates/'.$this->template_base.'/html_header');
		$this->template->html_header->title = $this->page_title;
		$this->template->html_header->site_name = $this->site_name;
		$this->template->body = '';
		$this->template->footer = View::factory('templates/'.$this->template_base.'/footer');
        $this->template->footer->site_name = $this->site_name;
		
		$logged_in = $this->auth->logged_in();
		$this->template->logged_in = $logged_in;
		$this->template->header->logged_in = $logged_in;
    }
 
}