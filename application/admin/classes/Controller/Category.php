<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Category extends Controller_Website {

	public function before()
	{
		$this->page_title = '';
		$this->model_name = 'category';
		parent::before();
	}
	
	public function action_index()
	{		
		$view = View::factory($this->model_name.'/index');
		$view->model_name = $this->model_name;
		$view->content_title = 'Categories';
		
		$categories = ORM::factory(ucfirst($this->model_name))->where('level', '=', 1)->find_all();
		
		$view->categories = $categories;
		$this->template->body = $view;
	}
	
	public function action_add()
	{
		$form = Arr::get($_POST, 'form', NULL);
		if ($form)
		{
			if (isset($form['id']))
			{
				$category = ORM::factory(ucfirst($this->model_name), $form['id']);
			}
			else
			{
				$category = ORM::factory(ucfirst($this->model_name));
			}	
			
			$category->parent_id = $form['parent'];
			$category->name = $form['name'];
			$category->description = $form['description'];
			$category->level = 1;
			if ($form['parent'] != '0')
			{
				$category->level = 2;
			}			
			$category->save();
			
			$message = Text::ucfirst($this->model_name).' Saved.';
			Notice::add(Notice::SUCCESS, $message);
			$this->redirect('/'.$this->model_name);
			
		}
	}
	
	public function action_delete()
	{
		$id = $this->request->param('id');
		$category = ORM::factory(ucfirst($this->model_name), $id);
		
		$assets = $category->assets->find_all();
		foreach ($assets as $asset)
        {
            $category->remove('Assets', $asset);
        }
		
		$campaigns = $category->campaigns->find_all();
		foreach ($campaigns as $campaign)
        {
            $category->remove('Categories', $category);
        }
        $category->delete();
		
		Notice::add(Notice::SUCCESS, 'Category Deleted.');				
		$this->redirect('/category/index');
	}
	
	public function action_edit()
	{
		$id = $this->request->param('id');
		$category = ORM::factory(ucfirst($this->model_name), $id);
		$categories = ORM::factory(ucfirst($this->model_name))->where('level', '=', 1)->find_all();
		$parent_ids = array();
		foreach ($categories as $parent)
		{
			$parent_ids[$parent->id] = $parent->name;
		}
		$view = View::factory($this->model_name.'/edit');
		$view->model_name = $this->model_name;
		$view->content_title = 'Categories';
		$view->parent_ids = $parent_ids;
		$view->category = $category;
		$this->template->body = $view;
	}
	
} // End Index
