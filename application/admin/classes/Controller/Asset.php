<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Asset extends Controller_Website {

    public function before()
    {
        $this->page_title = '';
        $this->model_name = 'asset';
        
        $request = Request::initial();
        $requested_action = $request->action();
        
        if ($requested_action == 'handle_upload')
        {
            $this->template = 'templates/default/layout_ajax';
        }
        
        parent::before();
    }
    
    public function action_index()
    {
        $this->redirect('/asset/image');
    }
    
    public function action_video()
    {   
        $model_name = $this->model_name;
        $type = 'video';
        $view = View::factory($model_name.'/videos');
        $view->model_name = $this->model_name;
        $view->content_title = 'Videos';
        
        $request = Request::initial();
        $query_array = $request->query();
        $view->query_array = $query_array;
        
        $search_form = array(
            'type' => $type,
            'title' => '',
            'user_type' => 'admin'
        );
        $form = Arr::get($_GET, 'form', $search_form);
        if ( ! isset($form))
        {
            $form = array();
        }
        foreach ($search_form as $key => $value)
        {
            $form[$key] = Arr::get($form, $key, $value);
        }
        $view->video = $form;
                
        $videos = $this->database_search($model_name = 'asset', $form);
        $result_count = $videos->count_all();
        
        $videos = $this->database_search($model_name = 'asset', $form);
        
        $page = Arr::get($_GET, 'page', 1);
        
        if (Arr::get($_GET, 'view_all', false))
        {
            $page_limit = $result_count;
            
            $view_all_button = HTML::anchor('/asset/video', 'Paginate List', array('id' => 'abutton', 'class' => 'btn btn-success'));
            $view->view_all_button = $view_all_button;
        }
        else
        {
            $page_limit = 20;
            $offset = ($page-1)*$page_limit;
            $videos->limit($page_limit)->offset($offset);
            
            $view_all_button = HTML::anchor('/asset/video?view_all=true', 'View All', array('id' => 'abutton', 'class' => 'btn btn-success'));
            $view->view_all_button = $view_all_button;
        }
        
        $pagination = Pagination::factory(array(
            'items_per_page' => $page_limit,
            'total_items' => $result_count,
        ));
        $view->pagination = $pagination;
        
        $order_by_value = Arr::get($_GET, 'order_by', 'id');
        $sorted = Arr::get($_GET, 'sorted', 'asc');
        
        $videos = $videos->order_by($order_by_value, $sorted)->find_all();
        $view->videos = $videos;
        
        $this->template->body = $view;
    }
    
    public function action_image()
    {   
        $model_name = $this->model_name;
        $type = 'image';
        $view = View::factory($model_name.'/images');
        $view->model_name = $this->model_name;
        $view->content_title = 'Images';
        
        $request = Request::initial();
        $query_array = $request->query();
        $view->query_array = $query_array;
        
        $search_form = array(
            'type' => $type,
            'title' => '',
            'user_type' => 'admin'
        );
        $form = Arr::get($_GET, 'form', $search_form);
        if ( ! isset($form))
        {
            $form = array();
        }
        foreach ($search_form as $key => $value)
        {
            $form[$key] = Arr::get($form, $key, $value);
        }
        $view->image = $form;
                
        $images = $this->database_search($model_name = 'asset', $form);
        $result_count = $images->count_all();
        
        
        
        $page = Arr::get($_GET, 'page', 1);
        
        if (Arr::get($_GET, 'view_all', false))
        {
            $page_limit = $result_count;
            
            $view_all_button = HTML::anchor('/asset/image', 'Paginate List', array('id' => 'abutton', 'class' => 'btn btn-success'));
            $view->view_all_button = $view_all_button;
        }
        else
        {
            $page_limit = 20;
            $offset = ($page-1)*$page_limit;
            $images->limit($page_limit)->offset($offset);
            
            $view_all_button = HTML::anchor('/asset/image?view_all=true', 'View All', array('id' => 'abutton', 'class' => 'btn btn-success'));
            $view->view_all_button = $view_all_button;
        }
        
        $pagination = Pagination::factory(array(
            'items_per_page' => $page_limit,
            'total_items' => $result_count,
        ));
        $view->pagination = $pagination;
        
        $order_by_value = Arr::get($_GET, 'order_by', 'id');
        $sorted = Arr::get($_GET, 'sorted', 'desc');
        
        $images = $images->order_by($order_by_value,$sorted)->find_all();   
        $view->images = $images;
        
        
        $this->template->body = $view;
    }

    public function action_vimeo_video()
    {   
        $model_name = $this->model_name;
        $type = 'vimeo';
        $view = View::factory($model_name.'/vimeo_videos');
        $view->model_name = $this->model_name;
        $view->content_title = 'Vimeo Videos';
        
        $request = Request::initial();
        $query_array = $request->query();
        $view->query_array = $query_array;
        
        $search_form = array(
            'type' => $type,
            'title' => '',
            'user_type' => 'admin'
        );
        $form = Arr::get($_GET, 'form', $search_form);
        
        if ( ! isset($form))
        {
            $form = array();
        }
        
        foreach ($search_form as $key => $value)
        {
            $form[$key] = Arr::get($form, $key, $value);
        }
        $view->vimeo_video = $form;
                
        $vimeo_videos = $this->database_search($model_name = 'asset', $form);
        $result_count = $vimeo_videos->count_all();
        
        $vimeo_videos = $this->database_search($model_name = 'asset', $form);
        
        $page = Arr::get($_GET, 'page', 1);
        
        if (Arr::get($_GET, 'view_all', false))
        {
            $page_limit = $result_count;
            
            $view_all_button = HTML::anchor('/asset/vimeo_video', 'Paginate List', array('id' => 'abutton', 'class' => 'btn btn-success'));
            $view->view_all_button = $view_all_button;
        }
        else
        {
            $page_limit = 20;
            $offset = ($page-1)*$page_limit;
            $vimeo_videos->limit($page_limit)->offset($offset);
            
            $view_all_button = HTML::anchor('/asset/vimeo_video?view_all=true', 'View All', array('id' => 'abutton', 'class' => 'btn btn-success'));
            $view->view_all_button = $view_all_button;
        }
        
        $pagination = Pagination::factory(array(
            'items_per_page' => $page_limit,
            'total_items' => $result_count,
        ));
        $view->pagination = $pagination;
        
        $order_by_value = Arr::get($_GET, 'order_by', 'id');
        $sorted = Arr::get($_GET, 'sorted', 'asc');
        
        $vimeo_videos = $vimeo_videos->order_by($order_by_value,$sorted)->find_all();   
        $view->vimeo_videos = $vimeo_videos;
        
        $this->template->body = $view;
    }

    public function action_document()
    {   
        $model_name = $this->model_name;
        $type = 'pdf';
        $view = View::factory($model_name.'/documents');
        $view->model_name = $this->model_name;
        $view->content_title = 'Documents';
        
        $request = Request::initial();
        $query_array = $request->query();
        $view->query_array = $query_array;
        
        $search_form = array(
            'type' => $type,
            'title' => '',
            'user_type' => 'admin'
        );
        $form = Arr::get($_GET, 'form', $search_form);
        if ( ! isset($form))
        {
            $form = array();
        }
        foreach ($search_form as $key => $value)
        {
            $form[$key] = Arr::get($form, $key, $value);
        }
        $view->document = $form;
        
        $documents = $this->database_search($model_name = 'asset', $form);
        $result_count = $documents->count_all();
        
        $documents = $this->database_search($model_name = 'asset', $form);
        
        $page = Arr::get($_GET, 'page', 1);
        
        if (Arr::get($_GET, 'view_all', false))
        {
            $page_limit = $result_count;
            
            $view_all_button = HTML::anchor('/asset/document', 'Paginate List', array('id' => 'abutton', 'class' => 'btn btn-success'));
            $view->view_all_button = $view_all_button;
        }
        else
        {
            $page_limit = 20;
            $offset = ($page-1)*$page_limit;
            $documents->limit($page_limit)->offset($offset);
            
            $view_all_button = HTML::anchor('/asset/document?view_all=true', 'View All', array('id' => 'abutton', 'class' => 'btn btn-success'));
            $view->view_all_button = $view_all_button;
        }
        
        $pagination = Pagination::factory(array(
            'items_per_page' => $page_limit,
            'total_items' => $result_count,
        ));
        $view->pagination = $pagination;
        
        $order_by_value = Arr::get($_GET, 'order_by', 'id');
        $sorted = Arr::get($_GET, 'sorted', 'asc');
        
        $documents = $documents->order_by($order_by_value,$sorted)->find_all();   
        $view->documents = $documents;
        
        $this->template->body = $view;
    }

    public function action_user_video()
    {   
        $model_name = $this->model_name;
        $type = 'video';
        $view = View::factory($model_name.'/videos');
        $view->model_name = $this->model_name;
        $view->content_title = 'User Videos';
        
        $request = Request::initial();
        $query_array = $request->query();
        $view->query_array = $query_array;
        
        $search_form = array(
            'type' => $type,
            'title' => '',
            'user_type' => 'user'
        );
        $form = Arr::get($_GET, 'form', $search_form);
        if ( ! isset($form))
        {
            $form = array();
        }
        foreach ($search_form as $key => $value)
        {
            $form[$key] = Arr::get($form, $key, $value);
        }
        $view->video = $form;
                
        $videos = $this->database_search($model_name = 'asset', $form);
        $result_count = $videos->count_all();
        
        $videos = $this->database_search($model_name = 'asset', $form);
        
        $page = Arr::get($_GET, 'page', 1);
        
        if (Arr::get($_GET, 'view_all', false))
        {
            $page_limit = $result_count;
            
            $view_all_button = HTML::anchor('/asset/user_video', 'Paginate List', array('id' => 'abutton', 'class' => 'btn btn-success'));
            $view->view_all_button = $view_all_button;
        }
        else
        {
            $page_limit = 20;
            $offset = ($page-1)*$page_limit;
            $videos->limit($page_limit)->offset($offset);
            
            $view_all_button = HTML::anchor('/asset/user_video?view_all=true', 'View All', array('id' => 'abutton', 'class' => 'btn btn-success'));
            $view->view_all_button = $view_all_button;
        }
        
        $pagination = Pagination::factory(array(
            'items_per_page' => $page_limit,
            'total_items' => $result_count,
        ));
        $view->pagination = $pagination;
        
        $order_by_value = Arr::get($_GET, 'order_by', 'id');
        $sorted = Arr::get($_GET, 'sorted', 'asc');
        
        $videos = $videos->order_by($order_by_value,$sorted)->find_all();
        $view->videos = $videos;
        
        $this->template->body = $view;
    }
    
    public function action_user_image()
    {   
        $model_name = $this->model_name;
        $type = 'image';
        $view = View::factory($model_name.'/images');
        $view->model_name = $this->model_name;
        $view->content_title = 'User Images';
        
        $request = Request::initial();
        $query_array = $request->query();
        $view->query_array = $query_array;
        
        $search_form = array(
            'type' => $type,
            'title' => '',
            'user_type' => 'user'
        );
        $form = Arr::get($_GET, 'form', $search_form);
        if ( ! isset($form))
        {
            $form = array();
        }
        foreach ($search_form as $key => $value)
        {
            $form[$key] = Arr::get($form, $key, $value);
        }
        $view->image = $form;
                
        $images = $this->database_search($model_name = 'asset', $form);
        $result_count = $images->count_all();
        
        $images = $this->database_search($model_name = 'asset', $form);
        
        $page = Arr::get($_GET, 'page', 1);
        
        if (Arr::get($_GET, 'view_all', false))
        {
            $page_limit = $result_count;
            
            $view_all_button = HTML::anchor('/asset/user_image', 'Paginate List', array('id' => 'abutton', 'class' => 'btn btn-success'));
            $view->view_all_button = $view_all_button;
        }
        else
        {
            $page_limit = 20;
            $offset = ($page-1)*$page_limit;
            $images->limit($page_limit)->offset($offset);
            
            $view_all_button = HTML::anchor('/asset/user_image?view_all=true', 'View All', array('id' => 'abutton', 'class' => 'btn btn-success'));
            $view->view_all_button = $view_all_button;
        }
        
        $pagination = Pagination::factory(array(
            'items_per_page' => $page_limit,
            'total_items' => $result_count,
        ));
        $view->pagination = $pagination;
        
        $order_by_value = Arr::get($_GET, 'order_by', 'id');
        $sorted = Arr::get($_GET, 'sorted', 'asc');
        
        $images = $images->order_by($order_by_value,$sorted)->find_all();
        $view->images = $images;
        
        $this->template->body = $view;
    }

    public function action_edit()
    {
        $model_name = $this->model_name;
        
        $id = $this->request->param('id');
        $asset = ORM::factory(ucfirst($this->model_name), $id);
        
        $content_title = 'Edit Asset';
        
        $view = View::factory($model_name.'/add_edit');
        $view->asset = $asset;
        $view->content_title = $content_title;
        
        $category_model = new Model_Category;
        $categories = $category_model->get_category_tree();
        $view->categories = $categories;
        
        $selected_categories = array();
        foreach ($asset->categories->find_all() as $asset_category)
        {
            $selected_categories[] = $asset_category->id;
        }
        $view->selected_categories = $selected_categories;
        
        $this->template->body = $view;
    }
    
    public function action_save()
    {
        $post = Arr::get($_POST, 'asset');
        foreach ($post as $key => $value)
        {
            switch ($key)
            {
                case 'id':
                    if ($value == 0)
                    {
                        $asset = ORM::factory(ucfirst($this->model_name));
                    }
                    else
                    {
                        $asset = ORM::factory(ucfirst($this->model_name), $value);
                    }
                    break;
                default:
                    $asset->$key = $value;
                    break;
            }
        }
        $asset->save();
        
        $tags = Arr::get($_POST, 'tags');
        $tags = explode(',', $tags);
        
        $tag_model = new Model_Tag;
        $tag_model->add_tags($tags, $asset);
        
        $categories = Arr::get($_POST, 'categories');
        
        $category_model = new Model_Category;
        $category_model->add_categories($categories, $asset);
        
        Notice::add(Notice::SUCCESS, 'Asset Saved.');
        $this->redirect('/asset/'.$asset->type);
    }
    
    public function action_delete()
    {
        $id = $this->request->param('id');
        $asset = ORM::factory(ucfirst($this->model_name), $id);
        $type = $asset->type;
        $asset->remove_all_relations();
        $asset->delete();
        
        Notice::add(Notice::SUCCESS, 'Asset Deleted.');             
        $this->redirect('/asset/'.$type);
    }
    
    public function action_upload()
    {
        $view = View::factory($this->model_name.'/upload');
        $this->template->body = $view;
    }
    
    public function action_handle_upload()
    {
        require Kohana::find_file('vendor', 'jquery_file_uploader/upload.class', 'php');
        $upload_options = array(
            'script_url' => '/media/common/jquery_file_uploader/',
            'upload_dir' => 'media/uploads/',
            'upload_url' => '/media/uploads/',
            'image_versions' => array(
                'thumbnail' => array(
                    'upload_dir' => 'media/uploads/thumbnails/',
                    'upload_url' => '/media/uploads/thumbnails/',
                    'max_width' => 80,
                    'max_height' => 80
                )
            )
        );
        
        if (isset($_FILES)) {
            foreach ($_FILES as $file_key => $file_value)
            {
                $param_name = $file_key;
                break;
            }
            
			if (isset($param_name))
			{
				$upload_options['param_name'] = $param_name;
			}
        }
        
        $upload_handler = new UploadHandler($upload_options);
        
        header('Pragma: no-cache');
        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Content-Disposition: inline; filename="files.json"');
        header('X-Content-Type-Options: nosniff');
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: OPTIONS, HEAD, GET, POST, PUT, DELETE');
        header('Access-Control-Allow-Headers: X-File-Name, X-File-Type, X-File-Size');

        switch ($_SERVER['REQUEST_METHOD'])
        {
            case 'OPTIONS':
                break;
            case 'HEAD':
            case 'GET':
                $upload_handler->get();
                break;
            case 'POST':
                if (isset($_REQUEST['_method']) && $_REQUEST['_method'] === 'DELETE') {
                    $upload_handler->delete();
                }
                else
                {
                    $upload_handler->post();
                }
                break;
            case 'DELETE':
                $upload_handler->delete();
                break;
            default:
                header('HTTP/1.1 405 Method Not Allowed');
        }
        die();
    }
    
    public function action_upload_complete()
    {
        $url = urldecode(Arr::get($_GET, 'url'));
        $url_parts = pathinfo($url);
        $type = Arr::get($_GET, 'type');
        
        $asset_type = explode('/', $type);
        $asset_type = $asset_type[0];
        
        if ($asset_type == 'application')
        {
            $asset_type = explode('/', $type);
            $asset_type = $asset_type[1];
        }
        
        $asset = ORM::factory(ucfirst($this->model_name));
        $asset->title = $url_parts['filename'];
        $asset->type = $asset_type;
        $asset->user_type = 'admin';
        $asset->save();
        
        $file = ORM::factory('File');
        $file->asset_id = $asset->id;
        $file->type = 'upload';
        $file->url = $url;
        $file->storage = 'local';
        $file->save();
        
        $asset->process_uploaded_asset($asset_type);
        $return = array('asset_id' => $asset->id);
        echo json_encode($return);
        die();
    }
    
    public function action_search()
    {
        $type = Arr::get($_GET, 'type', 'video');
        $search_term = Arr::get($_GET, 'search_term', '');
        $selected_assets = Arr::get($_GET, 'selected_assets', '');
        
        $selected_assets_array = explode(',', $selected_assets);
        
        $assets = ORM::factory(ucfirst($this->model_name));
        $assets = $assets->where('type', '=', $type);
        $assets = $assets->where('title', 'LIKE', '%'.$search_term.'%');
        $assets = $assets->where('user_type', '=', 'admin');
        foreach ($selected_assets_array as $selected_asset)
        {
            $assets->where('id', '!=', $selected_asset);
        }
        $assets = $assets->limit(10)->find_all();
        foreach ($assets as $asset)
        {
            $asset_div = '<div class="asset" asset_id="'.$asset->id.'">';
            $asset_div.= '<div class="asset_handle pull-left hidden">';
            $asset_div.= '<div class="drag_handle">'.HTML::image('media/admin/img/drag_vertical.png').'</div>';
            $asset_div.= '</div>';
            $asset_div.= '<div class="asset_image pull-left">';
            $asset_div.= HTML::image($asset->files->where('type', '=', 'image_tiny')->find()->url);
            $asset_div.= '</div>';
            $asset_div.= '<div class="asset_title pull-left">';
            $asset_div.= '<h4>'.$asset->title.'</h4>';
            $asset_div.= '</div>';
            if ($asset->type == 'image')
            {
                $asset_div.= '<div class="asset_primary pull-left hidden">';
                $asset_div.= '<h5 class="pull-left">Primary Image:</h5>';
                $asset_div.= Form::radio('primary_image', $asset->id);
                $asset_div.= '</div>';
                
                $asset_div.= '<div class="asset_primary pull-left hidden">';
                $asset_div.= '<h5 class="pull-left">Background Image:</h5>';
                $asset_div.= Form::radio('background_image', $asset->id);
                $asset_div.= '</div>';
            }
            $asset_div.= '<div class="asset_button pull-right">';
            $asset_div.= Form::button(NULL, 'Add '.ucfirst($asset->type), array('class' => 'add_'.$asset->type.' btn btn-primary'));
            $asset_div.= '</div>';
            $asset_div.= '</div>';
            
            echo $asset_div;
        }
        die();
    }
    
    public function action_get_assets()
    {
        $campaign_id = Arr::get($_GET, 'campaign_id', '');
        $asset_ids = Arr::get($_GET, 'asset_ids', '');
        
        $asset_ids = explode(',', $asset_ids);
        
        if (count($asset_ids) > 0)
        {
            foreach ($asset_ids as $asset_id)
            {
                if ($asset_id != 0 AND $asset_id != '')
                {
                    $asset = ORM::factory(ucfirst($this->model_name))->where('id', '=', $asset_id)->find();
        
                    $asset_div = '<div class="asset" id="asset_'.$asset->id.'" asset_id="'.$asset->id.'">';
                    $asset_div.= '<div class="asset_handle pull-left">';
                    $asset_div.= '<div class="drag_handle">'.HTML::image('media/admin/img/drag_vertical.png').'</div>';
                    $asset_div.= '</div>';
                    $asset_div.= '<div class="asset_image pull-left">';
                    $asset_div.= HTML::image($asset->files->where('type', '=', 'image_tiny')->find()->url);
                    $asset_div.= '</div>';
                    $asset_div.= '<div class="asset_title pull-left">';
                    $asset_div.= '<h4>'.$asset->title.'</h4>';
                    $asset_div.= '</div>';
                    if ($asset->type == 'image')
                    {
                        $association = ORM::factory('Assets_Campaign')->where('asset_id', '=', $asset->id)->where('campaign_id', '=', $campaign_id)->find();
                        
                        $asset_div.= '<div class="asset_primary pull-left">';
                        $asset_div.= '<h5 class="pull-left">Primary Image:</h5>';
                        $asset_div.= Form::radio('primary_image', $asset->id, (bool) $association->primary);
                        $asset_div.= '</div>';
                        
                        $asset_div.= '<div class="asset_primary pull-left">';
                        $asset_div.= '<h5 class="pull-left">Background Image:</h5>';
                        $asset_div.= Form::radio('background_image', $asset->id, (bool) $association->background);
                        $asset_div.= '</div>';
                    }
                    $asset_div.= '<div class="asset_button pull-right">';
                    $asset_div.= Form::button(NULL, 'Remove '.ucfirst($asset->type), array('class' => 'remove_'.$asset->type.' btn btn-danger'));
                    $asset_div.= '</div>';
                    $asset_div.= '</div>';
                    
                    echo $asset_div;
                }
            }
        }
        die();
    }
    
    private function database_search($model, $params)
    {
        $model_orm = ORM::factory(ucfirst($model));
        foreach ($params as $key => $value)
        {
            if ($value != '')
            {
                $model_orm->where($key, 'like', '%'.$value.'%');
            }
        }
		
        return $model_orm;
    }
    
    public function action_replace_image()
    {
        $replacement_file = Arr::get($_FILES, 'image_replacement', false);
        $asset_id = Arr::get($_POST, 'asset_id', false);
        
        if ($replacement_file AND $asset_id)
        {
            $asset = ORM::factory(ucfirst($this->model_name), $asset_id);
            
            $test_file = $asset->files->find();
            $path_parts = pathinfo($test_file->url);
            $filename_parts = explode('_', $path_parts['filename']);
            
            $filename_base = $filename_parts[0];
            
            $temporary_url = 'media/uploads/'.$filename_base.'.jpg';
            move_uploaded_file(Arr::get($replacement_file, 'tmp_name'), $temporary_url);
            
            $file = ORM::factory('File');
            $file->type = 'upload';
            $file->url = $temporary_url;
            $file->storage = 'local';
            $file->save();
            
            $asset->add('files', $file);
            
            $asset_image_files = $asset->files->where('type', 'LIKE', 'image%')->find_all();
            foreach ($asset_image_files as $asset_image_file)
            {
                $asset->remove('files', $asset_image_file);
                $asset_image_file->delete();
            }
            
            $asset->process_uploaded_asset('image');
        }
        
        Notice::add(Notice::SUCCESS, 'Asset image Saved.');
        $this->redirect('/asset/edit/'.$asset->id);
    }

    public function action_import_vimeo()
    {
        $asset = ORM::factory(ucfirst($this->model_name));
        $asset->import_vimeo_videos();
        Notice::add(Notice::SUCCESS, 'Vimeo Library Updated.');
        $this->redirect('/asset/vimeo_video');
    }
    public function action_update_vimeo_video()
    {
        $id = $this->request->param('id');
        $asset = ORM::factory(ucfirst($this->model_name));
        $asset->update_vimeo_video($id);
        Notice::add(Notice::SUCCESS, 'Video '.$id.' updated.');
        $this->redirect('/asset/vimeo_video');
    }
    
} // End Index
