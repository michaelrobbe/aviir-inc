<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Index extends Controller_Website {

	public function before()
	{
		$this->page_title = '';
		
		parent::before();
	}
	
	public function action_index()
	{
		$this->redirect('/content');
	}

} // End Index
