<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Config extends Controller_Website {

	public function before()
	{
		$this->page_title = '';
		
		parent::before();
	}
	
	public function action_index()
	{
		$this->redirect('/config/website');
	}
	
	public function action_website()
	{
	    $group = 'website';
        
        $view = View::factory('config/'.$group);
		
		$post = Arr::get($_POST, $group, null);
		
		if ($post !== null)
		{
			foreach ($post as $key => $value)
			{
				$config = Kohana::$config->load($group);
				$config->set($key, $value);
			}
			Notice::add(Notice::SUCCESS, ucfirst($group).' Config Saved.');
		}
		
		$configs = Kohana::$config->load($group);
        
        $images = array();
        $orm_images = ORM::factory('Asset')->where('type', '=', 'image')->where('user_type', '=', 'admin')->find_all();
        foreach ($orm_images as $orm_image)
        {
            $images[$orm_image->id] = $orm_image->title;
        }
        $config_options_name = $group.'_background_image_options';
        $view->$config_options_name = $images;
        
        $boolean = array(0 => 'No', 1 => 'Yes');
        $config_options_name = $group.'_maintenance_options';
        $view->$config_options_name = $boolean;
        
        $view->group = $group;
		$view->configs = $configs;
        
		$this->template->body = $view;
	}
	
	public function action_facebook()
	{
		$view = View::factory('config/facebook');
		
		$post = Arr::get($_POST, 'facebook', null);
		
		if ($post !== null)
		{
			foreach ($post as $key => $value)
			{
				$config = Kohana::$config->load('facebook');
				$config->set($key, $value);
			}
			Notice::add(Notice::SUCCESS, 'Facebook Config Saved.');
		}
		
		$configs = Kohana::$config->load('facebook');
		$view->configs = $configs;
		
		$this->template->body = $view;
	}
	
	public function action_twitter()
	{
		$view = View::factory('config/twitter');
		
		$post = Arr::get($_POST, 'twitter', null);
		
		if ($post !== null)
		{
			foreach ($post as $key => $value)
			{
				$config = Kohana::$config->load('twitter');
				$config->set($key, $value);
			}
			Notice::add(Notice::SUCCESS, 'Twitter Config Saved.');
		}
		
		$configs = Kohana::$config->load('twitter');
		$view->configs = $configs;
		
		$this->template->body = $view;
	}
    
    public function action_google()
    {
        $view = View::factory('config/google');
        
        $post = Arr::get($_POST, 'google', null);
        
        if ($post !== null)
        {
            foreach ($post as $key => $value)
            {
                $config = Kohana::$config->load('google');
                $config->set($key, $value);
            }
            Notice::add(Notice::SUCCESS, 'Google Config Saved.');
        }
        
        $configs = Kohana::$config->load('google');
        $view->configs = $configs;
        
        $this->template->body = $view;
    }
    
    public function action_paypal()
    {
        $view = View::factory('config/paypal');
        
        $post = Arr::get($_POST, 'paypal', null);
        
        if ($post !== null)
        {
            foreach ($post as $key => $value)
            {
                $config = Kohana::$config->load('paypal');
                $config->set($key, $value);
            }
            Notice::add(Notice::SUCCESS, 'PayPal Config Saved.');
        }
        
        $configs = Kohana::$config->load('paypal');
        $view->configs = $configs;
        
        $this->template->body = $view;
    }

} // End Config
