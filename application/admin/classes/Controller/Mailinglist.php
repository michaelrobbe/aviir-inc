<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Mailinglist extends Controller_Website {

    public function before()
    {
        parent::before();
        
        $model_name = 'mailinglist';
        $plural_model_name = Inflector::plural($model_name);
        
        $this->page_title.= ' - '.Text::ucfirst($model_name);
        $this->content_title = Text::ucfirst($plural_model_name);
        $this->model_name = $model_name;
        $this->plural_model_name = $plural_model_name;
    }
    
    public function action_index()
    {
        $model_name = $this->model_name;
        
        $view = View::factory($model_name.'/index');
        $view->model_name = $this->model_name;
        $view->content_title = $this->content_title;
        
        $request = Request::initial();
        $query_array = $request->query();
        $view->query_array = $query_array;
        
        $search_form = array(
            'email' => '',
            'first_name' => '',
            'last_name' => ''
        );
        $form = Arr::get($_GET, 'form', $search_form);
        if ( ! isset($form))
        {
            $form = array();
        }
        foreach ($search_form as $key => $value)
        {
            $form[$key] = Arr::get($form, $key, $value);
        }
        $view->pledge = $form;
                
        $pledges = $this->database_search($model_name, $form);
        $result_count = $pledges->count_all();
        
        $pledges = $this->database_search($model_name, $form);
        
        $page = Arr::get($_GET, 'page', 1);
        
        if (Arr::get($_GET, 'view_all', false))
        {
            $page_limit = $result_count;
            
            $view_all_button = HTML::anchor('/mailinglist', 'Paginate List', array('id' => 'abutton', 'class' => 'btn btn-success'));
            $view->view_all_button = $view_all_button;
        }
        else
        {
            $page_limit = 20;
            $offset = ($page-1)*$page_limit;
            $pledges->limit($page_limit)->offset($offset);
            
            $view_all_button = HTML::anchor('/mailinglist?view_all=true', 'View All', array('id' => 'abutton', 'class' => 'btn btn-success'));
            $view->view_all_button = $view_all_button;
        }
        
        $pagination = Pagination::factory(array(
            'items_per_page' => $page_limit,
            'total_items' => $result_count,
        ));
        $view->pagination = $pagination;
        
        $order_by_value = Arr::get($_GET, 'order_by', 'id');
        $sorted = Arr::get($_GET, 'sorted', 'asc');
        
        $pledges = $pledges->order_by($order_by_value,$sorted)->find_all();
        
        $view->pledges = $pledges;
        
        $this->template->body = $view;
    }

    public function action_export()
    {
        $model_name = $this->model_name;
        $view = View::factory($model_name.'/export');
        
        $request = Request::initial();
        $query_array = $request->query();
        
        $search_form = array(
            'email' => '',
            'first_name' => '',
            'last_name' => ''
        );
        
        $form = Arr::get($_GET, 'form', $search_form);
        if ( ! isset($form))
        {
            $form = array();
        }
        foreach ($search_form as $key => $value)
        {
            $form[$key] = Arr::get($form, $key, $value);
        }
        
        $pledges = $this->database_search($model_name, $form);
        
        $order_by_value = Arr::get($_GET, 'order_by', 'id');
        $sorted = Arr::get($_GET, 'sorted', 'asc');
        
        $pledges = $pledges->order_by($order_by_value,$sorted)->find_all();
        
        $view->pledges = $pledges;
        
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename=pledges.csv');
        header('Pragma: no-cache');
        echo $view;
        die();
    }
    
    public function action_add()
    {
        $action = 'add';
        $model_name = $this->model_name;
        
        $view = View::factory($model_name.'/add_edit');
        $view->model_name = $this->model_name;
        $view->action = $action;
        $view->content_title = $this->content_title.' - '.Text::ucfirst($action).' '.Text::ucfirst($model_name);
        
        $pledge = ORM::factory(ucfirst($model_name));
        $view->pledge = $pledge;
        
        $this->template->body = $view;
    }
    
    public function action_edit()
    {
        $action = 'edit';
        $model_name = $this->model_name;
        
        $view = View::factory($model_name.'/add_edit');
        $view->model_name = $this->model_name;
        $view->action = $action;
        $view->content_title = $this->content_title.' - '.Text::ucfirst($action).' '.Text::ucfirst($model_name);
        
        $pledge_id = $this->request->param('id');
        
        $pledge = ORM::factory(ucfirst($model_name), $pledge_id);
        $view->pledge = $pledge;
        
        $this->template->body = $view;
    }
    
    public function action_save()
    {
        $model_name = $this->model_name;
        
        $pledge_id = $this->request->param('id');
        if ($user_id)
        {
            $pledge = ORM::factory(ucfirst($model_name), $pledge_id);
        }
        else
        {
            $pledge = ORM::factory(ucfirst($model_name));
        }
        
        $post = Arr::get($_POST, 'form', null);
        
        $pledge->email = $post['user']['email'];
        $pledge->first_name = $post['user']['first_name'];
        $pledge->last_name = $post['user']['last_name'];
        $pledge->save();
        
        $message = Text::ucfirst($model_name).' Saved.';
        Notice::add(Notice::SUCCESS, $message);
        $this->redirect('/'.$model_name);
    }

    public function action_delete()
    {
        $model_name = $this->model_name;
        
        $pledge_id = $this->request->param('id');
        
        if ($pledge_id)
        {
            $pledge = ORM::factory(ucfirst($model_name), $pledge_id);
            $pledge->delete();
            Notice::add(Notice::SUCCESS, ucfirst($model_name).' Deleted.');
            $this->redirect('/'.$model_name);
        }
        else
        {
            Notice::add(Notice::ERROR, ucfirst($model_name).' not found.');
            $this->redirect('/'.$model_name);
        }
    }
    
    private function database_search($model, $params)
    {
        $model_orm = ORM::factory(ucfirst($model));
        foreach ($params as $key => $value)
        {
            if ($value != '')
            {
                $model_orm->where($key, 'like', '%'.$value.'%');
            }
        }
        return $model_orm;
    }
}