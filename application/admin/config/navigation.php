<?php defined('SYSPATH') or die('No direct access allowed.');

return array(
	'main' => array(
		'home' => array(
			'title' => 'Home',
			'url' => '/',
			'controller' => 'index',
			'permission' => 'admin'
		),
		'content' => array(
			'title' => 'Content',
			'url' => '/content',
			'controller' => 'content',
			'permission' => 'content'
		),
		'category' => array(
			'title' => 'Categories',
			'url' => '/category',
			'controller' => 'category',
			'permission' => 'category'
		),
		'asset' => array(
			'title' => 'Media',
			'url' => '/asset',
			'controller' => 'asset',
			'permission' => 'media'
		),
		'users' => array(
			'title' => 'Users',
			'url' => '/user',
			'controller' => 'user',
			'permission' => 'user'
		),
		'profiles' => array(
			'title' => 'Profiles',
			'url' => '/profile',
			'controller' => 'profile',
			'permission' => 'profile'
		),
		'config' => array(
			'title' => 'Config',
			'url' => '/config',
			'controller' => 'config',
			'permission' => 'config'
		),
		'mailinglist' => array(
			'title' => 'Mailing List',
			'url' => '/mailinglist',
			'controller' => 'mailinglist',
			'permission' => 'mailinglist'
		),
	),
	'sidebar' => array(
		'config' => array(
			'website' => 'Website',
			'facebook' => 'Facebook',
			'twitter' => 'Twitter',
			'google' => 'Google',
			'paypal' => 'PayPal',
		),
		'asset' => array(
			'video' => 'Videos',
			'image' => 'Images',
			'user_video' => 'User Videos',
            'user_image' => 'User Images',
			'upload' => 'Upload',
		)
	),
);
