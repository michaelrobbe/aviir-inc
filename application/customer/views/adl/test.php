<?php

foreach($items as $item) {
	$content = ORM::factory('Content', $item['id']);
	$tests['test_name'] = strtoupper($content->get_field_value('testname'));
	$tests['test_code'] = strtoupper($content->get_field_value('testcode'));
	$tests['cpt_code'] = strtoupper($content->get_field_value('cptcode'));
	$tests['units'] = strtoupper($content->get_field_value('units'));
	$tests['category'] = str_replace('_', ' ',strtoupper($content->get_field_value('category')));
	$tests['specimen'] = str_replace('_', ' ', strtoupper($content->get_field_value('specimen_required')));

}
?>

<div class="row">
	<div class="span8 offset2">
		<?php
			foreach($tests as $key => $value) {
				$output = '';
				$output .= '<p><strong>' . $key . '</strong>: ';
				$output .= $value . '</p>';

				echo $output;
			}
		?>
	</div>
</div>