<?php 		
	echo $map->getHeaderJS();
	echo $map->getMapJS();
	
	$corp_address = $corporate->get_field_value('address');
	$corp_phone = $corporate->get_field_value('phone');
	$corp_toll = $corporate->get_field_value('tollfree');
	$corp_fax = $corporate->get_field_value('fax');
	
	$res_address = $research->get_field_value('address');
	$res_phone = $research->get_field_value('phone');
	$res_fax = $research->get_field_value('fax');
	
	$email_cl = $email->get_field_value('clientservice');
	$email_bill = $email->get_field_value('billing');
	$email_mark = $email->get_field_value('marketing');
	$email_sales = $email->get_field_value('sales');
?>
<div class="row">
	<div class="span12" id="map_cont">
		<?php
			$map->setWidth('1170', 'px');  
			$map->printOnLoad();
			$map->printMap();
		?>	
	</div>
</div>
<div class="row">
	<div class="span12" style="margin-top:25px; margin-bottom:25px;">
		<div class="page_title_non_header">CONTACT US</div>
		<div class="header_bar pull-right span10"></div>
	</div>
</div>
<div class="row">
	<div class="span4">
		<div class="contact_box">
			<div class="red_white_stripe_header"><h4>Corporate Office</h4></div>
			<div class="gray_box">
				<?php echo strip_tags($corp_address)?>
				<dl class="dl-horizontal">
					<dt>Phone:</dt>
					<dd><?php echo $corp_phone?></dd>
					<dt>Toll Free:</dt>
					<dd><?php echo $corp_toll?></dd>
					<dt>Fax:</dt>
					<dd><?php echo $corp_fax?></dd>
				</dl>
			</div>
			<img src="/media/customer/img/corporate.png" class="contact_bubble"/>
		</div>
	</div>
	<div class="span4">
		<div class="contact_box">
			<div class="red_white_stripe_header"><h4>Research & Development</h4></div>
			<div class="gray_box">
				<?php echo strip_tags($res_address)?>
				<dl class="dl-horizontal">
					<dt>Phone:</dt>
					<dd><?php echo $res_phone?></dd>
					<dt>Fax:</dt>
					<dd><?php echo $res_fax?></dd>
				</dl>
			</div>
			<img src="/media/customer/img/research.png" class="contact_bubble"/>
		</div>
	</div>
	<div class="span4">
		<div class="contact_box">
			<div class="red_white_stripe_header"><h4>Email Directly</h4></div>
			<div class="gray_box">
				<dl class="dl-horizontal" style="margin-left:-32px;">
					<dt>Client Services:</dt>
					<dd><?php echo Html::anchor('mailto:'.$email_cl, $email_cl)?></dd>
					<dt>Billing Department:</dt>
					<dd><?php echo Html::anchor('mailto:'.$email_bill, $email_bill)?></dd>
					<dt>Marketing:</dt>
					<dd><?php echo Html::anchor('mailto:'.$email_mark, $email_mark)?></dd>
					<dt>Sales:</dt>
					<dd><?php echo Html::anchor('mailto:'.$email_sales, $email_sales)?></dd>
				</dl>
			</div>
			<img src="/media/customer/img/emails.png" class="contact_bubble"/>
		</div>
	</div>
</div>
<div class="row">
	<div class="span12">
		<div class="dotted" style="margin-top:50px;"></div>
		<div class="red_white_stripe_header" style="margin-top:50px;"><h4>Get In Touch Now!</h4></div>
		<div class="gray_box" style="overflow:hidden;">
			<form action="/contact/email_us">
				<div style="width:300px;" class="pull-left">
					<input type="text" name="name" placeholder="Name" class="input-block-level"/>
					<input type="text" name="phone" placeholder="Phone" class="input-block-level"/>
					<input type="text" name="email" placeholder="Email" class="input-block-level"/>
				</div>
				<div class="pull-left" style="width:750px; margin-left:78px;">
					<textarea name="message" class="input-block-level" style="height:200px;" placeholder="Message Us:"></textarea>
					<input type="submit" class="btn btn-brown pull-right">
				</div>
			</form>
		</div>
	</div>
</div>
