<script>
	$(document).ready(function(){
		$('#query_submit').click(function(){
			$('#query_form').submit();
		});
	});
</script>
<style>
	.filter_span + .filter_span {
		border-left: 1px solid gray;
		padding: 0 0 0 10px;
		margin-left: 10px;
	}
</style>
<!-- <div class="row"> -->
<!-- 	<div class="span4 offset4"> -->
<!-- 		<h2>Under Construction.</h2> -->
<!-- 		<h4>Please check again soon.</h4> -->
<!-- 	</div> -->
<!-- </div> -->
<div class="row">
	<div class="span10 alpha_list">
		<?php
			foreach(range('A', 'Z') as $letter)
			{
				echo Html::anchor('/adl/search?by_letter='.$letter, $letter, array('class' => 'alpha_header'));
			}

		?>
	</div>
<!-- 	<div class="span5 offset3 input-append"> -->

<!-- 	</div> -->
</div>
<!--  <div class="row" style="text-align: center; margin:0 auto; width: 850px;">  -->
	<?php
// 		foreach ($categories as $category)
// 		{
// 			$value = $category['value'];
// 			echo "<div class='filter_span'>";
// 			echo Html::anchor('/adl/search?filter_by='.$value, strtoupper(str_replace('_', ' ', $value)), array('class' => 'adl_search_filter_tag'));
// 			echo "</div>";
// 		}
	?>
<!-- </div> -->
<div class="row">
	<div class="span8">
		<div class="red_white_stripe_header"><h4><?php echo strtoupper(str_replace('_', ' ',$searched_for)) ?></h4></div>
		<div class="table adl_search_table tablesorter" id="adl_search_table">
<!-- 			<thead> -->
<!-- 				<tr> -->
<!-- 					<th class="adl_search_header">TEST CODE</th> -->
<!-- 					<th class="adl_search_header">TEST NAME</th> -->
<!-- 					<th class="adl_search_header">CPT CODE</th> -->
<!-- 					<th class="adl_search_header">UNITS</th> -->
<!-- 					<th class="adl_search_header">CATEGORY</th> -->
<!-- 					<th class="adl_search_header">SPECIMEN REQUIRED</th> -->
<!-- 				</tr> -->
<!-- 			</thead> -->
<!-- 			<tbody> -->

<!--       <div class="test_row"> -->
<!--         <div class="test_code">COD</div> -->
<!--         <div class="test_name">TEST NAME</div> -->
<!--         <div class="test_cptcode">CPT CODE</div> -->
<!--       </div> -->

      <?php
				foreach ($items as $item)
				{
// 					echo '<div class="test_row">';
					$content = ORM::factory('Content', $item['id']);
					$test_name = strtoupper($content->get_field_value('testname'));
					$test_code = strtoupper($content->get_field_value('testcode'));
					$cpt_code = strtoupper($content->get_field_value('cptcode'));
// 					$units = strtoupper($content->get_field_value('units'));
// 					$category = str_replace('_', ' ',strtoupper($content->get_field_value('category')));
// 					$specimen = str_replace('_', ' ', strtoupper($content->get_field_value('specimen_required')));

					echo '<div class="test_row"><a href="/adl/test/' . $item['id'] .'"></a>';
					echo '<div class="test_code">';
					echo $test_code;
					echo "</div>";
					echo '<div class="test_name">';
					echo $test_name;
					echo "</div>";
					echo '<div class="test_cptcode">';
					echo $cpt_code;
					echo "</div>";
// 					echo '<td>';
// 					echo $units;
// 					echo "</td>";
// 					echo '<td>';
// 					echo $category;
// 					echo "</td>";
// 					echo '<td>';
// 					echo $specimen;
// 					echo "</td>";
					echo '</div>';
// 					echo '</div>';
	 			}
			?>
<!-- 			</tbody> -->
		</div>
	</div>

	 <div class="box-column-wrapper span4">
      <div class="box-column">
        <div class="box-column-inner">
          <div class="box-header">
            <h2>Search Tests</h2>
          </div>
            <div class="dropshadow" style="position: static; opacity: 0.5;">
              <img src="/media/customer/img/med_shadow180.png" alt="dropshadow">
            </div>
          <div class="box-content" style="text-align: left;">
			<form action='/adl/search' id="query_form">

				<?php
					echo Form::select('option', $options, $selected_option);
				?>

				<input type="text" name="q" id="appendedInput">
				<span class="add-on">
					<a href="#" id="query_submit">
						<input type="button" class="gray-button" value="Search" style="width:40%" />
<!-- 						<i class='icon-search'></i> -->
					</a>
				</span>
			</form>
          </div>
          <div class="dropshadow">
            <img src="/media/customer/img/med_shadow180.png" alt="dropshadow">
          </div>
        </div>
      </div>
    </div>

	 <div class="box-column-wrapper span4">
      <div class="box-column">
        <div class="box-column-inner">
          <div class="box-header">
            <h2>Contact The Lab</h2>
          </div>
            <div class="dropshadow" style="position: static; opacity: 0.5;">
              <img src="/media/customer/img/med_shadow180.png" alt="dropshadow">
            </div>
          <div class="box-content" style="text-align: left;">
            	<p>If you need to contact the lab, you can reach us from 8 am - 7 pm PST, Monday through Friday.</p>
				<p><strong>Phone:</strong> (949) 398-6300</p>
                    <p><strong>Fax:</strong> (949) 333-1442</p>
                    <p><strong>Email:</strong> <?php echo Html::anchor('mailto:clientservices@aviir.com', 'clientservices@aviir.com'); ?></p>
                    <p><strong>Address:</strong> 9805 Research Dr. Irvine, CA 92618</p>
          </div>
          <div class="dropshadow">
            <img src="/media/customer/img/med_shadow180.png" alt="dropshadow">
          </div>
        </div>
      </div>
    </div>
</div>
