<?php
$slider_items = ORM::factory('Content')->where('node_name', '=', 'slider')->find_all();
global $slides;
$slides = array();

foreach($slider_items AS $item) {
	$slides[] = ($link = $item->get_image_value('image', 'raw'));
}
?>

<div class="row">
	<div class="span12">
	<?php
		echo $slider;
	?>
	</div>
</div>

<div class="row">
	<div id="box-columns" class="row-fluid">
			  <?php
  foreach ($boxes as $box):
  	$title = $box->get_field_value('title');
  	$blurb = $box->get_field_value('blurb');
  	$link = $box->get_field_value('link');
  	$description = $box->get_field_value('description');
  	$image = $box->get_image_value('image', 'medium');
  ?>

    <div class="box-column-wrapper span4">
      <div class="box-column">
        <div class="box-column-inner">
          <?php echo HTML::anchor($link, '<span class="linkdiv"></span>'); ?>
          <div class="box-header">
            <h2><?php echo $title; ?></h2>
          </div>
          <div class="box-image">
            <div class="dropshadow">
              <?php echo HTML::image('media/customer/img/med_shadow180.png', array('alt' => 'dropshadow')); ?>
            </div>
            <?php echo HTML::image($image, array('alt' => 'Health professionals can use Aviir to order tests')); ?>
            <div class="dropshadow bottom-shadow">
              <?php echo HTML::image('media/customer/img/med_shadow.png', array('alt' => 'dropshadow')); ?>
            </div>
          </div>
          <div class="box-content-header">
            <h3><?php echo $blurb; ?></h3>
          </div>
          <div class="box-content">
            <p><?php echo $description; ?></p>
          </div>
          <div class="dropshadow">
            <?php echo HTML::image('media/customer/img/med_shadow180.png', array('alt' => 'dropshadow')); ?>
          </div>
        </div>
      </div>
    </div>

<?php endforeach; ?>
</div>

	</div>
<!-- <hr><hr><hr> -->
<!-- 	<div class="span8"> -->
	<?php
// 		foreach ($boxes as $box)
// 		{
// 			$title = $box->get_field_value('title');
// 			$blurb = $box->get_field_value('blurb');
// 			$link = $box->get_field_value('link');
// 			$description = $box->get_field_value('description');
// 			$image = $box->get_image_value('image', 'small');
// 			echo '<a href="'.$link.'">';
// 			echo "<div class='pull-left' style='width:190px; margin-right:10px;'>";
// 			echo "<div class='box_header' style=' background-color:#CEB98E'><h4 style='margin-bottom:0px;'>".$title."</h4></div>";
// 			echo "<div class='box' style='min-height:240px;'>";
// 			echo html::anchor($link,Html::image($image));
// 			echo "<h5>".html::anchor($link,$blurb, array('style'=>'color:black;'))."</h5>";
// 			echo html::anchor($link,$description, array('style'=>'color:black;'));
// 			echo Html::anchor($link,Html::image('media/customer/img/brown_bubble.png', array('class' => 'home_read_more', 'style' => 'right:30%')));
// 			echo "</div>";
// 			echo "</div>";
// 			echo '</a>';
// 		}
	?>
<!-- 	</div> -->
<!-- 	<div class="span4" > -->
<!-- 	<div class="pulse_header" style='background-color:#CEB98E'> -->
	    <?php
// 	    echo Html::image('media/customer/img/pulseimage.png', array('class'=>'pulse_header_image_adl'));
	    ?>
<!-- 	    <h4>THE PULSE</h4> -->
<!--     </div> -->
<!-- 		<div class="pulse_container"> -->
		<?php
// 			$count = 1;
// 			foreach($pulse_items as $pitem)
// 			{
// 				$pimage = $pitem->get_image_value('image', 'tiny');
// 				$ptitle = $pitem->get_field_value('title');
// 				$pdescription = $pitem->get_field_value('description');
// 				$plink = $pitem->get_field_value('link');
// 				$divide = $count / 2;
// 				echo '<a href="'.$plink.'" target="_blank">';
// 				if (ceil($divide) == $divide)
// 				{
// 					echo "<div class='pulse_item' style='background-color:#dcdcdc;'>";
// 				}
// 				else
// 				{
// 					echo "<div class='pulse_item'>";
// 				}

// 				echo HTML::image($pimage, array('class' => 'img img-polaroid pulse_image'));
// 				echo "<div class='pulse_content'>";
// 				echo "<h4 style='color:black;'>".strtoupper($ptitle)."</h4>";
// 				echo '<small>'.$pdescription.'</small>';
// 				echo "</div>";
// 				echo "</div>";
// 				echo "</a>";
// 				$count ++;
// 			}
		?>
<!-- 		</div> -->
<!-- 	</div> -->
<!-- </div> -->
