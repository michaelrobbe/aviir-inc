<?php
$professional_pages = DB::select('contents.id')
->from('contentfields')
->join('contents', 'left')
->on('contentfields.content_id', '=','contents.id')
->where('field_name', '=', 'menu_order')
->where('node_name', 'LIKE', 'professionals_pages')
->order_by('value', 'ASC')
->execute();

$patients_pages = DB::select('contents.id')
->from('contentfields')
->join('contents', 'left')
->on('contentfields.content_id', '=','contents.id')
->where('field_name', '=', 'menu_order')
->where('node_name', 'LIKE', 'patients_pages')
->order_by('value', 'ASC')
->execute();

$company_pages = DB::select('contents.id')
->from('contentfields')
->join('contents', 'left')
->on('contentfields.content_id', '=','contents.id')
->where('field_name', '=', 'menu_order')
->where('node_name', 'LIKE', 'company_pages')
->order_by('value', 'ASC')
->execute();

$adl_pages = DB::select('contents.id')
->from('contentfields')
->join('contents', 'left')
->on('contentfields.content_id', '=','contents.id')
->where('field_name', '=', 'menu_order')
->where('node_name', 'LIKE', 'ADL_pages')
->order_by('value', 'ASC')
->execute();

// echo '<pre>';
// print_r($professional_pages);
// echo '</pre>';

//     echo HTML::style('media/customer/css/header.css');
?>

<div id="header-wrapper">
	<div id="header" class="span12">
		<div id="navigation-wrapper">
			<div id="navigation">
				<div id="site-logo">
					<?php
					echo HTML::anchor('/', HTML::image('media/customer/img/logo@2x.png', array('alt' => 'Aviir - Helping to assess the risk of cardiovasular disorders through detection and prevention')));
					?>
				</div>
				<div id="dropdown-nav">
					<select class="responsive-nav">
						<option value="javascript:void(0)">--Select an Option--</option>
						<option value="<?php echo URL::site('professionals/'); ?>">Professional</option>
						<option value="<?php echo URL::site('patients/'); ?>">Patients</option>
						<option value="<?php echo URL::site('about/'); ?>">Company</option>
						<option value="<?php echo URL::site('adl/'); ?>">ADL</option>
					</select>
				</div>
				<ul class="nav pull-right">
					<li class="dropdown"><?php
					echo HTML::anchor('/professionals', '<span>Health Professionals</span>', array('class' =>'dropdown-toggle disabled', 'data-hover' => 'dropdown', 'data-toggle' => 'dropdown'));
					?>
						<div class="subnav-wrapper">
							<div class="caret-wrapper">
								<div class="dropdown-caret"></div>
								<div class="dropdown-caret dropdown-caret-white"></div>
							</div>
							<ul class="dropdown-menu">
								<li><?php echo HTML::anchor('professionals#tab-1','Improved Prediction') ?>
								</li>
								<li><?php echo HTML::anchor("professionals#tab-2",'Change The Future') ?>
								</li>
								<li><?php echo HTML::anchor("professionals#tab-3",'Better Patient Management') ?>
								</li>
								<li><?php echo HTML::anchor("professionals#tab-4",'Comprehensive Cardiac Testing') ?>
								</li>
								<li><?php echo HTML::anchor("professionals#tab-5",'Order A Test') ?>
								</li>
								<li><?php echo HTML::anchor("professionals/mirisk_vp_physician_faqs",'MIRISK VP Physician FAQs') ?>
								</li>
								<li><?php echo HTML::anchor("professionals/mirisk_physician_faqs",'MIRISK Physician FAQs') ?>
								</li>
								<li><?php echo HTML::anchor("professionals/blood_draw",'Schedule a Blood Draw') ?>
								</li>
								<li><?php echo HTML::anchor("professionals/psc_locations",'Find A PSC') ?>
								</li>
							</ul>
						</div>
					</li>
					<li class="dropdown"><?php
					echo HTML::anchor('/patients', '<span>Patients</span>', array('class' =>'dropdown-toggle disabled', 'data-hover' => 'dropdown', 'data-toggle' => 'dropdown'));
					?>

						<div class="subnav-wrapper">
							<div class="caret-wrapper">
								<div class="dropdown-caret"></div>
								<div class="dropdown-caret dropdown-caret-white"></div>
							</div>
							<ul class="dropdown-menu">
								<li><?php echo HTML::anchor("patients#tab-1",'The At Risk Patient');
					?>
								</li>
								<li><?php echo HTML::anchor("patients#tab-2",'Know Your Risk');
					?>
								</li>
								<li><?php echo HTML::anchor("patients#tab-3",'Getting Tested');
					?>
								</li>
								<li><?php echo HTML::anchor("patients#tab-4",'Understanding Your Results');
					?>
								</li>
								<li><?php echo HTML::anchor("patients#tab-5",'Change Your Future');
					?>
								</li>
								<li><?php echo HTML::anchor("patients/mirisk_vp_patient_faqs",'MIRISK VP Patient FAQs');
					?>
								</li>
								<li><?php echo HTML::anchor("patients/mirisk_patient_faqs",'MIRISK Patient FAQs');
					?>
								</li>
								<li><?php echo HTML::anchor("patients/financial_assistance_program",'Financial Assistance Program');
					?>
								</li>
								<li><?php echo HTML::anchor("patients/blood_draw",'Schedule a Blood Draw');
					?>
								</li>
								<li><?php echo HTML::anchor("contact/psc_locations",'Find A PSC');
					?>
								</li>
								<li><?php echo HTML::anchor("patients/make_a_payment",'Pay Your Bill');
					?>
								</li>
							</ul>
						</div>
					</li>
					<li class="dropdown"><?php
					echo HTML::anchor('/about', '<span>Our Company</span>', array('class' =>'dropdown-toggle disabled', 'data-hover' => 'dropdown', 'data-toggle' => 'dropdown'));
					?>

						<div class="subnav-wrapper">
							<div class="caret-wrapper">
								<div class="dropdown-caret"></div>
								<div class="dropdown-caret dropdown-caret-white"></div>
							</div>
							<ul class="dropdown-menu">
								<li><?php echo HTML::anchor("about",'About Us');
					?>
								</li>
								<li><?php echo HTML::anchor("blog",'News and Events');
					?>
								</li>
								<li><?php echo HTML::anchor("about/executives",'Executives');
					?>
								</li>
								<li><?php echo HTML::anchor("about/board_members",'Board Members');
					?>
								</li>
								<li><?php echo HTML::anchor("about/investors",'Investors');
					?>
								</li>
								<li><?php echo HTML::anchor("contact",'Contact Us');
					?>
								</li>
							</ul>
						</div>
					</li>
					<li class="dropdown"><?php
					echo HTML::anchor('/adl', '<span>ADL</span>', array('class' =>'dropdown-toggle disabled', 'data-hover' => 'dropdown', 'data-toggle' => 'dropdown'));
					?>
						<div class="subnav-wrapper">
							<div class="caret-wrapper">
								<div class="dropdown-caret"></div>
								<div class="dropdown-caret dropdown-caret-white"></div>
							</div>
							<ul class="dropdown-menu">
								<li><?php echo HTML::anchor("/adl/search",'Test Search');
					?>
								</li>
								<li><?php echo HTML::anchor("adl/psc_locations",'Find A PSC');
		?>
								</li>
								<li><?php echo HTML::anchor("adl/blood_draw",'Schedule a Blood Draw');
		?></li>
							</ul>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<!-- END NAVBAR -->

<!-- END HEADER -->
