<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />

		<title><?php echo $title ?></title>
		<meta name="description" content=""/>
		<meta name="author" content=""/>
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<!-- 		<script src="http://jquery-ui.googlecode.com/svn/tags/latest/ui/jquery.effects.core.js"></script> -->
<!-- 		<script src="http://jquery-ui.googlecode.com/svn/tags/latest/ui/jquery.effects.slide.js"></script> -->

		<?php
			if ($header_vars != '')
			{
				echo $header_vars;
			}
		?>
		<?php
			echo HTML::style('media/common/css/jNotify.jquery.css');
            echo HTML::style('media/common/css/bootstrap.css');
			echo HTML::style('media/common/css/js-video.min.css');
            echo HTML::style('media/common/css/jquery-ui-1.8.18.custom.css');
            echo HTML::style('media/customer/css/skin.css');
            echo HTML::style('media/customer/css/style.css');
            echo HTML::style('media/customer/css/royalslider.css');
		?>
		<!--[if lt IE 9]>
            <?php echo HTML::style('media/customer/css/skin_ie.css'); ?>
        <![endif]-->

<!-- 		  <script src="http://code.jquery.com/jquery-latest.js"></script> -->
		<?php
            echo HTML::script('media/common/js/libs/jquery-1.7.2.min.js');
            echo HTML::script('media/common/js/bootstrap.min.js');
            echo HTML::script('media/common/js/jNotify.jquery.min.js');
            echo HTML::script('media/common/js/libs/modernizr-2.5.3.min.js');
            echo HTML::script('media/customer/js/jquery.royalslider.min.js');
            echo HTML::script('media/customer/js/hover.js');
            echo HTML::script('media/customer/js/main.js');
            echo HTML::script('media/customer/js/script.js');


            $page = trim(str_replace('/', ' ', $_SERVER['REQUEST_URI']));

			if($_SERVER['REQUEST_URI'] == '/aviir/html/index.php/')
            	$page = '';

            if($page == '')
            	$page = 'home';

            $body_classes = preg_replace('/(\\?)(.*)/is', '', $page);

        ?>

		<!-- Le fav and touch icons -->
<!-- 		<script type="text/javascript" src="//use.typekit.net/sgj5qku.js"></script> -->
<!-- 		<script type="text/javascript">try{Typekit.load();}catch(e){}</script> -->
<!-- 		<link rel="shortcut icon" href="/media/common/ico/favicon.ico?v2"/> -->
<!-- 		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="/media/common/ico/apple-touch-icon-114-precomposed.png?v2"/> -->
<!-- 		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="/media/common/ico/apple-touch-icon-72-precomposed.png?v2"/> -->
<!-- 		<link rel="apple-touch-icon-precomposed" href="/media/common/ico/apple-touch-icon-57-precomposed.png?v2"/> -->

	</head>

	<body class="<?php echo $body_classes; ?>">
		<!--[if lt IE 8]><div id="ie7-suggestion"><p class=chromeframe>Your browser is <em>ancient!</em> <a href="http://browsehappy.com/">Upgrade to a different browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to experience this site.</p></div><div class="chromeframe-wrapper"></div><![endif]-->
<!--   		<div id="responsive-indicator"></div> -->
		<div id="all" class="container">
		<div id="left-all-bg"><div id="right-all-bg">
			<div id="main-content" class="container">
<!-- 	  		<header> -->
	  			<?php echo $header ?>
<!-- 			</header> -->

			<div id="body" class="container">
				<?php
	                echo Notice::render();
					if (isset($page_title))
					{
						echo '<div class="container" id="title_header">';
						echo '<div class="site_title">'.$page_title.'</div>';
						echo '<div class="header_bar pull-right span7"></div>';
						echo '</div>';
					}

	                $body->protocol = $protocol;
	                echo $body;
				?>
			</div>

<!-- 			<footer> -->
				<?php echo $footer ?>
<!-- 			</footer> -->

    	<?php
            $analytics = Kohana::$config->load('website')->get('analytics');

            if ($analytics)
            {
        ?>
        <script type="text/javascript">
            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', '<?php echo $analytics ?>']);
            _gaq.push(['_trackPageview']);

            (function() {
                var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            })();
        </script>
        <?php
            }
    	?>
    	</div>

    	</div>
		</div>
		</div>
		</div>
	</body>
</html>
