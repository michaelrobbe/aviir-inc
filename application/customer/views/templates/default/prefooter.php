<?php $news_items = ORM::factory('Content')->where('node_name', '=', 'blog_news')->or_where('node_name', '=', 'blog_events')->limit(3)->find_all(); ?>
<div class="footer_divider"></div>
 <div class="span4">
	<span class="footer_header">FOLLOW US:</span>
	<?php 
		echo Html::image('media/customer/img/facebook.png', array('class' => 'footer_icon'));
		echo Html::image('media/customer/img/twitter.png',array('class' => 'footer_icon'));
		echo Html::image('media/customer/img/linkedin.png',array('class' => 'footer_icon'));
	?>
	<div class="footer_header footer_subscribe">SUBSCRIBE TO OUR MAILING LIST</div>
	<form>
		<input type="text" name="mailing_fname" placeholder="FIRST NAME:"/>
		<input type="text" name="mailing_lname" placeholder="LAST NAME:"/>
		<input type="text" name="mailing_email" placeholder="EMAIL:"/>
		<br/>
		<button type="submit" class="btn btn-info footer_submit">SUBMIT</button>
	</form>
</div>
<div class="span4">
	<div class="footer_header" style="margin-top:8px;">GUARD A HEART TIPS</div>
	<div class="dotted"></div>
	<div class="footer_tip">
		"Deep Breathing Exercise"
	</div>
	<div class="dotted"></div>
</div>
<div class="span4">
	<div class="footer_header" style="margin-top:8px; margin-bottom:10px;">NEWS AND EVENTS:</div>
	<?php 
		foreach ($news_items as $item)
		{
			$title = $item->get_field_value('title');
			echo "<div class='footer_news_item'>".$title."</div>";
			echo "<div class='dotted'></div>";
		}
	?>
</div>
</div>