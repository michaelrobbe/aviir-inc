<?php
		$news_items = ORM::factory('Content')->where('node_name', '=', 'blog_news')->or_where('node_name', '=', 'blog_events')->order_by('id', 'DESC')->limit(2)->find_all();
		$news_count = ORM::factory('Content')->where('node_name', '=', 'tips')->count_all();
		$random_number = rand(1, $news_count -1);
		$news_item = ORM::factory('Content')->where('node_name', '=', 'tips')->limit(1)->offset($random_number)->find();
?>
					<div id="footer-wrapper">
					<div id="footer">
					<div id="footer-inner" class="row-fluid">

						<div class="footer-column-wrapper span4">
							<div class="footer-column">
									<div class="social_sprites">
									<h3>Follow Us:</h3>
										<a class="social_icons" id="social_twitter" href="https://twitter.com/AviirLabs"></a>
										<a class="social_icons" id="social_facebook" href="https://www.facebook.com/Aviir"></a>
										<a class="social_icons" id="social_linkedin" href="http://www.linkedin.com/company/aviir-inc."></a>
										<a class="social_icons" id="social_vimeo" href="http://vimeo.com/aviirlabs"></a>
									</div>
								<h4>Subscribe to Our Mailing List:</h4>
								<form action="/contact/mailing_list" id="contact_mail_form" novalidate="novalidate">
									<input type="text" size="25" name="mailing_fname" class="mailing_list" value="First Name:">
									<input type="text" size="25" name="mailing_lname" class="mailing_list" value="Last Name:">
									<input type="text" size="25" name="mailing_email" class="mailing_list" value="Email Address:">
									<input type="submit" value="Submit">
								</form>
							</div>
						</div>


						<div class="footer-column-wrapper footer_dropshadow span4">
						<div id="footer_dropshadow_left">
						<div id="footer_dropshadow_right">
						<div class="footer-column">

							<div id="guardaheart_logo">
							<?php
								echo HTML::image('media/customer/img/GaH_ico.png', array('alt' => 'Guard a Heart', 'width' => '74', 'height' => '52'));
							?>
<!-- 								<img src="img/GaH_ico.png" width="74" height="52" alt="Guard a Heart"> -->
							</div>
							<div id="guardaheart_quotes">
								<div class="gah_quote">
			    					<?php
			    						$tip = $news_item->get_field_value('tip');
			    						echo '<p>"'.$tip.'"</p>';
			    					?>
								</div>
<!-- 									<p>"Don't Stress out focusing on things you can't change. Focus on things that you can change and on making the effort to do so."</p> -->

<!-- 									<p>"Don't Stress out focusing on things you can't change. Focus on things that you can change and on making the effort to do so."</p> -->
<!-- 								</div> -->

<!-- 									<p>"Don't Stress out focusing on things you can't change. Focus on things that you can change and on making the effort to do so."</p> -->
<!-- 								</div> -->
							</div>
							<div id="gah_footer">
								<p>
								<a href="http://www.guardaheart.org" target="_BLANK">www.guardaheart.org</a> <br><a href="http://www.savethe1.org" target="_BLANK">www.savethe1.org</a>
								</p>
							</div>
						</div>
						</div>
						</div>
						</div>


						<div class="footer-column-wrapper span4">
						<div class="footer-column" id="news_events">
							<h3>News and Events</h3>
							<?php
							/**
							 * Build out and include excerpts from the content, or teaser content. Remove loud "always uppercase" titles.
							 **/
								foreach ($news_items as $item) {
									echo '<div class="news_story">';
									$title = $item->get_field_value('title');
// 									$title = ucfirst(strtolower($title . '&&&'));
									echo "<div class='news_story'>".Html::anchor('/blog/post/'.$item->id,$title)."</div>";
									echo '</div>';
								}
							?>
<!-- 							<div class="news_story"> -->
<!-- 								<p>Douglas Harrington, MD addresses select group of physicians regarding latest advances in cardio vascular risk assessment.</p> -->
<!-- 							</div> -->
<!-- 							<div class="news_story"> -->
<!-- 								<p>103.3 WAKG Radio Interview: <br /><a href="/blog/post/1996">60 Seconds to a Healthier You</a></p> -->
<!-- 							</div> -->
						</div>
						</div>

						<div id="footer-copyright-wrapper">
						<div id="footer-copyright">
							<ul id="footer-nav">
								<li><a href="http://www.aviir.com/contact">Contact Us</a></li>
<!-- 								<li><a href="#">Site Map</a></li> -->
								<li><a href="http://www.aviir.com/legal/privacy_policy">Privacy Policy</a></li>
								<li><a href="http://www.aviir.com/legal/terms">Terms and Conditions</a></li>
							</ul>
							<div class="copyright">&copy; 2013 Aviir Corporation</div>
						</div>
						</div>
					</div>
					</div>
					</div>
