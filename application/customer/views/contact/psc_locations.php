<?php
	echo $map->getHeaderJS();
	echo $map->getMapJS();
?>
<div class="row">
	<div class="span12" id="map_cont">
		<?php
			$map->setWidth('926', 'px');
			$map->setHeight('428', 'px');
			$map->printOnLoad();
			$map->printMap();
		?>
	</div>
</div>
<div class="row" style="margin-top:40px; margin-bottom:25px;">
	<div class="span12">
		<div class="page_title_non_header" style="color:#9A111F;">FIND A PATIENT SERVICE CENTER</div>
		<div class="header_bar pull-right span5" style="width: 440px;"></div>
	</div>
</div>
<div class="row">

    <div class="box-column-wrapper span4">
      <div class="box-column">
        <div class="box-column-inner">
<!--           <a href="/professionals"><span class="linkdiv"></span> </a> -->
          <div class="box-header">
            <h2>Search PSC Locations</h2>
          </div>
<!--           <div class="box-image"> -->
            <div class="dropshadow" style="position: static; opacity: 0.5;">
              <img src="/media/customer/img/med_shadow180.png" alt="dropshadow">
            </div>
<!--             <img src="/media/customer/img/hcp_photo.png" alt="Health professionals can use Aviir to order tests"> -->
<!--             <div class="dropshadow bottom-shadow"> -->
<!--               <img src="/media/customer/img/med_shadow.png" alt="dropshadow"> -->
<!--             </div> -->
<!--           </div> -->
<!--           <div class="box-content-header"> -->
<!--             <h3>Change the Future</h3> -->
<!--           </div> -->
          <div class="box-content">
            <form action="/contact/psc_locations">
					<input name="address" type="text" placeholder="Address:"><br/>
					<input name="city" type="text" placeholder="City:"><br/>
					<input name="state" class="input-mini" type="text" placeholder="State:">
					<input name="zip" class="input-small" type="text" placeholder="Zip:"><br/>
<!-- 					<div class="dotted"></div> -->
					<h2 style="text-align: left;">Filter Search</h2>
				<?php
				    $scopes = array(
				        10 => '10 Miles',
				        20 => '20 Miles',
				        30 => '30 Miles',
				        50 => '50 Miles',
				        100 => '100 Miles',
                    );
                    echo Form::select('scope', $scopes, Arr::get($_GET, 'scope', 10));
				?>
 				<input class="gray-button" type="submit" value="Search" />
				</form>
          </div>
          <div class="dropshadow">
            <img src="/media/customer/img/med_shadow180.png" alt="dropshadow">
          </div>
        </div>
      </div>


	</div>
	<div class="span8">
		<h3><?php echo $message ?></h3>
		<div class="investor_header"></div>
	</div>
	<div class="span8">
		<?php
			if (isset($pscs))
			{
				foreach ($pscs as $psc)
				{
					$address = Arr::get($psc, 'address');
					$name = Arr::get($psc, 'name');
					$information = Arr::get($psc, 'information');
					echo "<div class='psc_box'>";
// 					echo "<h3>".$name."</h3>";
// 					echo $address;
					echo "</br>";
					echo $information;
					echo "</div>";
					echo "<div class='dotted'></div>";
				}
			}

		?>
	</div>
</div>
