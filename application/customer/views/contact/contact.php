<?php
	echo Html::script('media/common/js/libs/jquery.validate.min.js');
	echo $map->getHeaderJS();
	echo $map->getMapJS();

    $corp_address = $corporate->get_field_value('address');
	$corp_phone = $corporate->get_field_value('phone');
	$corp_fax = $corporate->get_field_value('fax');
    $media_inquiries = $corporate->get_field_value('media_inquiries');
    $investor_relations = $corporate->get_field_value('investor_relations');

    $services_phone = $client_services->get_field_value('phone');
    $services_fax = $client_services->get_field_value('fax');
    $services_email = $client_services->get_field_value('email');
	$services_hours = $client_services->get_field_value('hours');

	$email_cl = $email->get_field_value('clientservice');
	$email_bill = $email->get_field_value('billing');
	$email_mark = $email->get_field_value('marketing');
	$email_sales = $email->get_field_value('sales');
?>
<script>
	$(document).ready(function(){
		$('#contact_form').validate({
			rules:{
				name: "required",
				email: {
					required:true,
					email:true
				},
				message: "required"
			},
			messages: {
				name: "Please enter your name.",
				email: {
					required:"Please enter your email address.",
					email:"Please use a valid email address."
				},
				message: "Please tell us why you are contacting us."
			}
		})

		$('#contact_submit').click(function(event){
		    event.preventDefault();
			if ($('#contact_form').valid())
			{
			    $('#contact_form').fadeOut('fast', function() {
			        $.ajax({
						type: "GET",
						url: $('#contact_form').attr('action'),
						data: $('#contact_form').serialize(),
						success: function() {
							$('#contact_form').html('<h4>Thank you for contacting us.</h4>');
					        $('#contact_form').fadeIn('fast');
						}
					});
			    })
			}
		});
	});


</script>
<!-- <div class="row"> -->
	<div id="map_cont">
		<?php
			$map->setWidth('926', 'px'); //940 - 14px
			$map->setHeight('414', 'px'); //428 - 14px
			$map->printOnLoad();
			$map->printMap();
		?>
	</div>
<!-- </div> -->
<div class="row">
	<div class="span12" style="margin-top:40px; margin-bottom:25px;">
		<div class="page_title_non_header">CONTACT US</div>
		<div class="header_bar pull-right span9"></div>
	</div>
</div>
<div class="row" style="margin-left: -15px;">


    <div class="box-column-wrapper span4">
      <div class="box-column">
        <div class="box-column-inner">
          <div class="box-header">
            <h2>Corporate Office</h2>
          </div>
            <div class="dropshadow" style="position: static; opacity: 0.5;">
              <img src="/media/customer/img/med_shadow180.png" alt="dropshadow">
            </div>
          <div class="box-content">
				    <p>Address: <?php echo $corp_address ?> </p>

					<p>Phone:<?php echo $corp_phone ?> <br />
					Fax:<?php echo $corp_fax ?></p>

					<p>Media Inquiries:<?php echo $media_inquiries ?> <br />
					Investor Relations:<?php echo $investor_relations ?></p>
          </div>
          <div class="dropshadow">
            <img src="/media/customer/img/med_shadow180.png" alt="dropshadow">
          </div>
        </div>
      </div>
    </div>


    <div class="box-column-wrapper span4">
      <div class="box-column">
        <div class="box-column-inner">
          <div class="box-header">
            <h2>Client Services</h2>
          </div>
            <div class="dropshadow" style="position: static; opacity: 0.5;">
              <img src="/media/customer/img/med_shadow180.png" alt="dropshadow">
            </div>
          <div class="box-content">
				<p>Toll Free: <?php echo $services_phone?><br/>
				Fax: <?php echo $services_fax ?><br/>
				Email: <?php echo $services_email ?></p>

				<p>Hours of Operation: <?php echo $services_hours ?></p>
          </div>
          <div class="dropshadow">
            <img src="/media/customer/img/med_shadow180.png" alt="dropshadow">
          </div>
        </div>
      </div>
    </div>


        <div class="box-column-wrapper span4">
      <div class="box-column">
        <div class="box-column-inner">
          <div class="box-header">
            <h2>Email Directly</h2>
          </div>
            <div class="dropshadow" style="position: static; opacity: 0.5;">
              <img src="/media/customer/img/med_shadow180.png" alt="dropshadow">
            </div>
          <div class="box-content">
				<p>Client Services: <?php echo HTML::mailto('mailto:'.$email_cl, $email_cl)?></p>
				<p>Billing Department: <?php echo HTML::mailto('mailto:'.$email_bill, $email_bill)?></p>
				<p>Marketing: <?php echo HTML::mailto('mailto:'.$email_mark, $email_mark)?> <br />
				Sales: <?php echo HTML::mailto('mailto:'.$email_sales, $email_sales)?></p>
          </div>
          <div class="dropshadow">
            <img src="/media/customer/img/med_shadow180.png" alt="dropshadow">
          </div>
        </div>
      </div>
    </div>

</div>
<!-- <div class="row"> -->
	<div class="getintouch_form">
<!-- 		<div class="dotted"></div> -->
			<form action="/contact/email_us" id="contact_form">

				<div class="gray_form_elements" style="overflow:hidden;">
				<h4>Get In Touch</h4>
				<div style="width:250px;" class="pull-left">
					<input type="text" name="name" placeholder="Full Name:" class="input-block-level"/>
					<input type="text" name="phone" placeholder="Number:" class="input-block-level"/>
					<input type="text" name="email" placeholder="Email Address:" class="input-block-level"/>
				</div>
				<div style="width:520px; margin-left:58px; float: left;">
					<textarea name="message" class="input-block-level" placeholder="Message Us:"></textarea>
				</div>
				</div>
					<span class="gray-button-wrap"><input type="submit" class="gray-button" style="float:right; width: 129px" value="SEND NOW" id="contact_submit"></span>
			</form>
	</div>
<!-- </div> -->

