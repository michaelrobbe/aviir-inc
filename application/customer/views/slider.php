<?php
global $slides;
?>

<div id="slideshow-wrapper">
	<div id="slideshow" class="fluid12">
		<div id="full-width-slider" class="royalSlider heroSlider rsHor rsMinW">
		  <?php
		  for($i=0; $i < count($slides); $i++) {
		  	echo '<div class="rsContent slide' . ($i+1) . '">';
		  	echo HTML::image($slides[$i], array('alt' => 'Slideshow - Reduce your chance for a heart attack'));
		  	echo '</div>';
		  }
		  ?>
		</div>
	</div>
</div>
<div class="dropshadow">
	<?php echo HTML::image('media/customer/img/slider_shadow.png', array('alt' => 'dropshadow')); ?>
</div>