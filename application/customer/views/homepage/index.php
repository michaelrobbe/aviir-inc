<?php

$slider_items = ORM::factory('Content')->where('node_name', '=', 'slider')->find_all();
global $slides;
$slides = array();

foreach($slider_items AS $item) {
	$slides[] = ($link = $item->get_image_value('image', 'raw'));
}
?>
<div id="content" class="row-fluid">
  <?php
  echo $slider;
  ?>

<div id="box-columns" class="row-fluid">
  <?php
  foreach ($boxes as $box):
  	$title = $box->get_field_value('title');
  	$blurb = $box->get_field_value('blurb');
  	$link = $box->get_field_value('link');
  	$description = $box->get_field_value('description');
  	$image = $box->get_image_value('image', 'medium');
  ?>

    <div class="box-column-wrapper span4">
      <div class="box-column">
        <div class="box-column-inner">
          <?php echo HTML::anchor($link, '<span class="linkdiv"></span>'); ?>
          <div class="box-header">
            <h2><?php echo $title; ?></h2>
          </div>
          <div class="box-image">
            <div class="dropshadow">
              <?php echo HTML::image('media/customer/img/med_shadow180.png', array('alt' => 'dropshadow')); ?>
            </div>
            <?php echo HTML::image($image, array('alt' => 'Health professionals can use Aviir to order tests')); ?>
            <div class="dropshadow bottom-shadow">
              <?php echo HTML::image('media/customer/img/med_shadow.png', array('alt' => 'dropshadow')); ?>
            </div>
          </div>
          <div class="box-content-header">
            <h3><?php echo $blurb; ?></h3>
          </div>
          <div class="box-content">
            <p><?php echo $description; ?></p>
          </div>
          <div class="dropshadow">
            <?php echo HTML::image('media/customer/img/med_shadow180.png', array('alt' => 'dropshadow')); ?>
          </div>
        </div>
      </div>
    </div>

<?php endforeach; ?>
</div>