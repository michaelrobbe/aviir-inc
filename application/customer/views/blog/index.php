<?php
    echo HTML::style('media/customer/css/blog.css')
?>

<script>
	$(document).ready(function(){
		$('#mail_form_below').click(function(){
			 $("html, body").animate({ scrollTop: $(document).height() }, "slow");
  			 return false;
		});
	});
</script>
<div class="row">
<!-- 	<div class="span12 blog_filter"> -->
<!-- 		<h4>FILTER BY:  -->
            <?php
//                 $filter_methods = array('all', 'news', 'events');
//                 foreach ($filter_methods as $filter_method)
//                 {
//                     $filter_class = '';
//                     if ($filter_method == $current_filter_method)
//                     {
//                         $filter_class = 'filter_selected';
//                     }
//                     echo HTML::anchor('/blog?filter_by='.$filter_method, strtoupper($filter_method), array('class' => $filter_class));
//                 }
            ?>
<!--             <div class='blog_read_more pull-left' style="width: 370px; float: right; margin-top: 2px;"></div>  -->
<!--        </h4> -->
<!-- 	</div> -->
</div>
<!-- <div class="row"> -->
	<div class="span8 blog_news">
		<h2>NEWS</h2>
            <div class="dropshadow">
              <?php echo HTML::image('media/customer/img/med_shadow180.png', array('alt' => 'dropshadow')); ?>
            </div>
	<?php
		foreach($news_items as $item)
		{
			$item = ORM::factory('Content', $item->id);
			$title = $item->get_field_value('title');
			$urltitle = $item->get_field_value('urltitle');
			$post_content = $item->get_field_value('post_content');
			$image = $item->get_image_value('image', 'small');
			$date = Date('m/d/Y', $item->get_field_value('date'));

            echo "<div class='blog_post_item'>";
// 			if($item->node_name == "blog_events")
// 			{
// 				echo HTML::image('media/customer/img/event_bubble.png', array('class' => 'blog_bubble visible-desktop'));
// 				echo "<div class='blog_events_title'>";
// 			}
// 			elseif($item->node_name == "blog_news")
// 			{
// 				echo HTML::image('media/customer/img/new_news_bubble.png', array('class' => 'blog_bubble visible-desktop'));
// 				echo "<div class='blog_news_title'>";
// 			}
// 			else
// 			{
// 				echo HTML::image('media/customer/img/press_bubble.png', array('class' => 'blog_bubble visible-desktop'));
// 				echo "<div class='blog_press_title'>";
// 			}
			echo "<div class='post_box'>";
            echo HTML::anchor('blog/post/'.$urltitle, '<span class="linkdiv"></span>' );
			echo "<h3 class='blog_header_title'>" . Text::limit_chars($title, 50, '...', true);
			echo "</h3>";
			$time = $item->get_field_value('time');
			$address = $item->get_field_value('address');
			echo '<div class="date_info">'.$date.' | | '.$address.'</div>';
			echo "</div>";
//             echo "<div>";
// 			if ($image)
// 			{
// 			    echo HTML::image($image, array('style' => 'float:left; padding:10px;'));
// 			}
// 			$post_content = preg_replace('/<a.*?<\/a>/', '', $post_content);
//             echo Text::limit_chars(strip_tags($post_content), 300);
//             echo "</div>";
// 			echo "<div class='blog_read_more pull-left'></div>";
// 			echo "<div class='pull-left'>";
// 			echo Html::anchor('blog/post/'.$urltitle, 'Read More', array('class' => 'btn btn-red blog_read_more_button'));
// 			echo "</div>";
			echo "</div>";

		}
// 			echo "</div>";
	?>
	</div>

		<div class="span3 blog_events offset1">
		<h2>EVENTS</h2>
            <div class="dropshadow">
              <?php echo HTML::image('media/customer/img/med_shadow180.png', array('alt' => 'dropshadow')); ?>
            </div>
	<?php
		foreach($event_items as $item)
		{
			$item = ORM::factory('Content', $item->id);
			$title = $item->get_field_value('title');
			$urltitle = $item->get_field_value('urltitle');
			$post_content = $item->get_field_value('post_content');
			$image = $item->get_image_value('image', 'small');
			$date = Date('M', $item->get_field_value('date'));
			$date_day = Date('d', $item->get_field_value('date'));
/**
 * previous layout
 * $date = Date('m/d/Y', $item->get_field_value('date'));
 */

            echo "<div class='blog_post_item'>";
// 			if($item->node_name == "blog_events")
// 			{
// 				echo HTML::image('media/customer/img/event_bubble.png', array('class' => 'blog_bubble visible-desktop'));
// 				echo "<div class='blog_events_title'>";
// 			}
// 			elseif($item->node_name == "blog_news")
// 			{
// 				echo HTML::image('media/customer/img/new_news_bubble.png', array('class' => 'blog_bubble visible-desktop'));
// 				echo "<div class='blog_news_title'>";
// 			}
// 			else
// 			{
// 				echo HTML::image('media/customer/img/press_bubble.png', array('class' => 'blog_bubble visible-desktop'));
// 				echo "<div class='blog_press_title'>";
// 			}
			echo "<div class='post_box'>";
            echo HTML::anchor('blog/post/'.$urltitle, '<span class="linkdiv"></span>' );
			echo "<h3 class='blog_header_title'>" . Text::limit_chars($title, 44, '...', true);
			echo "</h3>";
// 			echo "</div>";
			$time = $item->get_field_value('time');
			$address = $item->get_field_value('address');
			echo '<div class="date_info date_indicator"><span></span><p>'.$date.'</p><p>'.$date_day.'</p></div>';
/**
 * Previous Layout
 * echo '<div class="date_info">'.$date.' | | '.$address.'</div>';
 */
//             echo "<div>";
// 			if ($image)
// 			{
// 			    echo HTML::image($image, array('style' => 'float:left; padding:10px;'));
// 			}
// 			$post_content = preg_replace('/<a.*?<\/a>/', '', $post_content);
//             echo Text::limit_chars(strip_tags($post_content), 300);
//             echo "</div>";
// 			echo "<div class='blog_read_more pull-left'></div>";
// 			echo "<div class='pull-left'>";
// 			echo Html::anchor('blog/post/'.$urltitle, 'Read More', array('class' => 'btn btn-red blog_read_more_button'));
			echo "</div>";
			echo "</div>";

		}
// 			echo "</div>";
	?>
	</div>


<!-- 	<div class="span4" style="margin-top:20px;"> -->
<!-- 		<div class="blue_white_stripe_header"> -->
<!-- 		    <h5>FIND A PSC LOCATION</h5> -->
		    <?php
// 		    echo Html::image('media/customer/img/maps_icon.png', array('class'=>'maps_box_icon', 'style' => 'float: right; margin-right: 10px;'))
		    ?>
<!-- 	    </div> -->
<!-- 		<div class="blog_search_box"> -->
<!-- 			<div class="search_box_interior"> -->
<!-- 				<p><small>Please fill out the form below with your address and we  will find a PSC location nearest you.</small></p> -->
<!-- 				<form action="/contact/psc_locations"> -->
<!-- 					<input name="address"  class="input-block-level" type="text" placeholder="ADDRESS:"><br/> -->
<!-- 					<input name="city" class="input-block-level" type="text" placeholder="CITY:"><br/> -->
<!-- 					<input name="state" class="input-mini" type="text" placeholder="STATE:"> -->
<!-- 					<input name="zip" class="input-small" type="text" placeholder="ZIP:"><br/> -->
<!-- 					<div class="dotted"></div> -->
<!-- <div style="text-align: center;"> -->
<!-- 					    <input class="btn btn-red" type="submit" value="SUBMIT"/> -->
<!-- 					</div> -->
<!-- 				</form> -->
<!-- 			</div> -->
<!-- 		</div> -->
<!-- 		<div class="blue_white_stripe_header"> -->
<!-- 		    <h5>CONTACT THE LAB</h5> -->
		    <?php
// 		    echo Html::image('media/customer/img/test_tubes.png', array('class'=>'test_tubes_box_icon', 'style' => 'float: right; margin-right: 10px;'));
		    ?>
<!-- 	    </div> -->
<!-- 		<div class="gray_box"> -->
<!-- 			<div class="gray_box_interior"> -->
<!-- 				<p><small>If you need to contact the lab, you can reach us from 8 am - 7 pm PST, Monday through Friday.</small></p> -->
<!--                     Phone: <b>(949) 398-6300</b><br/> -->
<!--                     Fax: <b>(949) 333-1442</b><br/> -->
<!--                     Email:  -->
                    <?php
//                     echo html::mailto('clientservices@aviir.com');
                    ?>
<!--                     <br/> -->
<!--                     Address: -->
<!--                     <b>9805 Research Dr.<br/> -->
<!--                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Irvine, CA 92618</b> -->
<!-- 			</div> -->
<!-- 		</div> -->
<!-- 	</div> -->
<!-- </div> -->

<div class="row">
<?php
	echo "<div class='span8'>";
// 	echo "<div class='dotted'></div>";
	echo "<div class='span4 offset4'>";
	echo $pagination;
	echo '</div>';
	echo '</div>';
?>
</div>

