<?php
    echo HTML::style('media/customer/css/blog.css')
?>

<div class="row">
    <div class="span12">
<?php

	$title =  $item->get_field_value('title');
	$post_content = str_replace('<div>', '<p>',$item->get_field_value('post_content'));
	$post_content = str_replace('</div>', '</p>', $post_content);
	$image = $item->get_image_value('image');
	$date = Date('m/d/Y', $item->get_field_value('date'));
	$text1 = Text::limit_words($post_content, 100, '');
	$text2 = explode($text1, $post_content);

	if($item->node_name == "blog_events")
	{
// 		echo Html::image('media/customer/img/event_bubble.png', array('class' => 'blog_full_bubble'));
// 		echo "<div class='blog_events_title' style='height:auto;'>";
//         echo "</div>";
		echo "<div class='post_box'>";
		echo "<h3 class='blog_header_title'>".$title."</h3>";
		$time = $item->get_field_value('time');
		$address = $item->get_field_value('address');
		echo '<div class="date_info">'.$date.' | '.$time.' | '.$address.'</div>';
		if ($image)
		{

			echo '<div class="span5">';
			echo Html::image($image);
			echo $text1;
			echo '</div>';
			echo '<div class="span5">';
			echo $text2[1];
			echo '</div>';
		}
		else
		{
			echo "<div>";
			echo $post_content;
			echo "</div>";

		}
	}
	elseif ($item->node_name == "blog_news")
	{
// 		echo Html::image('media/customer/img/new_news_bubble.png', array('class' => 'blog_full_bubble'));
// 		echo "<div class='blog_news_title' style='height:auto;'>";
//         echo "</div>";
		echo "<div class='post_box'>";
        echo "<h3 class='blog_header_title'>".$title."</h3>";
		$author = $item->get_field_value('author');
		echo '<div class="date_info">Posted On '.$date.' | by '.$author.'</div>';
		if ($image)
		{
			echo '<div class="span5">';
			echo Html::image($image);
			echo $text1;
			echo '</div>';
			echo '<div class="span5">';
			echo $text2[1];
			echo '</div>';
		}

		else
		{
			echo "<div>";
			echo $post_content;
			echo "</div>";

		}
	}
	else
	{
// 		echo Html::image('media/customer/img/press_bubble.png', array('class' => 'blog_full_bubble'));
// 		echo "<div class='blog_press_title' style='height:auto;'>";
// 		echo "</div>";
		echo "<div class='post_box'>";
		echo "<h3 class='blog_header_title'>".$title."</h3>";
		$author = $item->get_field_value('author');
		echo '<div class="date_info">Posted On '.$date.' | by '.$author.'</div>';
		if ($image)
		{
			echo '<div class="span5">';
			echo Html::image($image);
			echo $text1;
			echo '</div>';
			echo '<div class="span5">';
			echo $text2[1];
			echo '</div>';
		}

		else
		{
			echo "<div>";
			echo $post_content;
			echo "</div>";

		}
	}
?>
    </div>
</div>