<?php
	$step_number = $step_item->get_field_value('step_number');
	$title = $step_item->get_field_value('title');
	$heading = $step_item->get_field_value('heading');
	$description = $step_item->get_field_value('description');
	$image = $step_item->get_image_value('image2');
	$lrg_image = $step_item->get_image_value('image','xlarge');
	$smlink = $step_item->get_field_value('small_image_link');
?>
<div class="row">
	<div class="span12">
	<?php 
		if ($lrg_image)
		{
			echo Html::image($lrg_image);
		}	
	?>
	</div>
</div>
<div class="row" style="margin-bottom:10px; margin-top:20px;">
	<div class="span12" style="margin-top:-138px;">
	<?php 
		$steps = ORM::factory('Content')->where('node_name', '=', 'patients_steps')->find_all();
		$count = 1;
		
		foreach($steps as $item)
		{
		    $stitle = $item->get_field_value('title');
            $sheading = $item->get_field_value('heading');
            
		    $formatted_protitle = strtolower(str_replace(' ', '_', $sheading));
			$link = '/patients/'.$formatted_protitle;
			if ($step_number == $count)
			{
				echo "<div class='float_left'>";
			}
			else
			{
				echo "<div class='float_left' style='margin-top:50px;'>";
			}	
			
			echo "<div class='step_box_header'></div>";
			echo "<a href='".$link."'>";
			if ($step_number == $count)
			{
				echo "<div class='step_box' style=' min-height:0px; height:130px; color:black;'>";
			}
			else
			{
				echo "<div class='step_step_box' style='color:black; min-height:80px;'>";
			}	
			
			echo "<h4>".strtoupper($sheading)."</h4>";
			echo Html::image('media/customer/img/red_bubble.png', array('class' => 'step_read_more_patients'));
			echo "</div>";
			echo '<span class="step_text_overlay" style="color:white;">'.$count.'</span>';
			echo "</div>";
			echo '</a>';
			$count ++;
		}
	?>
	</div>
</div>
<div class="row">
	<div class="span9">
		<?php 
			echo "<h3 class='red_header'>".$heading."</h3>";
			echo $description;
		?>
	</div>
	<div class="span3">
		<?php
			if ($image)
            {
                if ($smlink AND $smlink != '')
                {
                    echo HTML::anchor($smlink, HTML::image($image, array('style' => 'width:200px; float:right;')));
                }
                else
                {
                    echo HTML::image($image, array('style' => 'width:200px; float:right;'));
                }
            }
		?>
	</div>
</div>
<div class="row">
	<div class="span12 next_step">
		<?php 
			if($step_number < 5)
			{
				$step_number ++;
                
                $content_field = ORM::factory('Contentfield')
                ->where('field_name', '=', 'step_number')
                ->where('value', '=', $step_number)
                ->where('node_name', '=', 'patients_steps')
                ->join('contents')
                ->on('contentfield.content_id', '=', 'contents.id')
                ->find();
                $step_title = Format::url_title($content_field->content->contentfields->where('field_name', '=', 'heading')->find()->value);
                
				echo Html::anchor('patients/'.$step_title, '<span class="red_header"><strong>TAKE THE NEXT STEP</strong></span>&nbsp;<i class="icon-arrow-right"></i>');
			}
		?>
	</div>
</div>

