<br/>
<div style="text-align: center;">
	<p>Your request has been submitted. A confirmation will be sent to you shortly via email or by phone.</p>
	<p><a href="<?php echo URL::site('/'); ?>">Click here to return to the main page</a></p>
</div>
<br/><br/>