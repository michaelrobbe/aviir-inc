<?php
    echo HTML::script('media/customer/js/bootstrap-datepicker.js');
    echo HTML::style('media/customer/css/datepicker.css');

    echo HTML::script('media/customer/js/bootstrap-timepicker.js');
    echo HTML::style('media/customer/css/timepicker.css');

	echo HTML::script('media/common/js/libs/jquery.validate.min.js');
?>
<script>
    $(function() {
        $('.datepicker').datepicker();
        $('.timepicker').timepicker();

        $("#blood_draw").validate({
        	rules: {
				insurance_company: {
					required: function(element) {
						return ($('input[name="insurance_medicare"]').val() == '');
					}
				},
				insurance_policy: {
					required: function(element) {
						return ($('input[name="insurance_medicare"]').val() == '');
					}
				},
				insurance_first_name: {
					required: function(element) {
						return ($('input[name="insurance_medicare"]').val() == '');
					}
				},
				insurance_last_name: {
					required: function(element) {
						return ($('input[name="insurance_medicare"]').val() == '');
					}
				},
				insurance_group: {
					required: function(element) {
						return ($('input[name="insurance_medicare"]').val() == '');
					}
				},
				insurance_medicare: {
					required: function(element) {
						return $('input[name="insurance_company"]').val() == '';
					}
				},
        	}
        });
    });
</script>

<style>
    .header_bar {
        width:300px;
    }
</style>

<div class="row">
    <div class="span12">
    <div class="tab-link-wrapper">
        <p><a id="tab-link-1" class="tab-links" href="#tab-1"><span class="circle">1</span>General Information</a></p>
        <p><a id="tab-link-2" class="tab-links" href="#tab-2"><span class="circle">2</span>Address for Service</a></p>
        <p><a id="tab-link-3" class="tab-links" href="#tab-3"><span class="circle">3</span>Insurance Information</a></p>
        <p><a id="tab-link-4" class="tab-links" href="#tab-4"><span class="circle">4</span>Appointment Details</a></p>

    </div>

    	<div class="blood_draw_info">
        <p>
            <?php echo $blood_draw->get_field_value('description'); ?>
        </p>
    	</div>

        <?php
            echo Form::open('professionals/blood_draw', array('class' => 'form-horizontal', 'id' => 'blood_draw'));
        ?>
        <div class="blood_draw_wrap clearfix">
        <div class="royalSlider rsMinW">

      <div class="tab-wrapper-1 form-tab-content rsContent clearfix">
        <div class="tab-1">
          <div class="tab-inner-1">
            <fieldset>
              <?php
//               echo '<h3><span class="circle">1</span>General Information</h3>';
            $field_name = 'first_name';
            $field_display = 'Patient First Name';
            echo '<div class="control-group">';
//             echo Form::label($field_name, $field_display, array('class' => 'control-label'));
            echo '<div class="controls">';
            echo Form::input($field_name, '', array('class' => 'required', 'placeholder' => $field_display));
            echo '</div>';
            echo '</div>';

            $field_name = 'last_name';
            $field_display = 'Patient Last Name';
            echo '<div class="control-group">';
//             echo Form::label($field_name, $field_display, array('class' => 'control-label'));
            echo '<div class="controls">';
            echo Form::input($field_name, '', array('class' => 'required', 'placeholder' => $field_display));
            echo '</div>';
            echo '</div>';

            $field_name = 'phone';
            $field_display = 'Patient Phone Number';
            echo '<div class="control-group">';
//             echo Form::label($field_name, $field_display, array('class' => 'control-label'));
            echo '<div class="controls">';
            echo Form::input($field_name, '', array('class' => 'required', 'placeholder' => $field_display));
            echo '</div>';
            echo '</div>';

            $field_name = 'email';
            $field_display = 'Patient Email Address';
            echo '<div class="control-group">';
//             echo Form::label($field_name, $field_display, array('class' => 'control-label'));
            echo '<div class="controls">';
            echo Form::input($field_name, '', array('class' => 'required email', 'placeholder' => $field_display));
            echo '</div>';
            echo '</div>';
            ?>
            </fieldset>
          </div>
        </div>
      </div>

      <div class="tab-wrapper-2 form-tab-content rsContent clearfix">
        <div class="tab-2">
          <div class="tab-inner-2">
            <fieldset>
              <?php
//             echo '<h3><span class="circle">2</span>Address for Service to be Performed</h3>';

            $field_name = 'address';
            $field_display = 'Street';
            echo '<div class="control-group">';
//             echo Form::label($field_name, $field_display, array('class' => 'control-label'));
            echo '<div class="controls">';
            echo Form::input($field_name, '', array('class' => 'required', 'placeholder' => $field_display));
            echo '</div>';
            echo '</div>';

            $field_name = 'address_2';
            $field_display = 'Suite, Apt. Etc. (optional)';
            echo '<div class="control-group">';
//             echo Form::label($field_name, $field_display, array('class' => 'control-label'));
            echo '<div class="controls">';
            echo Form::input($field_name, '', array('class' => '', 'placeholder' => $field_display));
            echo '</div>';
            echo '</div>';

            $field_name = 'city';
            $field_display = 'City';
            echo '<div class="control-group">';
//             echo Form::label($field_name, $field_display, array('class' => 'control-label'));
            echo '<div class="controls">';
            echo Form::input($field_name, '', array('class' => 'required', 'placeholder' => $field_display));
            echo '</div>';
            echo '</div>';

            $field_name = 'state';
            $field_display = 'State';
            echo '<div class="control-group">';
//             echo Form::label($field_name, $field_display, array('class' => 'control-label'));
            echo '<div class="controls">';
            ?>

			<select name="state" class="required">
				<option value="" disabled="disabled" selected="selected">Please Select a State</option>
				<option value="AL">Alabama</option>
				<option value="AK">Alaska</option>
				<option value="AZ">Arizona</option>
				<option value="AR">Arkansas</option>
				<option value="CA">California</option>
				<option value="CO">Colorado</option>
				<option value="CT">Connecticut</option>
				<option value="DE">Delaware</option>
				<option value="DC">District of Columbia</option>
				<option value="FL">Florida</option>
				<option value="GA">Georgia</option>
				<option value="HI">Hawaii</option>
				<option value="ID">Idaho</option>
				<option value="IL">Illinois</option>
				<option value="IN">Indiana</option>
				<option value="IA">Iowa</option>
				<option value="KS">Kansas</option>
				<option value="KY">Kentucky</option>
				<option value="LA">Louisiana</option>
				<option value="ME">Maine</option>
				<option value="MD">Maryland</option>
				<option value="MA">Massachusetts</option>
				<option value="MI">Michigan</option>
				<option value="MN">Minnesota</option>
				<option value="MS">Mississippi</option>
				<option value="MO">Missouri</option>
				<option value="MT">Montana</option>
				<option value="NE">Nebraska</option>
				<option value="NV">Nevada</option>
				<option value="NH">New Hampshire</option>
				<option value="NJ">New Jersey</option>
				<option value="NM">New Mexico</option>
				<option value="NY">New York</option>
				<option value="NC">North Carolina</option>
				<option value="ND">North Dakota</option>
				<option value="OH">Ohio</option>
				<option value="OK">Oklahoma</option>
				<option value="OR">Oregon</option>
				<option value="PA">Pennsylvania</option>
				<option value="RI">Rhode Island</option>
				<option value="SC">South Carolina</option>
				<option value="SD">South Dakota</option>
				<option value="TN">Tennessee</option>
				<option value="TX">Texas</option>
				<option value="UT">Utah</option>
				<option value="VT">Vermont</option>
				<option value="VA">Virginia</option>
				<option value="WA">Washington</option>
				<option value="WV">West Virginia</option>
				<option value="WI">Wisconsin</option>
				<option value="WY">Wyoming</option>
			</select>

            <?php
            echo '</div>';
            echo '</div>';

            $field_name = 'zip';
            $field_display = 'Zip';
            echo '<div class="control-group">';
//             echo Form::label($field_name, $field_display, array('class' => 'control-label'));
            echo '<div class="controls">';
            echo Form::input($field_name, '', array('class' => 'required', 'placeholder' => $field_display));
            echo '</div>';
            echo '</div>';
            ?>
            </fieldset>
          </div>
        </div>
      </div>

      <div class="tab-wrapper-3 form-tab-content rsContent clearfix">
              <div class="tab-3">
                <div class="tab-inner-3">
                  <fieldset>
                    <?php
//             echo '<h3><span class="circle">3</span>Insurance Information*</h3>';
            echo '<p>Please fill in your Medicare No. OR all 5 insurance information fields</p>';

            $field_name = 'insurance_medicare';
            $field_display = 'Medicare No.';
            echo '<div class="control-group">';
            //             echo Form::label($field_name, $field_display, array('class' => 'control-label'));
            echo '<div class="controls">';
            echo Form::input($field_name, '', array('class' => '', 'placeholder' => $field_display));
            echo '</div>';
            echo '</div>';

            echo '<h4 style="text-align:center;">OR</h4>';

            $field_name = 'insurance_company';
            $field_display = 'Insurance Company';
            echo '<div class="control-group">';
//             echo Form::label($field_name, $field_display, array('class' => 'control-label'));
            echo '<div class="controls">';
            echo Form::input($field_name, '', array('class' => '', 'placeholder' => $field_display));
            echo '</div>';
            echo '</div>';

            $field_name = 'insurance_policy';
            $field_display = 'I.D. or Policy No.';
            echo '<div class="control-group">';
//             echo Form::label($field_name, $field_display, array('class' => 'control-label'));
            echo '<div class="controls">';
            echo Form::input($field_name, '', array('class' => '', 'placeholder' => $field_display));
            echo '</div>';
            echo '</div>';

            $field_name = 'insurance_first_name';
            $field_display = 'Policy Holder First Name';
            echo '<div class="control-group">';
//             echo Form::label($field_name, $field_display, array('class' => 'control-label'));
            echo '<div class="controls">';
            echo Form::input($field_name, '', array('class' => '', 'placeholder' => $field_display));
            echo '</div>';
            echo '</div>';

            $field_name = 'insurance_last_name';
            $field_display = 'Policy Holder Last Name';
            echo '<div class="control-group">';
//             echo Form::label($field_name, $field_display, array('class' => 'control-label'));
            echo '<div class="controls">';
            echo Form::input($field_name, '', array('class' => '', 'placeholder' => $field_display));
            echo '</div>';
            echo '</div>';

            $field_name = 'insurance_group';
            $field_display = 'Group No.';
            echo '<div class="control-group">';
//             echo Form::label($field_name, $field_display, array('class' => 'control-label'));
            echo '<div class="controls">';
            echo Form::input($field_name, '', array('class' => '', 'placeholder' => $field_display));
            echo '</div>';
            echo '</div>';
        ?>
                  </fieldset>
                </div>
              </div>
            </div>

            <div class="tab-wrapper-4 form-tab-content rsContent clearfix">
              <div class="tab-4">
                <div class="tab-inner-4">
                  <fieldset>
                    <?php
//             echo '<h3><span class="circle">4</span>Appointment Details</h3>';

            $field_name = 'appointment_date_1';
            $field_display = '1st Choice Appointment Date';
            echo '<div class="control-group">';
//             echo Form::label($field_name, $field_display, array('class' => 'control-label'));
            echo '<div class="controls">';
            echo Form::input($field_name, '', array('class' => 'datepicker required', 'placeholder' => $field_display));
            echo '</div>';
            echo '</div>';

            $field_name = 'appointment_time_1';
            $field_display = 'Preferred Time';
            echo '<div class="control-group">';
//             echo Form::label($field_name, $field_display, array('class' => 'control-label'));
            echo '<div class="controls">';
            echo Form::input($field_name, '8:00 AM', array('class' => 'timepicker required', 'placeholder' => $field_display));
            echo '</div>';
            echo '</div>';

            $field_name = 'appointment_date_2';
            $field_display = '2nd Choice Appointment Date';
            echo '<div class="control-group">';
//             echo Form::label($field_name, $field_display, array('class' => 'control-label'));
            echo '<div class="controls">';
            echo Form::input($field_name, '', array('class' => 'datepicker required', 'placeholder' => $field_display));
            echo '</div>';
            echo '</div>';

            $field_name = 'appointment_time_2';
            $field_display = 'Preferred Time';
            echo '<div class="control-group">';
//             echo Form::label($field_name, $field_display, array('class' => 'control-label'));
            echo '<div class="controls">';
            echo Form::input($field_name, '10:00 AM', array('class' => 'timepicker required', 'placeholder' => $field_display));
            echo '</div>';
            echo '</div>';
            echo '<p>'.$blood_draw->get_field_value('closing').'</p>';

            echo '<div class="control-group"><div class="controls">';
            echo Form::submit('submit', 'Submit', array('class' => 'gray-button', 'style' => 'width: 100px'));
            echo '</div></div>';
            ?>
                  </fieldset>
                </div>
              </div></div>

                </div>
                <?php

            echo Form::close();
        ?>
        	</div>
              </div>
            </div>