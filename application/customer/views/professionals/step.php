<?php
	$step_number = $step_item->get_field_value('step_number');
	$title = $step_item->get_field_value('title');
	$heading = $step_item->get_field_value('heading');
	$description = $step_item->get_field_value('description');
	$image = $step_item->get_image_value('image2');
	$lrg_image = $step_item->get_image_value('image','xlarge');
	$smlink = $step_item->get_field_value('small_image_link');
?>

<?php foreach($step_item AS $k => $v): ?>
<div class="tab-<?php echo $k; ?>">
<div class="row">
	<div class="span12">
	<?php 
		if ($lrg_image)
		{
			echo Html::image($lrg_image);
		}	
	?>
	</div>
</div>
<div class="row" style="margin-bottom:10px; margin-top:20px;">
	<div class="span12" style="margin-top:-138px;">
	<?php 
		$steps = ORM::factory('Content')->where('node_name', '=', 'professionals_steps')->find_all();
		$count = 1;
		
		foreach($steps as $item)
		{
		    $stitle = $item->get_field_value('title');
		    $sheading = $item->get_field_value('heading');
            
		    $formatted_protitle = strtolower(str_replace(' ', '_', $stitle));
			$link ='/professionals/'.$formatted_protitle;
			if ($step_number == $count)
			{
				echo "<div class='float_left'>";
			}
			else
			{
				echo "<div class='float_left' style='margin-top:50px;'>";
			}	
			
			echo "<div class='step_box_header'></div>";
			echo "<a href='".$link."'>";
			if ($step_number == $count)
			{
				echo "<div class='step_box' style=' min-height:0px; height:110px; color:black;'>";
			}
			else
			{
				echo "<div class='step_step_box' style='color:black;'>";
			}	
			
			echo "<h4>".strtoupper($sheading)."</h4>";
			echo "</div>";
			echo "</div>";
			echo "</a>";
			$count ++;
		}
	?>
	</div>
</div>
<div class="row">
	<div class="span9">
		<?php 
			echo "<h3 class='red_header'>".$heading."</h3>";
			echo $description;
		?>
	</div>
	<div class="span3">
		<?php
            if ($image)
			{
			    if ($smlink AND $smlink != '')
                {
                    echo HTML::anchor($smlink, HTML::image($image,array('style' => 'width:200px; float:right;')));
                }
                else
                {
                    echo HTML::image($image,array('style' => 'width:200px; float:right;'));
                }
			}
		?>
	</div>
</div>
<div class="row">
	<div class="span12 next_step">
		<?php 
			if($step_number < 5)
			{
				$step_number ++;
                
                $content_field = ORM::factory('Contentfield')
                ->where('field_name', '=', 'step_number')
                ->where('value', '=', $step_number)
                ->where('node_name', '=', 'professionals_steps')
                ->join('contents')
                ->on('contentfield.content_id', '=', 'contents.id')
                ->find();
                $step_title = Format::url_title($content_field->content->contentfields->where('field_name', '=', 'heading')->find()->value);
                
				echo Html::anchor('professionals/'.$step_title, '<span class="red_header"><strong>NEXT</strong></span>&nbsp;<i class="icon-arrow-right"></i>');
			}
		?>
	</div>
</div>
</div>
<?php endforeach; ?>
