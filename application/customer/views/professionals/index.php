<?php
global $slides;
$slides = array();
$image_base = 'media/customer/img/';
// $slides[] = 'slider_miriskvp.jpg';
// $slides[] = 'slider_savetheone.jpg';
// $slides[] = 'slider_moments.jpg';
$slides[] = $image_base . 'slider_mirisk_cutaway.jpg';
$slides[] = $image_base . 'professionals_slide2.jpg';
$slides[] = $image_base . 'professionals_slide3.jpg';
$slides[] = $image_base . 'professionals_slide4.jpg';
$slides[] = $image_base . 'professionals_slide5.jpg';

?>

<div class="row">
	<div class="span12">
	<?php
		echo $slider;
	?>
	</div>
</div>
<div class="row">
	<div class="span12 page-tabs">
	<?php
		$count = 1;
		foreach($items as $item)
		{
			$title = $item->get_field_value('title');
			$heading = $item->get_field_value('heading');
			$tab_number = $item->get_field_value('step_number');
			$image = $item->get_image_value('image3', 'small');

            $formatted_title = strtolower(str_replace(' ', '_', $title));
            //$link = '/professionals/'.$formatted_title;
            $link = '#tab-' . $tab_number;
			echo "<div class='float_left'>";
			echo "<div class='tab_caret tab_caret-" . $tab_number. "'></div>";
			echo '<a href="'.$link.'" id="tab-link-' . $tab_number . '" class="tab-link rsTmb">';
			echo "<div class='pro_page_box' style='color:black;'>";
			echo '<span></span>';
			echo "<h4>" . $heading . "</h4>";
			echo Html::image($image, array('class' => 'pro_page_box_image'));
// 			echo Html::image('media/customer/img/red_read_more.png', array('class' => 'step_read_more'));
			echo "</div>";
			echo "</div>";
			echo '</a>';
			$count ++;
		}
	?>
	</div>
</div>
<div class="row home-content" style="margin-top:40px;">
	<div class="span12" style="margin-top:15px; margin-bottom:20px;">
		<div class="page_title_non_header">HEALTH PROFESSIONALS</div>
		<div class="header_bar pull-right span7"></div>
	</div>
	<div class="span8">
		<div class="steps_video_header"><h4>Our Cardiovascular Expert Explains Vulnerable Plaque</h4></div>
		<div class="js-video vimeo widescreen">
			<iframe src="http://player.vimeo.com/video/54545740?&byline=0&title=0&portrait=0" width="100%" height="auto" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
		</div>
	</div>
	<div class="span4">
	<div class="pulse_header">
	    <?php echo HTML::image('media/customer/img/pulseimage.png', array('class'=>'pulse_header_image_pro'));?>
	    <h4>THE PULSE</h4>
    </div>
	<div class="pulse_container">
		<?php
			$count = 1;
			foreach($pulse_items as $pitem)
			{
				$pimage = $pitem->get_image_value('image', 'tiny');
				$ptitle = $pitem->get_field_value('title');
				$pdescription = $pitem->get_field_value('description');
				$plink = $pitem->get_field_value('link');
				$divide = $count / 2;
				echo '<a href="'.$plink.'" target="_blank">';
				if (ceil($divide) == $divide)
				{
					echo "<div class='pulse_item' style='background-color:#dcdcdc;'>";
				}
				else
				{
					echo "<div class='pulse_item'>";
				}

				echo HTML::image($pimage, array('class' => 'img img-polaroid pulse_image'));
				echo "<div class='pulse_content'>";
				echo "<h4 style='color:black;'>".strtoupper($ptitle)."</h4>";
				echo '<small>'.$pdescription.'</small>';
				echo "</div>";
				echo "</div>";
				echo "</a>";
				$count ++;
			}
		?>
		</div>
	</div>
</div>
<?php
foreach ($step_item AS $tab):
	$tab_number = $tab->get_field_value('step_number');
	$title = $tab->get_field_value('title');
	$heading = $tab->get_field_value('heading');
	$description = $tab->get_field_value('description');
	$image = $tab->get_image_value('image2');
	$lrg_image = $tab->get_image_value('image','xlarge');
	$smlink = $tab->get_field_value('small_image_link');

?>
<div class="tab-wrapper-<?php print $tab_number; ?> tabs">
	<div class="tab-<?php print $tab_number; ?>">
		<div class="tab-inner-<?php print $tab_number; ?>">
			<h1><?php echo $heading; ?></h1>
			<?php echo $description; ?></a>
		</div>
	</div>
</div>
<?php endforeach; ?>
