<!-- <div class="row"> -->
<?php
	foreach($items as $item)
	{
		$name = $item->get_field_value('name');
		$title = $item->get_field_value('title');
		$description = $item->get_field_value('description');
		$image = $item->get_image_value('image');

		echo "<div class='element_description'>";
		echo "<div class='element_content clearfix'>";
        if ($image)
        {
            echo Html::image($image, array('class' => 'exec_image'));
        }
//         echo $image;
		echo "<h3>".$name."</h3>";
		echo "<h4 class='exec_title'>".$title."</h4>";
		echo $description;
		echo "</div>";
		echo "</div>";
// 		echo '<div class="span12 exec_header"></div>';

	}
?>
<!-- </div> -->

