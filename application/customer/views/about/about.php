<div class="row">
	<div class="span12">
	<?php
// 		echo $slider;
	?>
	</div>
</div>
<div class="row" style="margin-top:15px; margin-bottom:15px;">
	<div class="span12">
		<div class="page_title_non_header">ABOUT US</div>
		<div class="header_bar pull-right span9"></div>
	</div>
</div>
<div class="row">
	<div class='span8 accordion'>
	<?php
		foreach($items as $item)
		{
			$title = $item->get_field_value('title');
			$description = $item->get_field_value('description');
// 			echo "<div>";
// 			echo '<h4 class="about_header">&nbsp;'.$title.'</h4>';
// 			echo '<div class="about_description">';
// 			echo $description;
// 			echo "</div>";
// 			echo "</div>";

		echo '<table>';
		echo '<tr><td><h2>';
		echo $title;
		echo '</h2></td></tr>';
		echo '<tr class="link_body"><td>';
		echo $description;
		echo '</td></tr>';
		echo '</table>';

		}
	?>
	</div>
	<div class="span4">

    <div class="box-column-wrapper span4">
      <div class="box-column">
        <div class="box-column-inner">
<!--           <a href="/professionals"><span class="linkdiv"></span> </a> -->
          <div class="box-header">
            <h2>Search PSC Locations</h2>
          </div>
<!--           <div class="box-image"> -->
            <div class="dropshadow" style="position: static; opacity: 0.5;">
              <img src="/media/customer/img/med_shadow180.png" alt="dropshadow">
            </div>
<!--             <img src="/media/customer/img/hcp_photo.png" alt="Health professionals can use Aviir to order tests"> -->
<!--             <div class="dropshadow bottom-shadow"> -->
<!--               <img src="/media/customer/img/med_shadow.png" alt="dropshadow"> -->
<!--             </div> -->
<!--           </div> -->
<!--           <div class="box-content-header"> -->
<!--             <h3>Change the Future</h3> -->
<!--           </div> -->
          <div class="box-content">
            <form action="/contact/psc_locations">
					<input name="address" type="text" placeholder="Address:"><br/>
					<input name="city" type="text" placeholder="City:"><br/>
					<input name="state" class="input-mini" type="text" placeholder="State:">
					<input name="zip" class="input-small" type="text" placeholder="Zip:"><br/>
<!-- 					<div class="dotted"></div> -->
					<h2 style="text-align: left;">Filter Search</h2>
					<select name="filter">
						<option disabled="disabled" selected="selected">--Please Select an Option--</option>
						<option>50 Miles</option>
						<option>25 Miles</option>
						<option>10 Miles</option>
						<option>5 Miles</option>
					</select>
 				<input class="gray-button" type="submit" value="Search" />
				</form>
          </div>
          <div class="dropshadow">
            <img src="/media/customer/img/med_shadow180.png" alt="dropshadow">
          </div>
        </div>
      </div>
    </div>

        <div class="box-column-wrapper span4">
      <div class="box-column">
        <div class="box-column-inner">
<!--           <a href="/professionals"><span class="linkdiv"></span> </a> -->
          <div class="box-header">
            <h2>Contact The Lab</h2>
          </div>
<!--           <div class="box-image"> -->
            <div class="dropshadow" style="position: static; opacity: 0.5;">
              <img src="/media/customer/img/med_shadow180.png" alt="dropshadow">
            </div>
<!--             <img src="/media/customer/img/hcp_photo.png" alt="Health professionals can use Aviir to order tests"> -->
<!--             <div class="dropshadow bottom-shadow"> -->
<!--               <img src="/media/customer/img/med_shadow.png" alt="dropshadow"> -->
<!--             </div> -->
<!--           </div> -->
<!--           <div class="box-content-header"> -->
<!--             <h3>Change the Future</h3> -->
<!--           </div> -->
          <div class="box-content" style="text-align: left;">
            	<p>If you need to contact the lab, you can reach us from 8 am - 7 pm PST, Monday through Friday.</p>
				<p><strong>Phone:</strong> (949) 398-6300</p>
                    <p><strong>Fax:</strong> (949) 333-1442</p>
                    <p><strong>Email:</strong> <?php echo Html::anchor('mailto:clientservices@aviir.com', 'clientservices@aviir.com'); ?></p>
                    <p><strong>Address:</strong> 9805 Research Dr. Irvine, CA 92618</p>
          </div>
          <div class="dropshadow">
            <img src="/media/customer/img/med_shadow180.png" alt="dropshadow">
          </div>
        </div>
      </div>
    </div>

    <!-- 		<div class="blue_white_stripe_header"><h4>FIND A PSC LOCATION</h4></div> -->
<!-- 		<div class="blog_search_box"> -->
<!-- 			<div class="search_box_interior"> -->
<!-- 				<p><small>Please fill out the form below with your address and we  will find a PSC location nearest you.</small></p> -->
<!-- 				<form action="/contact/psc_locations"> -->
<!-- 					<input name="address"  class="input-block-level" type="text" placeholder="ADDRESS:"><br/> -->
<!-- 					<input name="city" class="input-block-level" type="text" placeholder="CITY:"><br/> -->
<!-- 					<input name="state" class="input-mini" type="text" placeholder="STATE:"> -->
<!-- 					<input name="zip" class="input-small" type="text" placeholder="ZIP:"><br/> -->
<!-- 					<div class="dotted"></div> -->
<!-- 				<input style="margin-left:60px;" class="btn btn-red" type="submit" value="SUBMIT"/> -->
<!-- 				</form> -->
<!-- 			</div> -->
<!-- 		</div> -->
<!-- 		<div class="blue_white_stripe_header"><h4>CONTACT THE LAB</h4></div> -->
<!-- 		<div class="gray_box"> -->
<!-- 			<div class="gray_box_interior"> -->
<!-- 				<p><small>If you need to contact the lab, you can reach us from 8 am - 7 pm PST, Monday through Friday.</small></p> -->
<!-- 				    Phone: <b>(949) 398-6300</b><br/> -->
<!--                     Fax: <b>(949) 333-1442</b><br/> -->
<!--                     Email:clientservices@aviir.com<br/> -->
<!--                     Address: -->
<!--                     <b>9805 Research Dr.<br/> -->
<!--                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Irvine, CA 92618</b> -->
<!-- 			</div> -->
<!-- 		</div> -->

	</div>
</div>
