<div class="row">
	<div class="span12">
		<p>Aviir, Inc. is a privately held company funded by leading Life Sciences and Healthcare venture capital firms:</p>
	</div>
</div>
<div class="row">
<?php
	foreach($items as $item)
	{
		$name = $item->get_field_value('name');
		$link = $item->get_field_value('website_link');
		$description = strip_tags($item->get_field_value('description'));
		$image = $item->get_image_value('image', 'medium');

		echo '<div class="element_description">';
		echo '<div class="element_content clearfix">';

		echo "<div class='investor_image_wrapper'>";
		if ($image)
		{
			echo Html::anchor($link, Html::image($image, array('class' => 'investor_image')));
		}

		echo "</div>";
		echo "<div class='investor_text'>";
		echo "<p>&ldquo; " . $description . " &rdquo;</p>";
		echo Html::anchor($link, "<div class='gray-button'>Visit Their Site</div>",array('target' => '_blank'));
		echo "</div>";

		echo "</div>";
		echo "</div>";
	}
?>
</div>

