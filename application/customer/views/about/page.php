<?php
	$title = $item->get_field_value('title');
	$heading = $item->get_field_value('heading');
	$description = $item->get_field_value('description');
	$image = $item->get_image_value('image');
	$video = $item->get_field_value('video');
?>

<div class="row">
	<div class="span8">
		<?php 
			echo "<h3 class='red_header'>".$title."</h3>";
			echo $description;
		?>
	</div>
	<div class="span4">
		<?php 
			echo Html::image($image);
		?>
	</div>
</div>
<div class="row">
	<div class="span8">
		<?php
		if ($video)
		{
			echo '<iframe src="http://player.vimeo.com/video/'.$video.'?api=1$player_id=player" width="620px" height="349px" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';
		}
		?>
	</div>
</div>

