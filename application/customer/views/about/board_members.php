<div class="row">
<?php
	foreach($items as $item)
	{
		$name = $item->get_field_value('name');

		$name_array = explode('-', $name);

		if(!isset($name_array[1])) {
			$company = '';
		} else {
			$company = ' &ndash; <span class="board_title">' . $name_array[1] . '</span>';
		}

		$description = $item->get_field_value('description');

		echo "<div class='element_description'>";
		echo "<div class='element_content clearfix'>";
		echo "<h3>" . $name_array[0] . $company . "</h3>";
		echo $description;
		echo "</div>";
		echo "</div>";
// 		echo '<div class="span12 exec_header"></div>';
	}
?>
</div>

