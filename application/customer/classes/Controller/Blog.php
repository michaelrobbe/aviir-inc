<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Blog extends Controller_Website {

	public function action_index()
	{
	    $view = View::factory('blog/index');

// 		$filter_by = Arr::get($_GET, 'filter_by', 'all');
//         $view->current_filter_method = $filter_by;
// 		$where2 = "blog_events";
// 		switch($filter_by)
// 		{
// 			case null:
// 			case 'all':
// 				$count = ORM::factory('Content')->where('node_name', '=', 'blog_news')->or_where('node_name', '=', 'blog_events')->or_where('node_name', '=', 'blog_press')->count_all();
// 				$where = "blog_news";
// 				$where2 = "blog_events";
// 				$where3 = "blog_press";
// 				break;
// 			case 'events':
// 				$count = ORM::factory('Content')->where('node_name', '=', 'blog_events')->count_all();
// 				$where = "blog_events";
// 				break;
// 			case 'news':
// 				$where = "blog_news";
// 				$count = ORM::factory('Content')->where('node_name', '=', 'blog_news')->count_all();
// 				break;
// 			case 'press':
// 				$where = "blog_press";
// 				$count = ORM::factory('Content')->where('node_name', '=', 'blog_press')->count_all();
// 				break;
// 		}

	    $count_news = ORM::factory('Content')->where('node_name', '=', 'blog_news')->count_all();
	    $count_events = ORM::factory('Content')->where('node_name', '=', 'blog_events')->count_all();

	    $count = $count_news + $count_events;

		$per_page = 4;
		$page_num = $this->request->param('id', 1);
		$offset   = ($page_num - 1) * $per_page;
		$pagination = Pagination::factory(array(
			'total_items'    => $count,
			'items_per_page' => $per_page,
			'offset'  =>  $offset,
			'view'    =>  'pagination/basic'
	  	));

// 		if ($filter_by == 'all')
// 		{
			$news_items = DB::select('contents.id', 'value')
						->from('contentfields')
						->join('contents', 'left')
						->on('contentfields.content_id', '=', 'contents.id')
                        ->where_open()
						->where('contents.node_name', '=', 'blog_news')
                        ->where_close()
						->where('field_name', '=', 'date')
						->order_by('value', 'DESC')
						->limit($pagination->items_per_page)
						->offset($pagination->offset)
						->as_object()
						->execute();

			$event_items = DB::select('contents.id', 'value')
						->from('contentfields')
						->join('contents', 'left')
						->on('contentfields.content_id', '=', 'contents.id')
						->where_open()
						->where('contents.node_name', '=', 'blog_events')
						->where_close()
						->where('field_name', '=', 'date')
						->order_by('value', 'DESC')
						->limit($pagination->items_per_page)
						->offset($pagination->offset)
						->as_object()
						->execute();
// 		}
// 		else
// 		{
// 			$items = DB::select('contents.id')
// 						->from('contentfields')
// 						->join('contents', 'left')
// 						->on('contentfields.content_id', '=', 'contents.id')
// 						->where('contents.node_name', '=', $where)
// 						->where('field_name', '=', 'date')
// 						->order_by('value', 'DESC')
// 						->limit($pagination->items_per_page)
// 						->offset($pagination->offset)
// 						->as_object()
// 						->execute();
// 		}

		$view->pagination = $pagination;
		$view->news_items = $news_items;
		$view->event_items = $event_items;
// 		$this->template->page_title = "NEWS & EVENTS";
		$this->template->body = $view;
	}

	public function action_post()
	{
		$post_title = $this->request->param('post_title');

		if (is_numeric($post_title))
		{
			$post_id = $post_title;
			$item = ORM::factory('Content', $post_id);
		}
		else
		{
			$item = ORM::factory('Contentfield')
                ->where('field_name', '=', 'urltitle')
                ->where('value', 'LIKE', '%'.$post_title.'%')
				->where_open()
                ->where('node_name', '=', 'blog_events')
				->or_where('node_name', '=', 'blog_news')
				->or_where('node_name', '=', 'blog_press')
				->where_close()
                ->join('contents')
                ->on('contentfield.content_id', '=', 'contents.id')
                ->find();
			$item = ORM::factory('Content', $item->content_id);
		}

		$view = View::factory('blog/post');
		$view->item = $item;
		$this->template->body = $view;
	}

} // End Index
