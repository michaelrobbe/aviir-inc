<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Patients extends Controller_Website {

	public function action_index()
	{
		$items = ORM::factory('Content')->where('node_name', '=', 'patients_steps')->find_all();
		$pulse_items = ORM::factory('Content')->where('node_name', '=', 'pulse')->order_by('id', 'DESC')->find_all();
		$view = View::factory('patients/index');
// 		$slider_items = ORM::factory('Content')->where('node_name', '=', 'slider')->find_all();
		$slider = View::factory('slider');
// 		$slider->slider_items = $slider_items;
		$view->slider = $slider;
		$view->items = $items;
		$view->pulse_items = $pulse_items;
		$this->template->title = 'Patients';

		$view->step_item = $items;
		$this->template->body = $view;
	}

    public function action_static()
    {
        $page = $this->request->param('page');

        if ( ! method_exists($this, 'action_'.$page))
        {
            $title = Format::un_url_title($page);
            $content_field = ORM::factory('Contentfield')
                ->where_open()
                // ->where('field_name', '=', 'title')
                ->or_where('field_name', '=', 'heading')
                ->where_close()
                ->where('value', 'LIKE', '%'.$title.'%')
                ->find();

            if ($content_field->id > 0)
            {
                if (strstr($content_field->content->node_name, 'steps') !== false)
                {
                    $_GET['step'] = $content_field->content->contentfields->where('field_name', '=', 'step_number')->find()->value;
                    $this->action_step();
                }
                elseif (strstr($content_field->content->node_name, 'page') !== false)
                {
                	$_GET['page'] = $content_field->content->id;
                    $this->action_page();
                }
            }
        }
        else
        {
        	$this->template->title = $this->site_name.' - Patients - '.ucwords(str_replace('_', ' ', $page));
            $page_action = 'action_'.$page;
            $this->$page_action();
        }
    }

	public function action_step()
	{
		$step_number = $this->request->param('id', Arr::get($_GET, 'step', 1));

		$items = ORM::factory('Content')->where('node_name', '=', 'patients_steps')->find_all();
		$step_item = '';
		foreach ($items as $item)
		{
			$field = $item->contentfields->where('field_name', '=', 'step_number')->find()->value;
			if ($field == $step_number)
			{
				$step_item = $item;
				$title = $step_item->get_field_value('heading');
				$this->template->title = $this->site_name.' - Patients - '.$title;
			}
		}

		$view = View::factory('patients/step');
		$view->step_item = $step_item;
		$this->template->body = $view;
	}

	public function action_page()
	{
		$page = $this->request->param('id', Arr::get($_GET, 'page', 1));
		$item = ORM::factory('Content', $page);

		$title = $item->get_field_value('title');
		$this->template->title = $this->site_name.' - Patients - '.$title;

		$view = View::factory('patients/page');
		$view->item = $item;
		$this->template->header->title = $item->get_field_value('title');
		$this->template->body = $view;
	}

	/**
	 *
	 * Function has been disabled, and instead pointed at /contact/psc_locations/

	public function action_psc_locations()
	{
		require_once Kohana::find_file('vendor', 'googlemaps/GoogleMap', 'php');
		require_once Kohana::find_file('vendor', 'googlemaps/JSMin', 'php');
		$map = new GoogleMapAPI();
		$message = "Enter Your Address To Find PSCs Near You.";
		$view = View::factory('professionals/psc_locations');
		$psc_locations = ORM::factory('Content')->where('node_name', '=', 'locations')->find_all();
		$user_address = $_GET;
		if ($user_address)
		{
			$distance_scope = Arr::get($user_address, 'scope', 50);
			$psc_addresses = array();
			foreach ($psc_locations as $location)
			{
				$name = $location->get_field_value('name');

				$address = '';
				$address .= $location->get_field_value('address');
				$address .= ' '.$location->get_field_value('city');
				$address .= ' '.$location->get_field_value('state');
				$address .= ' '.$location->get_field_value('zip');

				$information = '';
				$information = '<strong>' . $name . '</strong><br/>';
				$information .= $address . '<br/>';
				$information .= '<b> Phone: </b>'.$location->get_field_value('phone');
				$information .= '<br/><b> Fax: </b>'.$location->get_field_value('fax');
				$information .= '<br/><b> Hours: </b>'.$location->get_field_value('hours_week');
				$information .= '<br/><b> Weekend Hours: </b>'.$location->get_field_value('hours_weekend');

				$geocode = $map->getGeocode($address);

				$latitude = $geocode['lat'];
				$longitude = $geocode['lon'];
				$psc_addresses[$location->id]['lat'] = $latitude;
				$psc_addresses[$location->id]['lon'] = $longitude;
				$psc_addresses[$location->id]['id'] = $location->id;
				$psc_addresses[$location->id]['address'] = $address;
				$psc_addresses[$location->id]['information'] = $information;
				$psc_addresses[$location->id]['name'] = $name;
			}
			$user_add = $user_address['address'];
			$user_city = $user_address['city'];
			$user_state = $user_address['state'];
			$user_zip = $user_address['zip'];

			$geocoded_user_address = $map->getGeoCode($user_add.' '.$user_city.' '.$user_state.' '.$user_zip);
            $geocoded_user_address_full = $map->geoGetCoordsFull($user_add.' '.$user_city.' '.$user_state.' '.$user_zip);

            $latitude = 33.944702;
            $longitude = -118.297081;

            if (isset($geocoded_user_address_full->results[0]))
            {
                $latitude = $geocoded_user_address_full->results[0]->geometry->location->lat;
                $longitude = $geocoded_user_address_full->results[0]->geometry->location->lng;
            }
            elseif ($geocoded_user_address)
            {
                $latitude = $geocoded_user_address['lat'];
                $longitude = $geocoded_user_address['lon'];
            }

			$valid_addresses = array();
			$good_addresses = 0;
			$message = '';
			foreach ($psc_addresses as $pscadd)
			{
				$latitude2 = Arr::get($pscadd, 'lat');
				$longitude2 = Arr::get($pscadd, 'lon');
				$distance = $map->geoGetDistance($latitude, $longitude, $latitude2, $longitude2);

				if ($distance <= $distance_scope)
				{
					$address = Arr::get($pscadd, 'address');
					$name = Arr::get($pscadd, 'name');
					$information = Arr::get($pscadd, 'information');
					$map->addMarkerByAddress($address,$name,$information);
					$valid_addresses[] = $pscadd;
					$good_addresses ++;
				}
			}
			if ($good_addresses == 0)
			{
				foreach ($psc_addresses as $pscadd)
				{
					$address = Arr::get($pscadd, 'address');
					$name = Arr::get($pscadd, 'name');
					$information = Arr::get($pscadd, 'information');
					$map->addMarkerByAddress($address,$name,$information);
				}
			}
			else
			{
				$view->pscs = $valid_addresses;
			}
			if ($geocoded_user_address)
            {
                $longitude = (float) number_format($longitude, 6);
                $latitude = (float) number_format($latitude, 6);
                $map->setCenterCoords($longitude, $latitude);
            }
			$message = "We Found <span style='color:#9A111F;'>".$good_addresses." PSCs</span> near you.";
		}
		else
		{
			foreach ($psc_locations as $location)
			{
				$name = $location->get_field_value('name');

				$address = '';
				$address .= $location->get_field_value('address');
				$address .= ' '.$location->get_field_value('city');
				$address .= ' '.$location->get_field_value('state');
				$address .= ' '.$location->get_field_value('zip');

				$information = '';
				$information .= '<b> Phone: </b>'.$location->get_field_value('phone');
				$information .= '<br/><b> Fax: </b>'.$location->get_field_value('fax');
				$information .= '<br/><b> Hours: </b>'.$location->get_field_value('hours_week');
				$information .= '<br/><b> Weekend Hours: </b>'.$location->get_field_value('hours_weekend');
				$map->addMarkerByAddress($address,$name,$information);
			}

		}

		$map->_minify_js = isset($_REQUEST["min"])?FALSE:TRUE;
		$map->enableStreetViewControls();
		$map->setMapType('ROADMAP');
		$view->message = $message;
		$view->map = $map;
		$this->template->body = $view;
	}
*/

	public function action_blood_draw()
    {
        $post = $_POST;

        if ($post != NULL)
        {
            $to = array('clientservices@aviir.com' => 'Aviir Services');
            $from = array('noreply@aviir.com' => 'Aviir');

            $email_message = 'First Name: '.Arr::get($post, 'first_name', '').'<br/>';
            $email_message.= 'Last Name: '.Arr::get($post, 'last_name', '').'<br/>';
            $email_message.= 'Phone: '.Arr::get($post, 'phone', '').'<br/>';
            $email_message.= 'Email: '.Arr::get($post, 'email', '').'<br/>';

            $email_message.= '<h4>Address for Service to be Performed</h4>';
            $email_message.= '<hr/>';

            $email_message.= 'Address: '.Arr::get($post, 'address', '').'<br/>';
            $email_message.= 'Address 2: '.Arr::get($post, 'address_2', '').'<br/>';
            $email_message.= 'City: '.Arr::get($post, 'city', '').'<br/>';
            $email_message.= 'State: '.Arr::get($post, 'state', '').'<br/>';
            $email_message.= 'Zip: '.Arr::get($post, 'zip', '').'<br/>';

            $email_message.= '<h4>Insurance Information</h4>';
            $email_message.= '<hr/>';

            $email_message.= 'Insurance Company: '.Arr::get($post, 'insurance_company', '').'<br/>';
            $email_message.= 'I.D. or Policy No.: '.Arr::get($post, 'insurance_policy', '').'<br/>';
            $email_message.= 'Policy Holder First Name: '.Arr::get($post, 'insurance_first_name', '').'<br/>';
            $email_message.= 'Policy Holder Last Name: '.Arr::get($post, 'insurance_last_name', '').'<br/>';
            $email_message.= 'Group No.: '.Arr::get($post, 'insurance_group', '').'<br/>';
            $email_message.= 'Medicare No.: '.Arr::get($post, 'insurance_medicare', '').'<br/>';

            $email_message.= '<h4>Appointment Details</h4>';
            $email_message.= '<hr/>';

            $email_message.= '1st Choice Appointment Date: '.Arr::get($post, 'appointment_date_1', '').'<br/>';
            $email_message.= 'Preferred Time: '.Arr::get($post, 'appointment_time_1', '').'<br/>';
            $email_message.= '2nd Choice Appointment Date: '.Arr::get($post, 'appointment_date_2', '').'<br/>';
            $email_message.= 'Preferred Time: '.Arr::get($post, 'appointment_time_2', '').'<br/>';

            $mailer = Mailer::instance();
            $mailer->to($to);
            $mailer->from($from);
            $mailer->subject('Blood Draw Form Submission');
            $mailer->html($email_message);
            $result = $mailer->send();

            $blood_draw = ORM::factory('Content')->where('node_name', '=', 'patients_blood_draw')->find();

			$to = array(Arr::get($post, 'email', '') => Arr::get($post, 'first_name', '').' '.Arr::get($post, 'last_name', ''));
            $from = array('noreply@aviir.com' => 'Aviir');

			$mailer = Mailer::instance();
            $mailer->to($to);
            $mailer->from($from);
            $mailer->subject($blood_draw->get_field_value('email_subject'));
            $mailer->html($blood_draw->get_field_value('email_body'));
            $result = $mailer->send();

            $view = View::factory('patients/blood_draw_submit');
            $this->template->page_title = 'Thanks';
            $this->template->body = $view;
        }
        else
        {
            $view = View::factory('patients/blood_draw');

            $blood_draw = ORM::factory('Content')->where('node_name', '=', 'patients_blood_draw')->find();
            $view->blood_draw = $blood_draw;

            $this->template->page_title = $blood_draw->get_field_value('title');
            $this->template->body = $view;
        }
    }
} // End Index
