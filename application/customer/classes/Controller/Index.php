<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Index extends Controller_Website {

	public function action_index()
	{
		$item = ORM::factory('Content')->where('node_name', '=', 'homepage_homepage')->find();
		$boxes = ORM::factory('Content')->where('node_name', '=', 'homepage_boxes')->find_all();
		$slider_items = ORM::factory('Content')->where('node_name', '=', 'slider')->find_all();
		$view = View::factory('homepage/index');
		$slider = View::factory('slider');
		$slider->slider_items = $slider_items;
		$view->slider = $slider;
		$view->item = $item;
		$view->boxes = $boxes;
		$this->template->title = 'Home';
		$this->template->body = $view;
	}

	// public function action_switch_db()
	// {
		// $files = ORM::factory('File')->find_all();
		// foreach ($files as $file)
		// {
			// $url = $file->url;
			// $url = str_replace('templestudiosbucket', 'aviir', $url);
			// $file->url = $url;
			// $file->save();
		// }
	// }
} // End Index
