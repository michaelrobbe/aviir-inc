<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Contact extends Controller_Website {

	public function action_index()
	{
		require_once Kohana::find_file('vendor', 'googlemaps/GoogleMap', 'php');
		require_once Kohana::find_file('vendor', 'googlemaps/JSMin', 'php');
		$map = new GoogleMapAPI();

		$corporate = ORM::factory('Content')->where('node_name', '=', 'aviir_corporate')->find();
		$client_services = ORM::factory('Content')->where('node_name', '=', 'aviir_client_services')->find();
		$email = ORM::factory('Content')->where('node_name', '=', 'aviir_email')->find();

		$corporate_address = $corporate->get_field_value('address');
		$map->addMarkerByAddress(strip_tags($corporate_address),"Corporate Office",$corporate_address);
		$map->_minify_js = isset($_REQUEST["min"])?FALSE:TRUE;

		$map->enableInfoWindow();
		$map->enableStreetViewControls();
		$map->setMapType('ROADMAP');
		$map->setZoomLevel(10);

		$view = View::factory('contact/contact');
		$view->corporate = $corporate;
		$view->client_services = $client_services;
		$view->email = $email;
		$view->map = $map;
		$this->template->body = $view;
	}

	public function action_mailing_list()
	{
		$get = $_GET;
		$first_name = $get['mailing_fname'];
		$last_name = $get['mailing_lname'];
		$email = $get['mailing_email'];

		$receiver = ORM::factory('Mailinglist');
		$receiver->first_name = $first_name;
		$receiver->last_name = $last_name;
		$receiver->email = $email;
		$receiver->save();
		Notice::add(Notice::SUCCESS, 'Thank you for signing up.');
		$this->redirect('/index');
	}

	public function action_psc_locations()
	{
		require_once Kohana::find_file('vendor', 'googlemaps/GoogleMap', 'php');
		require_once Kohana::find_file('vendor', 'googlemaps/JSMin', 'php');
		$map = new GoogleMapAPI('map');
        // $map->setAPIKey('AIzaSyBVZAiVCjIhqVkLAChDvU5aCdLg6jgJDec');
        $map->use_suggest = true;
        $map->setZoomLevel(10);

		$message = "Enter Your Address To Find PSCs Near You.";
		$view = View::factory('contact/psc_locations');
		$psc_locations = ORM::factory('Content')->where('node_name', '=', 'locations')->find_all();
		$user_address = $_GET;
		if ($user_address)
		{
			$distance_scope = Arr::get($user_address, 'scope', 50);
			$psc_addresses = array();
			foreach ($psc_locations as $location)
			{
				$name = $location->get_field_value('name');

				$address = '';
				$address .= $location->get_field_value('address');
				$address .= ' '.$location->get_field_value('city');
				$address .= ' '.$location->get_field_value('state');
				$address .= ' '.$location->get_field_value('zip');
                trim($address);

				$information = '';
		        $information = '<strong>' . $name . '</strong><br/>';
				$information .= $address . '<br/>';
				$information .= '<b> Phone: </b>'.$location->get_field_value('phone');
				$information .= '<br/><b> Fax: </b>'.$location->get_field_value('fax');
				$information .= '<br/><b> Hours: </b>'.$location->get_field_value('hours_week');
				$information .= '<br/><b> Weekend Hours: </b>'.$location->get_field_value('hours_weekend');

				$geocode = $map->getGeocode($address);

				$location_latitude = $geocode['lat'];
				$location_longitude = $geocode['lon'];
				$psc_addresses[$location->id]['lat'] = $location_latitude;
				$psc_addresses[$location->id]['lon'] = $location_longitude;
				$psc_addresses[$location->id]['id'] = $location->id;
				$psc_addresses[$location->id]['address'] = $address;
				$psc_addresses[$location->id]['information'] = $information;
				$psc_addresses[$location->id]['name'] = $name;
			}

			$user_add = $user_address['address'];
			$user_city = $user_address['city'];
			$user_state = $user_address['state'];
			$user_zip = $user_address['zip'];

            $geocoded_user_address = $map->getGeoCode($user_add.' '.$user_city.' '.$user_state.' '.$user_zip);
			$geocoded_user_address_full = $map->geoGetCoordsFull($user_add.' '.$user_city.' '.$user_state.' '.$user_zip);

            $latitude = 33.944702;
            $longitude = -118.297081;

            if (isset($geocoded_user_address_full->results[0]))
            {
                $latitude = $geocoded_user_address_full->results[0]->geometry->location->lat;
                $longitude = $geocoded_user_address_full->results[0]->geometry->location->lng;
            }
            elseif ($geocoded_user_address)
            {
                $latitude = $geocoded_user_address['lat'];
                $longitude = $geocoded_user_address['lon'];
            }

			$valid_addresses = array();
			$good_addresses = 0;
			$message = '';

			foreach ($psc_addresses as $pscadd)
			{
				$latitude2 = Arr::get($pscadd, 'lat');
				$longitude2 = Arr::get($pscadd, 'lon');
				$distance = $map->geoGetDistance($latitude, $longitude, $latitude2, $longitude2);

				if ($distance <= $distance_scope)
				{
					$address = Arr::get($pscadd, 'address');
					$name = Arr::get($pscadd, 'name');
					$information = Arr::get($pscadd, 'information');

					if($kyle = $map->addMarkerByAddress($address,$name,$information)){
						print $kyle;
					}
					else {
						print '<p>marker unsuccessful</p>';
					}

// 					if($entry = $map->addMarkerByCoords($latitude2, $longitude2)){
// 						echo $entry . '<br>';
// 					}

					$valid_addresses[] = $pscadd;
					$good_addresses++;

// 					print_r($pscadd);
				}
			}
			if ($good_addresses == 0)
			{
				print 'zero good addresses &&&';
// 				print_r($psc_addresses);

				foreach ($psc_addresses as $pscadd)
				{
					$address = Arr::get($pscadd, 'address');
					$name = Arr::get($pscadd, 'name');
					$information = Arr::get($pscadd, 'information');
					$map->addMarkerByAddress($address,$name,$information);
				}
			}
			else
			{
				print 'Display addresses &&&';

				$view->pscs = $valid_addresses;
			}
            if ($geocoded_user_address)
            {
            	print 'User Addresses Geocoded &&&';

                $longitude = (float) number_format($longitude, 6);
                $latitude = (float) number_format($latitude, 6);
                $map->setCenterCoords($longitude, $latitude);
            }
			$message = "We Found <span style='color:#9A111F;'>".$good_addresses." PSCs</span> near you.";
		}
		else
		{
			foreach ($psc_locations as $location)
			{
				$name = $location->get_field_value('name');

				$address = '';
				$address .= $location->get_field_value('address');
				$address .= ' '.$location->get_field_value('city');
				$address .= ' '.$location->get_field_value('state');
				$address .= ' '.$location->get_field_value('zip');

				$information = '';
				$information .= '<b> Phone: </b>'.$location->get_field_value('phone');
				$information .= '<br/><b> Fax: </b>'.$location->get_field_value('fax');
				$information .= '<br/><b> Hours: </b>'.$location->get_field_value('hours_week');
				$information .= '<br/><b> Weekend Hours: </b>'.$location->get_field_value('hours_weekend');
				$map->addMarkerByAddress($address,$name,$information);
            }
		}

		$map->_minify_js = isset($_REQUEST["min"])?FALSE:TRUE;
		$map->enableStreetViewControls();
		$map->setMapType('ROADMAP');
		$view->message = $message;
		$view->map = $map;
		$this->template->body = $view;
	}

	public function action_email_us()
	{
		$get = $_GET;

		$name = Arr::get($get, 'name');
		$email = Arr::get($get, 'email', false);
		$phone = Arr::get($get, 'phone');
		$message = Arr::get($get, 'message');
		$to = array('sales@aviir.com' => 'Aviir Contact Form', 'marketing@aviir.com' => 'Aviir Contact Form');
		$from = array('noreply@aviir.com' => $name);

		$email_message = 'Name: '.$name.'<br/>';
		$email_message .= 'Phone: '.$phone.'<br/>';
		$email_message .= 'Email Address: '.$email.'<br/>';
		$email_message .= '<p>'.$message.'</p>';

		if ($email)
		{
			$mailer = Mailer::instance();
		    $mailer->to($to);
		    $mailer->from($from);
		    $mailer->subject('Contact Form Submission');
		    $mailer->html($email_message);
		    $result = $mailer->send();
		}
		die();
	}
} // End Index

