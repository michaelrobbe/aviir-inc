<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Adl extends Controller_Website {

	public function before()
	{
		parent::before();

		$request = Request::initial();
        $requsted_controller = $request->controller();
        $requsted_action = $request->action();

		$page_main_title = strtoupper($requsted_controller);
        if ($requsted_action != 'index')
        {
            $page_main_title .= ' - '.ucwords($requsted_action);
        }

        $this->page_title = $this->site_name.' - '.$page_main_title;
		$this->template->title = $this->page_title;
	}

    public function action_static()
    {
        $page = $this->request->param('page');

        if ( ! method_exists($this, 'action_'.$page))
        {
            $title = Format::un_url_title($page);
            $content_field = ORM::factory('Contentfield')
                ->where('field_name', '=', 'title')
                ->where('value', '=', $title)
                ->where('node_name', '=', 'ADL_pages')
                ->join('contents')
                ->on('contentfield.content_id', '=', 'contents.id')
                ->find();

            if ($content_field->id > 0)
            {
                if (strstr($content_field->content->node_name, 'page') !== false)
                {
                    $_GET['page'] = $content_field->content->id;
                    $this->action_page();
                }
            }
        }
        else
        {
        	$this->template->title = $this->site_name.' - ADL - '.ucwords(str_replace('_', ' ', $page));
            $page_action = 'action_'.$page;
            $this->$page_action();
        }
    }

	public function action_psc_locations()
	{
		require_once Kohana::find_file('vendor', 'googlemaps/GoogleMap', 'php');
		require_once Kohana::find_file('vendor', 'googlemaps/JSMin', 'php');
		$map = new GoogleMapAPI();
		$message = "Enter Your Address To Find PSCs Near You.";
		$view = View::factory('adl/psc_locations');
		$psc_locations = ORM::factory('Content')->where('node_name', '=', 'locations')->find_all();
		$user_address = $_GET;
		if ($user_address)
		{
			$distance_scope = Arr::get($user_address, 'scope', 50);
			$psc_addresses = array();
			foreach ($psc_locations as $location)
			{
				$name = $location->get_field_value('name');

				$address = '';
				$address .= $location->get_field_value('address');
				$address .= ' '.$location->get_field_value('city');
				$address .= ' '.$location->get_field_value('state');
				$address .= ' '.$location->get_field_value('zip');

				$information = '';
				$information = '<strong>' . $name . '</strong><br/>';
				$information .= $address . '<br/>';
				$information .= '<b> Phone: </b>'.$location->get_field_value('phone');
				$information .= '<br/><b> Fax: </b>'.$location->get_field_value('fax');
				$information .= '<br/><b> Hours: </b>'.$location->get_field_value('hours_week');
				$information .= '<br/><b> Weekend Hours: </b>'.$location->get_field_value('hours_weekend');

				$geocode = $map->getGeocode($address);

				$latitude = $geocode['lat'];
				$longitude = $geocode['lon'];
				$psc_addresses[$location->id]['lat'] = $latitude;
				$psc_addresses[$location->id]['lon'] = $longitude;
				$psc_addresses[$location->id]['id'] = $location->id;
				$psc_addresses[$location->id]['address'] = $address;
				$psc_addresses[$location->id]['information'] = $information;
				$psc_addresses[$location->id]['name'] = $name;
			}
			$user_add = $user_address['address'];
			$user_city = $user_address['city'];
			$user_state = $user_address['state'];
			$user_zip = $user_address['zip'];

			$geocoded_user_address = $map->getGeoCode($user_add.' '.$user_city.' '.$user_state.' '.$user_zip);
            $geocoded_user_address_full = $map->geoGetCoordsFull($user_add.' '.$user_city.' '.$user_state.' '.$user_zip);

            $latitude = 33.944702;
            $longitude = -118.297081;

            if (isset($geocoded_user_address_full->results[0]))
            {
                $latitude = $geocoded_user_address_full->results[0]->geometry->location->lat;
                $longitude = $geocoded_user_address_full->results[0]->geometry->location->lng;
            }
            elseif ($geocoded_user_address)
            {
                $latitude = $geocoded_user_address['lat'];
                $longitude = $geocoded_user_address['lon'];
            }

			$valid_addresses = array();
			$good_addresses = 0;
			$message = '';
			foreach ($psc_addresses as $pscadd)
			{
				$latitude2 = Arr::get($pscadd, 'lat');
				$longitude2 = Arr::get($pscadd, 'lon');
				$distance = $map->geoGetDistance($latitude, $longitude, $latitude2, $longitude2);

				if ($distance <= $distance_scope)
				{
					$address = Arr::get($pscadd, 'address');
					$name = Arr::get($pscadd, 'name');
					$information = Arr::get($pscadd, 'information');
					$map->addMarkerByAddress($address,$name,$information);
					$valid_addresses[] = $pscadd;
					$good_addresses ++;
				}
			}
			if ($good_addresses == 0)
			{
				foreach ($psc_addresses as $pscadd)
				{
					$address = Arr::get($pscadd, 'address');
					$name = Arr::get($pscadd, 'name');
					$information = Arr::get($pscadd, 'information');
					$map->addMarkerByAddress($address,$name,$information);
				}
			}
			else
			{
				$view->pscs = $valid_addresses;
			}
            if ($geocoded_user_address)
            {
                $longitude = (float) number_format($longitude, 6);
                $latitude = (float) number_format($latitude, 6);
                $map->setCenterCoords($longitude, $latitude);
            }
			$message = "We Found <span style='color:#CEB98E;'>".$good_addresses." PSCs</span> near you.";
		}
		else
		{
			foreach ($psc_locations as $location)
			{
				$name = $location->get_field_value('name');

				$address = '';
				$address .= $location->get_field_value('address');
				$address .= ' '.$location->get_field_value('city');
				$address .= ' '.$location->get_field_value('state');
				$address .= ' '.$location->get_field_value('zip');

				$information = '';
				$information .= '<b> Phone: </b>'.$location->get_field_value('phone');
				$information .= '<br/><b> Fax: </b>'.$location->get_field_value('fax');
				$information .= '<br/><b> Hours: </b>'.$location->get_field_value('hours_week');
				$information .= '<br/><b> Weekend Hours: </b>'.$location->get_field_value('hours_weekend');
				$map->addMarkerByAddress($address,$name,$information);
			}

		}

		$map->_minify_js = isset($_REQUEST["min"])?FALSE:TRUE;
		$map->setMapType('ROADMAP');
		$view->message = $message;
		$view->map = $map;

		$this->template->body = $view;
	}

	public function action_contact()
	{
		require_once Kohana::find_file('vendor', 'googlemaps/GoogleMap', 'php');
		require_once Kohana::find_file('vendor', 'googlemaps/JSMin', 'php');
		$map = new GoogleMapAPI();

		$corporate = ORM::factory('Content')->where('node_name', '=', 'aviir_corporate')->find();
		$research = ORM::factory('Content')->where('node_name', '=', 'aviir_researchdev')->find();
		$email = ORM::factory('Content')->where('node_name', '=', 'aviir_email')->find();

		$corporate_address = $corporate->get_field_value('address');

		$map->addMarkerByAddress(strip_tags($corporate_address),"Corporate Office",$corporate_address);
		$map->_minify_js = isset($_REQUEST["min"])?FALSE:TRUE;
		$map->enableStreetViewControls();
		$map->setMapType('ROADMAP');
		$map->setZoomLevel(12);

		$view = View::factory('adl/contact');
		$view->corporate = $corporate;
		$view->research = $research;
		$view->email = $email;
		$view->map = $map;
		$this->template->body = $view;
	}

	public function action_page()
	{
		$page = $this->request->param('id', Arr::get($_GET, 'page', 1));
		$item = ORM::factory('Content', $page);

		$title = $item->get_field_value('title');
		$this->template->title = $this->site_name.' - ADL - '.$title;

		$view = View::factory('adl/page');
		$view->item = $item;
		$this->template->page_title = $item->get_field_value('title');
		$this->template->body = $view;
	}

	public function action_search()
	{
		$by_letter = Arr::get($_GET, 'by_letter');
		$filter_by = Arr::get($_GET, 'filter_by');
		$query =  Arr::get($_GET, 'q');
		$option =  Arr::get($_GET, 'option');
		$selected_option = 'testname';
		$items = DB::select('contents.id')
						->from('contents')
						->where('node_name', 'LIKE', 'ADL_tests')
						->execute();
		$searched_for = "All";

		if ( ! is_null($filter_by))
		{
			$items = DB::select('contents.id')
						->from('contentfields')
						->join('contents', 'left')
						->on('contentfields.content_id', '=', 'contents.id')
						->where('value', '=', $filter_by)
						->execute();
			$searched_for = $filter_by;
		}


		if ( ! is_null($by_letter))
		{
			$items = DB::select('contents.id')
						->from('contentfields')
						->join('contents', 'left')
						->on('contentfields.content_id', '=', 'contents.id')
						->where('field_name', '=', 'testname')
						->where('value', 'LIKE', $by_letter.'%')
						->order_by('value', 'ASC')
						->execute();
			$searched_for = $by_letter;
		}

		if ( ! is_null($query))
		{
			$items = DB::select('contents.id')
						->from('contentfields')
						->join('contents', 'left')
						->on('contentfields.content_id', '=', 'contents.id')
						->where('field_name', '=', $option)
						->where('value', 'LIKE', '%'.$query.'%')
						->order_by('value', 'ASC')
						->execute();
			$searched_for = $query;
			$selected_option = $option;
		}

		$categories =  DB::select('value')
						->distinct('value')
						->from('contentfields')
						->where('field_name', '=', 'category')
						->where('value', '!=', '')
						->order_by('value', 'ASC')
						->execute();

		$options = array('testname' => 'TEST NAME', 'cptcode' => 'CPT CODE', 'testcode' => 'TEST CODE');
		$view = View::factory('adl/search');
		$view->categories = $categories;
		$view->options = $options;
		$view->items = $items;
		$view->selected_option = $selected_option;
		$view->searched_for = $searched_for;
		$this->template->page_title = "TEST MENU";
		$this->template->body = $view;
	}

	public function action_test()
	{
		$test_id = $this->request->param('id');
		$view = View::factory('adl/test');

		$items = DB::select('contents.id')
				->from('contents')
// 				->where('node_name', 'LIKE', 'ADL_tests')
				->where('id', '=', $test_id)
				->execute();

		$view->items = $items;
		$this->template->page_title = "INDIVIDUAL TEST";
		$this->template->body = $view;
	}

	public function action_index()
	{
		$boxes = ORM::factory('Content')->where('node_name', '=', 'ADL_boxes')->find_all();
		$slider_items = ORM::factory('Content')->where('node_name', '=', 'slider')->find_all();
		$pulse_items = ORM::factory('Content')->where('node_name', '=', 'pulse')->order_by('id', 'DESC')->find_all();
		$view = View::factory('adl/index');
		$slider = View::factory('slider');
		$slider->slider_items = $slider_items;
		$view->slider = $slider;
		$view->boxes = $boxes;
		$view->pulse_items = $pulse_items;
		$this->template->title = 'ADL';
		$this->template->body = $view;
	}

    public function action_blood_draw()
    {
        $post = $_POST;

        if ($post != NULL)
        {
            $to = array('clientservices@aviir.com' => 'Aviir Services');
            $from = array('noreply@aviir.com' => 'Aviir');

            $email_message = 'First Name: '.Arr::get($post, 'first_name', '').'<br/>';
            $email_message.= 'Last Name: '.Arr::get($post, 'last_name', '').'<br/>';
            $email_message.= 'Phone: '.Arr::get($post, 'phone', '').'<br/>';
            $email_message.= 'Email: '.Arr::get($post, 'email', '').'<br/>';

            $email_message.= '<h4>Address for Service to be Performed</h4>';
            $email_message.= '<hr/>';

            $email_message.= 'Address: '.Arr::get($post, 'address', '').'<br/>';
            $email_message.= 'Address 2: '.Arr::get($post, 'address_2', '').'<br/>';
            $email_message.= 'City: '.Arr::get($post, 'city', '').'<br/>';
            $email_message.= 'State: '.Arr::get($post, 'state', '').'<br/>';
            $email_message.= 'Zip: '.Arr::get($post, 'zip', '').'<br/>';

            $email_message.= '<h4>Insurance Information</h4>';
            $email_message.= '<hr/>';

            $email_message.= 'Insurance Company: '.Arr::get($post, 'insurance_company', '').'<br/>';
            $email_message.= 'I.D. or Policy No.: '.Arr::get($post, 'insurance_policy', '').'<br/>';
            $email_message.= 'Policy Holder First Name: '.Arr::get($post, 'insurance_first_name', '').'<br/>';
            $email_message.= 'Policy Holder Last Name: '.Arr::get($post, 'insurance_last_name', '').'<br/>';
            $email_message.= 'Group No.: '.Arr::get($post, 'insurance_group', '').'<br/>';
            $email_message.= 'Medicare No.: '.Arr::get($post, 'insurance_medicare', '').'<br/>';

            $email_message.= '<h4>Appointment Details</h4>';
            $email_message.= '<hr/>';

            $email_message.= '1st Choice Appointment Date: '.Arr::get($post, 'appointment_date_1', '').'<br/>';
            $email_message.= 'Preferred Time: '.Arr::get($post, 'appointment_time_1', '').'<br/>';
            $email_message.= '2nd Choice Appointment Date: '.Arr::get($post, 'appointment_date_2', '').'<br/>';
            $email_message.= 'Preferred Time: '.Arr::get($post, 'appointment_time_2', '').'<br/>';

            $mailer = Mailer::instance();
            $mailer->to($to);
            $mailer->from($from);
            $mailer->subject('ADL Blood Draw Form Submission');
            $mailer->html($email_message);
            $result = $mailer->send();

            $blood_draw = ORM::factory('Content')->where('node_name', '=', 'ADL_blood_draw')->find();

			$to = array(Arr::get($post, 'email', '') => Arr::get($post, 'first_name', '').' '.Arr::get($post, 'last_name', ''));
            $from = array('noreply@aviir.com' => 'Aviir');

			$mailer = Mailer::instance();
            $mailer->to($to);
            $mailer->from($from);
            $mailer->subject($blood_draw->get_field_value('email_subject'));
            $mailer->html($blood_draw->get_field_value('email_body'));
            $result = $mailer->send();

            $view = View::factory('adl/blood_draw_submit');
            $this->template->header->title = 'Thanks';
            $this->template->body = $view;
        }
        else
        {
            $view = View::factory('adl/blood_draw');

            $blood_draw = ORM::factory('Content')->where('node_name', '=', 'ADL_blood_draw')->find();
            $view->blood_draw = $blood_draw;

            $this->template->page_title = $blood_draw->get_field_value('title');
            $this->template->body = $view;
        }
    }
} // End Index

