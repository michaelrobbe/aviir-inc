<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Legal extends Controller_Website {
    
    public function action_privacy_policy()
    {
        $privacy = ORM::factory('Content')->where('node_name', '=', 'privacy')->find();
        $view = View::factory('legal/privacy');
        $view->privacy = $privacy;
        $this->template->header->title = 'Privacy Policy';
        $this->template->body = $view;
    }
    
    public function action_terms()
    {
        $terms = ORM::factory('Content')->where('node_name', '=', 'terms')->find();
        $view = View::factory('legal/terms');
        $view->terms = $terms;
        $this->template->header->title = 'Terms and Conditions';
        $this->template->body = $view;
    }
}
?>