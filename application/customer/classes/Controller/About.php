<?php defined('SYSPATH') or die('No direct script access.');

class Controller_About extends Controller_Website {
	
	public function action_index()
	{
		$about = ORM::factory('Content')->where('node_name', '=', 'company_about')->find_all();
		$view = View::factory('about/about');
		$slider_items = ORM::factory('Content')->where('node_name', '=', 'slider')->find_all();
		$slider = View::factory('slider');
		$slider->slider_items = $slider_items;
		$view->slider = $slider;
		$view->items = $about;
		$this->template->body = $view;
	}
	
	public function action_executives()
	{
		$execs = ORM::factory('Content')->where('node_name', '=', 'company_executives')->find_all();
		$view = View::factory('about/executives');
		$view->items = $execs;
		$this->template->page_title = 'Executives';
		$this->template->body = $view;
	}
	
	public function action_investors()
	{
		$investors = ORM::factory('Content')->where('node_name', '=', 'company_investors')->find_all();
		$view = View::factory('about/investors');
		$view->items = $investors;
		$this->template->page_title = 'Investors';
		$this->template->body = $view;
	}

	public function action_page()
	{
		$page = $this->request->param('id', 1);
		$item = ORM::factory('Content', $page);
		$view = View::factory('about/page');
		$view->item = $item;
		$this->template->page_title = $item->get_field_value('title');
		$this->template->body = $view;
	}
	
	public function action_board_members()
	{
		$execs = ORM::factory('Content')->where('node_name', '=', 'company_boardmembers')->find_all();
		$view = View::factory('about/board_members');
		$view->items = $execs;
		$this->template->page_title = 'Board Members';
		$this->template->body = $view;
	}

} // End Index
