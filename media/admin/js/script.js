/* Author:

*/

$(".delete").click(function(e) {
	e.preventDefault();
	var target_url = $(this).attr("href");
	
	$('.modal_delete_yes_button').attr('href', target_url);
	$('#delete_dialog').modal({
		keyboard: false
	});
});

$('.vimeo_preview').live('click', function(){
		var source = $(this).attr('video_id');
		$('#vimeo_frame').attr('src', source);
		$('#play_video_box').modal('show');
	});

$('.modal_hide').click(function(e) {
	e.preventDefault();
	$('.dialog').modal('hide');
})

$(".datepicker").datepicker();

$(".category_form").validate({
	rules: {
		'form[name]':{
			required:true
		}	
	},
	messages: {
		'form[name]': {
			required:'Please enter a category name'
		}
	} 
});

$("a.trigger").click(function (event) {
    event.preventDefault();
	$(this).next().animate({
		 height: 'toggle', opacity: 'toggle'
	}, "slow");
	$(this).toggleClass("opened");
});

$(function(){
    $(".tag_field").tagit();
    $('.category_select').multiSelect();
    
    $( ".selectable" ).selectable();
    
    $('.video_search').click(function(event) {
        event.preventDefault();
        
        $('#videos').html('<img src="/media/admin/img/ajax-loader.gif" />');
        
        var search_value = $('input[name="search_video_title"]').val();
        var videos_selected = $('#videos_selected_field').val();
        
        $.ajax({
            url: '/asset/search',
            data: 'type=vimeo&search_term='+search_value+'&selected_assets='+videos_selected,
            success: function(data) {
                $('#videos').html(data);
            },
            dataType: 'html'
        });
    });
    
   
    
    if ($('#videos_selected').length) {
        $.ajax({
            url: '/asset/get_assets',
            data: 'asset_ids='+$('#videos_selected_field').val()+'&campaign_id='+$('#campaign_id').val(),
            success: function(data) {
                $('#videos_selected').append(data);
            },
            dataType: 'html'
        });
    }
    


   $(".images_search_input").bind("keydown", function(event) {
      
      var keycode = (event.keyCode ? event.keyCode : (event.which ? event.which : event.charCode));
      if (keycode == 13) { // when the enter key
          
         $('button.image_search').click();
         return false;
      } else  {
         return true;
      }
   });


 

    
    $('.image_search').click(function(event) {
        event.preventDefault();
        
        $('#images').html('<img src="/media/admin/img/ajax-loader.gif" />');
        
        var search_value = $('input[name="search_image_title"]').val();
        var images_selected = $('#images_selected_field').val();
        
        $.ajax({
            url: '/asset/search',
            data: 'type=image&search_term='+search_value+'&selected_assets='+images_selected,
            success: function(data) {
                $('#images').html(data);
            },
            dataType: 'html'
        });
    });
    
    $('.add_image').live("click", function(event){
        event.preventDefault();
        
        var asset_dom = $(this).parents('.asset');
        var asset_id = asset_dom.attr('asset_id');
        
        var image_url = $(this).parents('.asset').children('.asset_image').children('img').attr('src');
        image_url = image_url.replace('tiny','medium');
        
        $('#images_selected_field').val($('#images_selected_field').val()+asset_id+',');
        
        asset_dom.children('.asset_primary').removeClass('hidden');
        
        var html = '<li widget_type="image" class="work_page" work_page="work_page" element_id="'+asset_id+'"><div class="image_container" style="background-image:url('+image_url+')"></div></li>';
        
        gridster.add_widget(html, 1, 1);
        
        asset_dom.fadeOut('fast', function() {
            asset_dom.remove();
        });
    });
    
    // about -> placement input digit validation
    
    var placement_inputs = [];
 	
 	$.validator.addClassRules("placement", {
			required: true,
      		digits: true
	});
	
	var placement_last = 0;
	
	$('.placement').each(function (index) {
		current_value = $(this).val();
		
		if ( current_value > placement_last ) {
			placement_last = current_value;
			
		}
		
		$('.placement').last().attr('placeholder', (parseInt(placement_last) + 1));
		 
	})
	
	$('div.well form').each(function(index) {
		$(this).validate();
	});
  
    
    $('.remove_image').live('click', function(event){
        event.preventDefault();
        
        var asset_dom = $(this).parents('.asset');
        var asset_id = asset_dom.attr('asset_id');
        
        $('#images_selected_field').val($('#images_selected_field').val().replace(asset_id+',', ''));
        
        asset_dom.fadeOut('fast', function() {
            asset_dom.remove();
        });
    });
    
    if ($('#images_selected').length) {
        $.ajax({
            url: '/asset/get_assets',
            data: 'asset_ids='+$('#images_selected_field').val()+'&campaign_id='+$('#campaign_id').val(),
            success: function(data) {
                $('#images_selected').append(data);
            },
            dataType: 'html'
        });
    }
});