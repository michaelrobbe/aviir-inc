var files_to_upload = 0;
var uploads_started = false;

$(document).ready(function() {
      $(':checkbox').iphoneStyle({
          checkedLabel: 'Yes',
          uncheckedLabel: 'No'
      });
});
$(function () {
    
    $('.remove_image').click(function(event) {
        event.preventDefault();
        var image_key = $(this).attr('image_key');
        
        $('#'+image_key+'_image_container').fadeOut('fast');
        $('input[name="'+image_key+'"]').val('');
    });
    
    $('#content_form').submit(function(event) {
    	var thisform = $('#content_form');
    	var save_button = $(this).find($('input[type="submit"]'));
    	save_button.hide();
    	
    	$('#loader').show();
    	if (files_to_upload > 0)
        {
            $('<p/>').text('Upload started.').appendTo($('#status_report'));
            event.preventDefault();
            if (uploads_started != true)
            {
                start_uploads();
                uploads_started = true;
            }
        }
        else
        {
            $('<p/>').text('Uploads complete.').appendTo($('#status_report'));
            return true;
        }
    });
    
    function start_uploads() {
        $('.fileupload').each(function() {
            target = $(this).attr('targetinput');
            
            if (eval(target+'_data')) {
                eval(target+'_data').submit();
            }
        });
    }
});