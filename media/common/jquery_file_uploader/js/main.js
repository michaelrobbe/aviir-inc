/*
 * jQuery File Upload Plugin JS Example 6.7
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

/*jslint nomen: true, unparam: true, regexp: true */
/*global $, window, document */

$(function () {
    'use strict';

    // Initialize the jQuery File Upload widget:
    $('#fileupload').fileupload();

    // Enable iframe cross-domain access via redirect option:
    $('#fileupload').fileupload(
        'option',
        'redirect',
        window.location.href.replace(
            /\/[^\/]*$/,
            '/cors/result.html?%s'
        )
    );

    $('#fileupload').fileupload('option', {
        url: '/asset/handle_upload',
        sequentialUploads: true,
        limitConcurrentUploads: 1,
        acceptFileTypes: /(.|\/)(gif|jpe?g|png|mpe?g|wmv|mp4|avi|mov|pdf)$/i,
        process: [
            {
                action: 'load',
            },
            {
                action: 'save'
            }
        ]
    })
    .bind('fileuploadprogress', function (e, data) {
        // Log the current bitrate for this upload:
    })
    .bind('fileuploadcompleted', function (e, data) {
        $.ajax({
            url: '/asset/upload_complete',
            data: { url: data.result[0]['url'], type: data.result[0]['type']}
        });
    });
    // Upload server status check for browsers with CORS support:
    if ($.support.cors) {
        $.ajax({
            url: '/asset/handle_upload',
            type: 'HEAD'
        }).fail(function () {
            $('<span class="alert alert-error"/>')
                .text('Upload server currently unavailable - ' +
                        new Date())
                .appendTo('#fileupload');
        });
    }
});
