/*
 * jQuery File Upload Plugin JS Example 6.7
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

/*jslint nomen: true, unparam: true, regexp: true */
/*global $, window, document */

$(function () {
    'use strict';

    // Initialize the jQuery File Upload widget:
    $('#fileupload').fileupload();

    // Enable iframe cross-domain access via redirect option:
    $('#fileupload').fileupload(
        'option',
        'redirect',
        window.location.href.replace(
            /\/[^\/]*$/,
            '/cors/result.html?%s'
        )
    );

    $('#fileupload').fileupload('option', {
        url: '/profile/handle_upload',
        maxFileSize: 100000000,
        sequentialUploads: true,
        limitConcurrentUploads: 1,
        process: [
            {
                action: 'load',
                fileTypes: /^(image|video)\/(gif|jpeg|png|wmv|mp4|avi|mov)$/,
                maxFileSize: 100000000
            },
            {
                action: 'save'
            }
        ]
    })
    .bind('fileuploadcompleted', function (e, data) {
        $.ajax({
            url: '/profile/upload_complete',
            data: { url: data.result[0]['url'], type: data.result[0]['type']}
        });
    });
    // Upload server status check for browsers with CORS support:
    if ($.support.cors) {
        $.ajax({
            url: '/profile/handle_upload',
            type: 'HEAD'
        }).fail(function () {
            $('<span class="alert alert-error"/>')
                .text('Upload server currently unavailable - ' +
                        new Date())
                .appendTo('#fileupload');
        });
    }
});
