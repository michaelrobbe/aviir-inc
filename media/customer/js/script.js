window.onhashchange = hashChangeAction;
var n;

function messageBox(error) {
		  jError(error
				  ,
			{
			  autoHide : true, // added in v2.0
			  clickOverlay : false, // added in v2.0
			  MinWidth : 250,
			  TimeShown : 3000,
			  ShowTimeEffect : 200,
			  HideTimeEffect : 200,
			  LongTrip :20,
			  HorizontalPosition : 'center',
			  VerticalPosition : 'center',
			  ShowOverlay : true,
	 		  ColorOverlay : '#000',
			  OpacityOverlay : 0.3
			});
}


function doSubmit(thisform) {

	var error = '';
	var field; 
	
	$('input').parents('tr').find('.req').each(function() {
		field = $(this).parent('td').parents('tr').find('input').val();
		
		if( field == '' || field == null ){
			var field_text = $(this).parent('td'); 
			field_text.css('color', 'red');
			field_text.parent('td').css('font-weight', 'bold');
			
			var words = $.trim(field_text.text().replace("*", ""));
			error = error + '<p>Please fill out ' + words + '</p>'; 
		}
	});

	var dollars = $('[name="amount"]').val();
	if( isNaN(dollars) || dollars == "" || dollars == null ) {
		error = error + "<p>Invalid dollar amount</p> ";		
	}
	else {
		$('[name="transaction_amount"]').val(dollars);
	}
	
	if(error == '' || error == null) {
		var refcontent = $('[name="echo_first_name"]').val() + "," + $('[name="echo_last_name"]').val() + "," + $('[name="echo_patient_phone"]').val() + "," + $('[name="echo_patient_email"]').val();
		$('[name="client_reference_number"]').val(refcontent);
		return true;
	} else {
		jError(error);
		return false;
	}
	
	return false;
}

$(document).ready(function() {
	//On checkbox check, it copies the information from the patient fields into the cardholder fields.
	$('input[name="same_billing"]').change(function() {
		var fn = $('input[name="echo_first_name"]').val();
		var ln = $('input[name="echo_last_name"]').val();
		var pp = $('input[name="echo_patient_phone"]').val();
		var pe = $('input[name="echo_patient_email"]').val();
		
		$('input[name="cardholder_first_name"]').val(fn);
		$('input[name="cardholder_last_name"]').val(ln);
		$('input[name="cardholder_phone"]').val(pp);
		$('input[name="customer_email"]').val(pe);
	})
});

/**
 * For browsers that don't support placeholders adds and removes the placeholder as value, for display
 * This snippet adds a .placeholder class to the input fields.
 */
$(document).ready(function() {
	$('[placeholder]').focus(function() {
		var input = $(this);
		if (input.val() == input.attr('placeholder')) {
			input.val('');
			input.removeClass('placeholder');
		}
	}).blur(function() {
		var input = $(this);
		if (input.val() == '' || input.val() == input.attr('placeholder')) {
			input.addClass('placeholder');
			input.val(input.attr('placeholder'));
		}
	}).blur();

	$('[placeholder]').parents('form').submit(function() {
		$(this).find('[placeholder]').each(function() {
			var input = $(this);
			if (input.val() == input.attr('placeholder')) {
				input.val('');
			}
		})
	});

});


function roundNumber(num, dec) {
	var result = Math.round(num*Math.pow(10,dec))/Math.pow(10,dec);
	return result;
}

var i = 1;
jQuery(document).ready(
	function() {
		
			$('table tr + tr').each(function() {
				h = $(this).innerHeight();
				$(this).css('height', h+'px');
				if( $("body").is("[class*='faqs'], [class*='about']") ){
					$(this).hide();
				}
			});
			
			$('td h2').each(function(index) {
				i = (index + 1);
				$(this).addClass('accordion-link-' + i);
				if( $("body").is("[class*='faqs']") ){
					$(this).prepend( '<span class="plus"></span> Q'+ i +': ' );
				} else {
					$(this).prepend( '<span class="plus"></span>' );
				}
			});
			
			$("td h2").click(function() {
				$(this).toggleClass('expand');
				$(this).children("span").toggleClass('plus');
				$(this).children("span").toggleClass('minus expand');
				
				$(this).parents("table").find("tr + tr").slideToggle();
			});
});

jQuery(document).ready(function($) {
		
		  $('.home #full-width-slider, .adl #full-width-slider').royalSlider({
			    arrowsNav: true,
			    loop: true,
			    keyboardNavEnabled: true,
			    controlsInside: false,
			    imageScaleMode: 'none',
			    arrowsNavAutoHide: false,
			    autoScaleSlider: true, 
			    autoScaleSliderWidth: 960,     
			    autoScaleSliderHeight: 374,
			    controlNavigation: 'bullets',
//			    controlNavigation: 'thumbnails',
			    	
			    thumbsFitInViewport: false,
			    navigateByClick: true,
			    startSlideId: 0,
			    autoPlay: {
			    	enabled: true,
				    pauseOnHover: true,
				    delay: 4000
				    },
			    addActiveClass: true,
			    transitionType:'move',
//			    globalCaption: true,
//			    deeplinking: {
//			      enabled: true,
//			      change: true,
//			      prefix: 'tab-'
//			    },
			    /* size of all images http://help.dimsemenov.com/kb/royalslider-jquery-plugin-faq/adding-width-and-height-properties-to-images */
			    imgWidth: 1253,
			    imgHeight: 506
			  });
	
		  $('.professionals #full-width-slider, .patients #full-width-slider').royalSlider({
			    arrowsNav: false,
			    loop: true,
//			    keyboardNavEnabled: true,
			    controlsInside: false,
			    imageScaleMode: 'none',
			    arrowsNavAutoHide: true,
			    autoScaleSlider: true, 
			    autoScaleSliderWidth: 960,     
			    autoScaleSliderHeight: 374,
//			    controlNavigation: 'bullets',
			    controlNavigation: 'thumbnails',
			    	
			    thumbsFitInViewport: false,
			    navigateByClick: true,
			    startSlideId: 0,
			    autoPlay: {
			    	enabled: true,
				    pauseOnHover: true,
				    delay: 4000
				    },
			    addActiveClass: true,
			    transitionType:'move',
//			    globalCaption: true,
			    deeplinking: {
			      enabled: true,
			      change: true,
			      prefix: 'tab-',
			    },
			    /* size of all images http://help.dimsemenov.com/kb/royalslider-jquery-plugin-faq/adding-width-and-height-properties-to-images */
			    imgWidth: 1253,
			    imgHeight: 506
			  });
		  
		  $('.blood_draw .royalSlider').royalSlider({
			    arrowsNav: true,
			    loop: false,
//			    keyboardNavEnabled: true,
			    controlsInside: true,
			    imageScaleMode: 'none',
			    arrowsNavAutoHide: false,
			    autoScaleSlider: true, 
			    autoScaleSliderWidth: 470,     
			    autoScaleSliderHeight: 550,
//			    controlNavigation: 'bullets',
			    controlNavigation: 'thumbnails',
			    thumbsFitInViewport: false,
			    navigateByClick: false,
			    startSlideId: 0,
			    sliderDrag: false,
			    sliderTouch: false,
			    autoPlay: {
			    	enabled: false,
				    },
			    addActiveClass: true,
			    transitionType:'move',
//			    globalCaption: true,
			    deeplinking: {
			      enabled: true,
			      change: true,
			      prefix: 'tab-',
			    },
			    /* size of all images http://help.dimsemenov.com/kb/royalslider-jquery-plugin-faq/adding-width-and-height-properties-to-images */
//			    imgWidth: 400,
//			    imgHeight: 380
			  });
		  
		  if($('div').is('[class*=royalSlider]')) {			  
			  var slider = $(".royalSlider").data('royalSlider');
			  n=1;
			  $('a#tab-link-'+n).addClass('at');
			  $('.tab_caret-'+n).removeClass('hidden');
			  
			  slider.ev.on('rsAfterSlideChange', function(){
				  n = slider.currSlideId+1;
				  $('.at').removeClass('at');
				  $('.tab_caret').addClass('hidden');
				  
				  $('a#tab-link-'+n).addClass('at');
				  $('.tab_caret-'+n).removeClass('hidden');
			  });
		  }
		  
		});

	/*end Slideshow Script */


jQuery(document).ready(function() {
		var slider = $(".royalSlider").data('royalSlider');
		var last;
		
		$('div[class|="tab-wrapper"]').hide();
		$('.blood_draw [class|="tab-wrapper"]').show();
		
		$('.tab_caret').not(':first').addClass('hidden');
		
		$('a[id|="tab-link"]').click(function() {
			n = $('.tab-link').index(this)+1;
			$('.home-content').hide();
			if($('body').is(':not(.blood_draw)')){
				$('div[class|="tab-wrapper"]').hide();
			}
			$('.at').removeClass('at');
			$('.tab_caret').addClass('hidden');
			$('.page-tabs .pro_page_box').animate({height: '45px'}, 300);
		
//			var url = window.location.href;
//			var hash = url.substring(url.indexOf('#tab-'));
//			alert(this.href);
				
			if($('body').is(':not(.blood_draw)')){
				if(last == n) {
					$(this).removeClass('at');
					$('.page-tabs .pro_page_box').animate({height: '167px'}, 300);
					$('div[class|="tab-wrapper"]').hide();
					$('.home-content').show();
					n = '';
				}
				else {			
					$(this).addClass('at');
					$('.tab_caret-'+n).removeClass('hidden');
				}
				slider.stopAutoPlay();
			}
				
				$('.tab-wrapper-' + n).show();
				
				last = n;
				
				
		});	
	});
	
	$(document).ready(function() {
		
		$('.dropdown-toggle').hover(function() {
			$(this).children('span').addClass('active');
			
			$(this).parent('.dropdown').hover(function() {
				$(this).children('span').addClass('active');
			},
			function(){				
				$('span').removeClass('active');
			});
		});
		
////		$('.subnav-wrapper').hide();
//		$k=0;
//		
//		$('.nav .dropdown').hover(function() {
//			$(this).children('.subnav-wrapper').fadeIn();
//			$(this).children('span').addClass('active');
//		}
//		,
//		function() {
//			$(this).children('.subnav-wrapper').removeClass('active').fadeOut();
//			$('.dropdown-toggle span').removeClass('active');
//		}
//		);
	});
	
	/* ---------------------------------------------------------------------- */
	/*	Main Navigation
	/* ---------------------------------------------------------------------- */
	
	jQuery(document).ready(function() {

		var $mainNav    = $('#dropdown-nav').children('ul'),
			optionsList = '<option value="" selected>Navigate...</option>';
		
		// Responsive nav
		$mainNav.find('li').each(function() {

			optionsList += '<option value="' + $anchor.attr('href') + '">' + indent + ' ' + $anchor.text() + '</option>';
		}).end()
		  .after('<select class="responsive-nav">' + optionsList + '</select>');

		$('.responsive-nav').on('change', function() {
			window.location = $(this).val();
		});
		
	});

	jQuery(document).ready(function() {
		  jQuery('input[type="text"]').each(function() {
		     var default_value = this.value;
		     jQuery(this).focus(function() {
		         if(this.value == default_value) {
		             this.value = '';
		             jQuery(this).addClass('user_input');
		         }
		     });
		     jQuery(this).blur(function() {
		         if(this.value == '') {
		             this.value = default_value;
		             jQuery(this).removeClass('user_input');
		         }
		     });
		  });
		});
	
	/* end Main Navigation */
	
	/* Slideshow Script */
//	jQuery(window).resize(function($) {
////	jQuery(document).ready(function($) {
//		jQuery(".royalSlider").royalSlider('updateSliderSize', true);
//	});
	
	window.onload = jQuery(function($) {
		var slider = $(".royalSlider").data('royalSlider');
		var url = window.location.href;
		var hash = url.substring(url.indexOf('#'));
		var hashid = hash.slice(-1);
		var v = hash.slice(0,5);
		
		if(v == '#tab-') {
			$('.home-content').hide();
			$('div[class|="tab-wrapper"]').hide();
			$('.at').removeClass('at');
			
			$('.tab_caret').addClass('hidden');
			$('.tab_caret-' + hashid).removeClass('hidden');
			$('.page-tabs .pro_page_box').animate({height: '45px'}, 300);
			$('a[id="tab-link-' + hashid + '"]').addClass('at');
			
			$('.tab-wrapper-' + hashid).show();
			
//			alert('Califoria');
			if($('body').is(':not(.blood_draw)')){
				slider.stopAutoPlay();
			}
		}		
	});

	function hashChangeAction() {
		
//		This Rings
		
		jQuery(function($) {
			var url = window.location.href;
			var hash = url.substring(url.indexOf('#'));
			var hashid = hash.slice(-1);
			var v = hash.slice(0,5);
//			alert(v);
			if($('.home-content').is(':hidden')) {
				$('div[class|="tab-wrapper"]').hide();
				$('.tab-wrapper-' + hashid).show();
				n = hashid;
			}

//			$('.blood_draw div[class|="tab-wrapper"]').hide();//.animate({left: '400px'}, 1000);
			$('.blood_draw .tab-wrapper-' + hashid).show();//.animate({right: '400px'}, 1000);
			n = hashid;
			
		});
	}
	
	window.onresize =(function($) {
	    setTimeout(function() {
	        jQuery('#full-width-slider').royalSlider('updateSliderSize');
	    }, 500);
	});